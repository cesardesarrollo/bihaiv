<?php
	require_once("../core/autoload.php");
	require_once('../core/modules/index/model/UserData.php');
	require_once('../core/modules/index/model/CuestionarioData.php');
	require_once('../core/modules/index/model/CuestionariosUsuarioData.php');
	require_once('../core/modules/index/model/EstatusCuestionarioData.php');
	require_once('../core/modules/index/model/DaoPregunta.php');
	require_once('../core/modules/index/model/DaoRespuestasUsuario.php');
	require_once('../core/modules/index/model/DaoRoles.php');
	require_once('../core/modules/index/model/DaoOrganizacion.php');
	require_once('../core/modules/index/model/DaoEvaluacion.php');
	require_once('../core/modules/index/model/DaoUbicaciones.php');
	require_once('../core/modules/index/model/DaoPaises.php');
	require_once('../core/modules/index/model/DaoEstados.php');
	require_once('../core/modules/index/model/DaoLocalidades.php');

	// Para agregar la ubicación matriz
	$DaoUbicaciones = new DaoUbicaciones();
	$DaoPaises      = new DaoPaises();
	$DaoEstados     = new DaoEstados();
	$DaoLocalidades = new DaoLocalidades();


    $zoom='16';
    // HYBRID, ROADMAP, SATELLITE, TERRAIN
    $mapTypeId='ROADMAP';
    $coords='20.7217961, -103.38968779999999';

	$userdata = new UserData();
	$database = new Database();
	$database->connect();

	if (isset($_REQUEST["user_id"])) {
		$user = $userdata->getById($_REQUEST["user_id"]);
	}

	// Obtener organización
	$sql = "SELECT id FROM  `organizacion` WHERE `Usuarios_idUsuarios` = '".$user->id."'";
	$query = Executor::doit($sql);
	$organizacion = Model::one($query[0],new Organizacion());

	$CuestionarioData = new CuestionarioData();
	$CuestionariosUsuarioData = new CuestionariosUsuarioData();
	$EstatusCuestionarioData = new EstatusCuestionarioData();
	$DaoEvaluacion = new DaoEvaluacion();

	// Obtener estado del proceso de contestación de cuestionarios
	$estado = null;
	if (isset($_REQUEST["nivel"])) {
		// Obtiene el cuestionario correspondiente
		$Cuestionario = $CuestionarioData->getCuestionario($_REQUEST["nivel"]);

		if(!is_object($Cuestionario)){
			Core::alert("Ha ocurrido un problema, no se encuentra un cuestionario de vinculación habilitado, regrese mas tarde o reviselo con el administrador de Bihaiv.");
			exit;
		}

		// Obtiene cuestionariousuario correspondiente
		$CuestionariosUsuario = $CuestionariosUsuarioData->getByUserCuestionario($user->id,$Cuestionario->idCuestionario);
		if(count($CuestionariosUsuario) === 1){
			// Obtiene estatus
			$EstatusCuestionario = $EstatusCuestionarioData->getByidCuestionariosUsuario($CuestionariosUsuario->idCuestionariosUsuario);
			// Estatus del cuestionario
			if(count($EstatusCuestionario) === 1){
				$estado = (int)$EstatusCuestionario->estatus;
			} else {
				Core::alert("Operación no permitida.");
				exit;
			}
		} else {
			Core::alert("Operación no permitida.");
			exit;
		}

	} else {
		Core::alert("Acceso no permitido.");
		Core::redir(APP_PATH);
		return;
	}

?>

<!doctype HTML>
<HTML Lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    	<link rel="icon" href="assets/img/home/url.png" type="image/x-icon" />
		<title>Cuestionario | Bihaiv</title>
		<link href='https://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="../res/icons/css/font-awesome.min.css">
		<link rel="stylesheet" href="../res/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="../assets/css/base.css">
	</head>
	<body role="document">
		<section class="Profile__header">
			<div class="container">
				<div class="row">
					<div class="col-sm-4 col-lg-4">
						<figure class="figure">
							<img src="../assets/img/home/logo_bihaiv.png" alt="Bihaiv" height="50">
						</figure>
					</div>
				</div>
			</div>
		</section>
		<section class="Profile__nav">
			<div class="container">
				<?php
				if (isset($_REQUEST["nivel"]) and isset($_REQUEST["user_id"])) {
					$user = $userdata->getById($_REQUEST["user_id"]);
					if ($_REQUEST["nivel"] == '1') {
						echo '<h2>Cuestionario de Perfilamiento</h2>';
					} elseif ($_REQUEST["nivel"] == '2') {
						echo '<h2>Cuestionario de Validación</h2>';
					}
				}
				 ?>
			</div>
		</section>
		<section class="Questions__Company">
			<div class="container">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 relative Company__Name">
					<h1><?php echo $user->name;
						if (isset($_REQUEST["nivel"]) and isset($_REQUEST["user_id"])) {
							if ($_REQUEST["nivel"] == '2') {
								echo " Ayuda a tus pares a conocer mejor tu trabajo";
							}
						}
						?>
					</h1>
				</div>
			</div>
		</section>


  		<div class="container padding-row">
			<?php

			//Comprobar sea un estado correcto, enviado a actores/expertos $estado===1 || $estado===2 || $estado===4 || $num_rows===0
			if($estado == 1 OR $estado === 0){

			/*PROCESA RESPUESTAS*/
			if (isset($_POST["enviar_respuestas"])){
					//Recibe parámetros
				 	@$id_cue   = @$_POST['id_cue'];
					@$user_id  = $_POST["user_id"];
					@$nivel    = $_POST["nivel"];

					if ( $_POST["nivel"] == 1 && $estado === 0 ){

						/*Perfilamiento automatizado*/
						$sql = "SELECT * FROM pregunta WHERE Cuestionario_idCuestionario = ".$id_cue." ORDER BY orden";
						$query = Executor::doit($sql);
						$preguntas = Model::many($query[0],new Pregunta());

						foreach ($preguntas as $pregunta) {

					    	if (!isset($_POST['preg_'.$pregunta->idPregunta])){
								Core::alert('¡Existen preguntas sin respuestas aún!');
								Core::goBack(1);
					    		return;
					    	}else{
								$resp_id = $_POST['preg_'.$pregunta->idPregunta];

								$sql = "SELECT * FROM opcionesrespuesta WHERE Pregunta_idPregunta = ".$pregunta->idPregunta." AND idOpcionesRespuesta = ".$resp_id."";
								$query = Executor::doit($sql);
								$respuesta = Model::one($query[0],new RespuestasUsuario());

					    		//Arreglo con preguntas y respuestas
								$resp =  utf8_encode($respuesta->Texto);
								$cuestionario_dtl[] = array(utf8_encode($pregunta->Pregunta)=>$resp);

								$sql = "SELECT * FROM roles WHERE  idRol = ".$respuesta->Roles_idRol."";
								$query = Executor::doit($sql);
								$rol = Model::one($query[0],new Roles());

							    @$arr_res[$rol->nombre] = @$arr_res[$rol->nombre] + 1;
							    @$arr_idRol[$rol->idRol] = @$arr_res[$rol->idRol] + 1;

							    // Guardar respuesta  USER_ID = IDORGANIZACION
								$sql = "INSERT INTO `respuestasusuario` (`fechaRespondio`,`OpcionesRespuesta_idOpcionesRespuesta`,`user_id`) VALUES ('".date('Y-m-d H:i:s')."','".$respuesta->idOpcionesRespuesta."','".$organizacion->id."')";
								$return = Executor::doit($sql);

					    	}
						}
						arsort($arr_res);
						arsort($arr_idRol);

						$cont = 0;
						foreach($arr_idRol as $item => $value){
							if($cont === 0){
							$idRol  = $item;
							}
							$cont++;
						}

						// Verifica que no exista ya un rol asignado
						$sql = "SELECT * FROM organizacion WHERE  `Roles_idRol` !=0 AND  `id` = ".$organizacion->id."";
						$resultado = Executor::doit($sql);

						if ($resultado[0]->num_rows === 0) {

							// Actualizar Organización con rol asignado
							$sql = "UPDATE `organizacion` SET `Roles_idRol` = ".$idRol." WHERE `id` = ".$organizacion->id."";
							$actualiza_org = Executor::doit($sql);
							$cont = 0;
							foreach($arr_res as $item => $value){
								if($cont === 0){
									/*Envío de correo*/
									Registro::enviar_email("resultado_perfilamiento",$item,$user_id,$cuestionario_dtl,"", 0);
									Core::alert('El sistema te ha clasificado en el grupo '.$item.'. Revisa tu correo para continuar el proceso de validación.');
									Core::redir(APP_PATH);
								}
								$cont++;
							}

						}else{
							Core::alert('Ha ocurrido un problema.');
						}

					}
					else if ($_POST["nivel"] == 2 && $estado === 1 ){
					    // Guardar respuetas
						$_telefono 	=	$_POST["_telefono"];
						$_url 		=	$_POST["_url"];
						$_ubicacion =	$_POST["_ubicacion"];
						$_mision 	=	$_POST["_mision"];
						$_vision 	=	$_POST["_vision"];
						$_quienSoy 	=	$_POST["_quienSoy"];
						$_lat 		=	$_POST["lat"];
						$_lng 		=	$_POST["lng"];

						/*
						* UBICACIÓN
						* @description: Ubicación principal
						* ================================
						*/
						if (!empty($_POST["EstateState"])){

							// ELimina existente
							$ubicacion_existente = $DaoUbicaciones->getMatrizByOrgId( $organizacion->id );
							if (count($ubicacion_existente) > 0){
								//$DaoUbicaciones->delete( $ubicacion_existente[0]->id );

								// Modificar existente
								$Ubicaciones = new Ubicaciones();
								$Ubicaciones->setIdOrganizacion( $organizacion->id );
								$Ubicaciones->setIsMatriz( 1 );
								$Ubicaciones->setIsMain( 1 );
								$Ubicaciones->setId( $ubicacion_existente[0]->id );

								if (!empty( $_POST['EstateCountry'] ) ){
								$pais = $DaoPaises->getByName($_POST['EstateCountry']);
								$Ubicaciones->setIdPais( $pais->id );
								}
								if (!empty( $_POST['EstateState'] ) ){
								$estado = $DaoEstados->getByName($_POST['EstateState']);
								$Ubicaciones->setIdEstado( $estado->id );
								} else {
									$Ubicaciones->setIdEstado( 0 );
								}
								if (!empty( $_POST['EstateCity'] ) ){
								$ciudad = $DaoLocalidades->getByName($_POST['EstateCity']);
								$Ubicaciones->setIdLocalidad( $ciudad->id );
								} else {
									$Ubicaciones->setIdLocalidad( 0 );
								}

								$ubicacion_id = $DaoUbicaciones->update( $Ubicaciones );
							}

						}

						// Valida campos llenos
						if ($_telefono === "" || $_ubicacion === "" || $_mision === "" ||  $_vision === "" || $_quienSoy === ""){
							Core::alert('Debes llenar todos los campos obligatorios.');
							Core::goBack(-1);
							exit;
						}

						// Verifica que no exista ya una ubicación asignada
						$sql = "SELECT * FROM organizacion WHERE `ubicacion` != NULL AND `id` = ".$organizacion->id."";
						$resultado_org = Executor::doit($sql);

						if ($resultado_org[0]->num_rows === 0) {

							$cuestionario_dtl = array("_telefono"=>$_telefono,"_url"=>$_url,"_ubicacion"=>$_ubicacion,"_mision"=>$_mision,"_vision"=>$_vision,"_quienSoy"=>$_quienSoy);
							$target_path = "../uploads/cuestionarios/";
							$src_path = APP_PATH."uploads/cuestionarios/";

							$sql = "UPDATE `organizacion` SET `telefono` = '".$_telefono."', `urlWeb` = '".$_url."', `ubicacion` = '".$_ubicacion."', `mision` = ' ".$_mision."', `vision` = '".$_vision."', `quienSoy` = '".$_quienSoy."', `Usuarios_idUsuarios` = ".$user_id.", `lat` = '".$_lat."', `lng` = '".$_lng."' WHERE `organizacion`.`id` = ".$organizacion->id."";
							$resultado = Executor::doit($sql);


							for($var = 1;$var<6;$var++){
								if ($_FILES['archivo'.$var]['name']!==""){
									$target = $target_path . $organizacion->id . "_" . basename( $_FILES['archivo'.$var]['name']);
									$src = $src_path . $organizacion->id . "_" . basename( $_FILES['archivo'.$var]['name']);
									if(move_uploaded_file($_FILES['archivo'.$var]['tmp_name'], $target)) {

										// Se guardan aportaciones
										$sql = "INSERT INTO `publicaciones` (`fechaCreado`, `tipo`, `user_id`) VALUES ('".date('Y-m-d H:i:s')."', '4', '".$organizacion->id."');";
										$publicaciones = Executor::doit($sql);

										$sql = "INSERT INTO `aportacion` (`titulo`, `descripcion`, `fecha`, `thumbnail`, `post_id`)
										VALUES ('".$_POST["titulo".$var]."', '".$_POST["descripcion".$var]."', '".date('Y-m-d H:i:s')."', '".$src."', '".$publicaciones[1]."')";
										$aportacion = Executor::doit($sql);

										if ($aportacion[0] != 1) {
											echo "Ha ocurrido un error al tratar de crear la aportación ". basename( $_FILES['archivo'.$var]['name']). ", trate de nuevo.";
											return;
										}
										$src = "";

									} else{
										echo "Ha ocurrido un error al tratar de cargar el archivo ". basename( $_FILES['archivo'.$var]['name']). ", trate de nuevo.";
										return;
									}
								}
							}
							Registro::enviar_email("resultado_evaluacion","",$user_id,$cuestionario_dtl,"", 0);
							Core::alert('¡Correcto!, El resultado de tu evaluación se te hará saber a tu correo en un periodo aproximado de 15 días.');
							Core::redir(APP_PATH);
					
						}
						else {
							Core::alert('Ya existe registro de datos para este actor.');
						}
					}
				}

				// Mostrando formulario
				else if (!isset($_POST["enviar_respuestas"])  && isset($_GET["nivel"])  && ($_GET["nivel"]==1 && $estado == 0)  OR ($_GET["nivel"]==2 && $estado == 1) ){


					/*DESPLIEGA CUESTIONARIO*/
					$user_id  = $_GET["user_id"];
					//$fech_act = $_GET["fech_act"];
					$nivel 	  = $_GET["nivel"];

					/*Obtener datos de usuario */
					if ($user->id !== 0 and $user->id !== ""){

						/*El usuario y prosigue a cargar cuestionario activo de perfilamiento*/
						$sql = "SELECT * FROM cuestionario WHERE activo = 1 and nivel  = ".$nivel." LIMIT 0,1";
						$query = Executor::doit($sql);
						$cuestionario = Model::one($query[0],new Cuestionario());

						// Valida que el cuestionario exista
						if($cuestionario->idCuestionario === 0 and $cuestionario->idCuestionario === "") {
							Core::alert("No existe un cuestionario con ese identificador.");
							return;
						}

						$sql = "SELECT * FROM cuestionariosusuario WHERE user_id = ".$organizacion->id." and idCuestionario  = ".$cuestionario->idCuestionario." LIMIT 0,1";
						$query = Executor::doit($sql);
						$cuestionariosusuario = Model::one($query[0],new UserData());

						// Valida que el cuestionario este asignado al usuario
						if(!$cuestionariosusuario->user_id === $user_id) {
							Core::alert("No existe un cuestionario asignado a este usuario.");
							return;
						}

						// Comprueba la caducidad
						if(!Cuestionario::vigencia($cuestionariosusuario->fechaCreacion,"perfilamiento")){ //true si aun tiene vigencia
							Core::alert("La vigencia para contestar este cuestionario ha caducado, por lo tanto este enlace ya no funciona.");
							return;
						}

						// Valida que el rol no esté establecido
						if($_GET["nivel"] === '1' && $cuestionariosusuario->Roles_idRol !== '0') {
							Core::alert("Este cuestionario ya ha sido contestado.");
							return;
						}

						// Valida que ubicación no esté establecida
						$sql = "SELECT * FROM organizacion WHERE `ubicacion` != '' AND `id` = ".$organizacion->id."";
						$resultado_org = Executor::doit($sql);
						if($_GET["nivel"] === '2' && $resultado_org[1] == 1) {
							Core::alert("Este cuestionario ya ha sido contestado.");
							return;
						}

						if($_GET["nivel"] === '1' && $estado === 0) {

						    echo "<h2>Cuestionario: ".utf8_encode($cuestionario->nombre)."</h2>";
						    echo "<form method='post'>";
						    echo "<input type='hidden' name='id_cue' value='".$cuestionario->idCuestionario."'/>";
						    echo "<input type='hidden' name='user_id' value='".$user->id."'/>";
						    echo "<input type='hidden' name='nivel' value='".$nivel."'/>";
					 		echo "<div class='row'><div class='col-md-12'>";
						    echo "<input type='hidden'' id='address''>";
						    echo "<ol>";

							$sql = "SELECT * FROM pregunta WHERE Cuestionario_idCuestionario = ".$cuestionario->idCuestionario." ORDER BY Orden";
							$query = Executor::doit($sql);
							$preguntas = Model::many($query[0],new UserData());

							foreach ($preguntas as $pregunta) {
							   	echo "<li><strong>".utf8_encode($pregunta->Pregunta)."</strong>";

								$sql = "SELECT * FROM opcionesrespuesta WHERE Pregunta_idPregunta = ".$pregunta->idPregunta." ORDER BY Orden";
								$query = Executor::doit($sql);
								$respuestas = Model::many($query[0],new UserData());

								echo "<ul class='list-inline'>";
								foreach ($respuestas as $respuesta) {
							    	echo "<li><input type='radio' value='".$respuesta->idOpcionesRespuesta."' name='preg_".$respuesta->Pregunta_idPregunta."' required> ".utf8_encode($respuesta->Texto)."</li>";
							    }
							    echo "</ul>";
								echo "</li>";
						    }
						    echo "</ol>";
						    echo "</div></div>";

						}
						else if ($_GET["nivel"] === '2' && $estado === 1 ) {

						    echo "<form enctype='multipart/form-data' method='post'>";
						    echo "<input type='hidden' name='id_cue' value='".$cuestionario->idCuestionario."'/>";
						    echo "<input type='hidden' name='user_id' value='".$user->id."'/>";
						    echo "<input type='hidden' name='nivel' value='".$nivel."'/>";
 							?>

						  <div class="form-group">
						    <label for="">Teléfono:</label>
						    <input type="text" name="_telefono" class="form-control"  placeholder="Teléfono" value="" required>
						  </div>
						  <div class="form-group">
						    <label for="">Url:</label>
						    <input type="text" name="_url" class="form-control" placeholder="Dirección de tu sitio" value="">
						  </div>
						  <div class="form-group">
					          <label>Ubicación:</label>
					          <input type="text" name="_ubicacion" id="address" class="form-control"  placeholder="Mi ubicación" value="" required>
					          <label class="hide">Latitud</label>
					          <input type="hidden" name="lat" id="lat" value="" />
					          <label class="hide">Longitud</label>
					          <input type="hidden" name="lng" id="lng" value="" />
					        </div>
					        <div class="form-group">
					          <div id="map_canvas" style="width: 400px; height: 280px; margin: 0 auto;"></div>
					          <div id="callback_result" class="hidden"></div>
					          <label>¿Actualizar marcador? </label>
					          <select name="reverseGeocode" id="reverseGeocode" class="form-control">
					            <option value="false" selected>No</option>
					            <option value="true">Si</option>
					          </select>
								<input type="hidden" id="EstateCity" name="EstateCity">
								<input type="hidden" id="EstateColony" name="EstateColony">
								<input type="hidden" id="EstateState" name="EstateState">
								<input type="hidden" id="EstateCountry" name="EstateCountry">
								<input type="hidden" id="EstateZipcode" name="EstateZipcode">
								<input type="hidden" id="EstateAddress" name="EstateAddress">
					        </div>
						  <div class="form-group">
						    <label for="">Misión:</label>
						    <textarea class="form-control" name="_mision" required></textarea>
						  </div>
						  <div class="form-group">
						    <label for="">Visión:</label>
						    <textarea class="form-control" name="_vision" value="" required></textarea>
						  </div>
						  <div class="form-group">
						    <label for="">Acerca de mi:</label>
						    <textarea class="form-control" name="_quienSoy" required></textarea>
						  </div>

						  <h2>Carga hasta 5 archivos que ayuden a validarte como organización</h2>
						  <div class="form-group">
						    <label for="">Archivo 1:</label>
						    <input type='file' name='archivo1'/>
						    <input type="text" name="titulo1" class="form-control"  placeholder="Título del documento" value="">
						    <textarea  class="form-control" name="descripcion1"  placeholder="Descripción del documento"></textarea>
						  </div>
						  <div class="form-group">
						    <label for="">Archivo 2:</label>
						    <input type='file' name='archivo2'/>
						    <input type="text" name="titulo2" class="form-control"  placeholder="Título del documento" value="">
						    <textarea  class="form-control" name="descripcion2"  placeholder="Descripción del documento"></textarea>
						  </div>
						  <div class="form-group">
						    <label for="">Archivo 3:</label>
						    <input type='file' name='archivo3'/>
						    <input type="text" name="titulo3" class="form-control"  placeholder="Título del documento" value="">
						    <textarea  class="form-control" name="descripcion3"  placeholder="Descripción del documento"></textarea>
						  </div>
						  <div class="form-group">
						    <label for="">Archivo 4:</label>
						    <input type='file' name='archivo4'/>
						    <input type="text" name="titulo4" class="form-control"  placeholder="Título del documento" value="">
						    <textarea  class="form-control" name="descripcion4"  placeholder="Descripción del documento"></textarea>
						  </div>
						  <div class="form-group">
						    <label for="">Archivo 5:</label>
						    <input type='file' name='archivo5'/>
						    <input type="text" name="titulo5" class="form-control"  placeholder="Título del documento" value="">
						    <textarea  class="form-control" name="descripcion5"  placeholder="Descripción del documento"></textarea>
						  </div>
					<?php
						} else { ?>
							<div class='row'>
								<div class='col-md-12'>
									<h2>Este cuestionario ya fue contestado o está deshabilitado</h2>
								</div>
							</div>
					<?php
						}

						if($nivel == "1" && $estado == 0 || $nivel == "2" && $estado == 1 ) {
					?>
					      <div class='row'><div class='col-md-12'><button type='submit' name='enviar_respuestas' value='ok' class='btn btn-primary pull-right'>Enviar</button></div></div>
					    </form>
				<?php
						}
					}else{
						echo "Un error ha ocurrido con el usuario.";
					}

				}else{
					echo "Acceso no autorizado.";
				}

			}else echo "Este cuestionario ya no se encuentra disponible.";
			?>
		</div> <!-- /container -->
		  <script src="../res/jquery/jquery.min.js"></script>
		  <script src="../res/bootstrap/js/bootstrap.min.js"></script>
	      <script src="http://maps.google.com/maps/api/js?key=AIzaSyBxG4FoW_Mx6zqrYJXQBsyzscH13xLFjqI&libraries=visualization"></script>
      	  <script src="../res/jquery-ui/jquery.ui.js"></script>
	      <script src="../res/icmap.js"></script>

		<script>
		 $(function () {
		        /*
		         * Maps
		         */
		        var addresspickerMap = $('#address').addresspicker({
		            regionBias: "mx",
		            updateCallback: showCallback,
		            mapOptions: {
		                zoom: <?php echo $zoom; ?>,
		                center: new google.maps.LatLng(<?php echo $coords; ?>),
		                scrollwheel: true,
		                mapTypeId: google.maps.MapTypeId.<?php echo $mapTypeId; ?>,
		                streetViewControl: false
		            },
		            elements: {
		                map:      "#map_canvas",
		                lat:      "#lat",
		                lng:      "#lng",
		                type:    '#type',
						locality:                     '#EstateCity',
						sublocality:                  '#EstateColony',
						administrative_area_level_1:  '#EstateState',
						country:                      '#EstateCountry',
						postal_code:                  '#EstateZipcode',
						route:                        '#EstateAddress'
		            }
		        });

		        var gmarker = addresspickerMap.addresspicker("marker");
		        gmarker.setVisible(true);

		        addresspickerMap.addresspicker("updatePosition");

		        var map = addresspickerMap.addresspicker("map");
		          google.maps.event.trigger(map,"resize");

		        $('#reverseGeocode').change(function(){
		          $("#address").addresspicker("option", "reverseGeocode", ($(this).val() === 'true'));
		        });

		        function showCallback(geocodeResult, parsedGeocodeResult){
		          $('#callback_result').text(JSON.stringify(parsedGeocodeResult, null, 4));
		        }
		    });

		</script>
	</body>
</HTML>
