<?php 
session_start();
require_once('../../../core/lang/lang.php');
?>
<div class="modal fade" id="m-info-rol" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?=translate('Descripción del rol')?></h4>
      </div>
      <div class="modal-body">
        <center>
          <img id="photo_rol" src="" alt="" style="max-width: 100%;">
        </center>
          <!-- <div class="Img-modal">
            <figure class="figure">
                <img id="photo_rol" src="" alt="">
            </figure>
          </div> -->
          <h1 id="name"></h1>
          <p id="description"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?=translate('Cerrar')?> <i class="fa fa-times"></i></button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
