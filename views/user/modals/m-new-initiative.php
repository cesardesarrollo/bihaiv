<?php
session_start();
require_once('../../../core/lang/lang.php');
require_once '../../../core/modules/index/model/DaoOrganizacion.php';
require_once '../../../core/modules/index/model/DaoUbicaciones.php';
$DaoOrganizacion = new DaoOrganizacion();
$organizacion    = $DaoOrganizacion->getByUserId($_SESSION["user_id"]);


$DaoUbicaciones = new DaoUbicaciones();
$Ubicaciones    = new Ubicaciones();
$Ubicaciones    = $DaoUbicaciones->getMainByOrgId( $organizacion->id );

?>
<div class="modal fade" id="m-new-initiative" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form id="saveLead">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><?=translate('Nueva Iniciativa')?></h4>
        </div>
        <div class="modal-body">
            <div class="row row-margin-bottom">
              <div class="col-sm-12 col-md-6 col-lg-6">
                  <div class="form-group">
                    <label for="txt__Name"><?=translate('Nombre')?>:</label>
                    <input type="text" class="form-control" id="txt__Name" placeholder="<?=translate('Nombre')?>" required autocomplete="false" data-validate-empty>
                  </div>
              </div>
               <div id="timeValidator">
                <div class="col-sm-6 col-md-3 col-lg-3">
                    <div class="form-group">
                      <label for="txt__StartHour"><?=translate('Hora inicio')?>:</label>
                      <input type="text" class="form-control time start" autocomplete="off" id="txt__StartHour" required>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3 col-lg-3">
                    <div class="form-group">
                      <label for="txt__EndHour"><?=translate('Hora fin')?>:</label>
                      <input type="text" class="form-control time end" autocomplete="off" id="txt__EndHour" required data-validate-empty>
                    </div>
                </div>
              </div>
              <div class="col-sm-12 col-md-6 col-lg-6">
                  <div class="form-group">
                    <label for="txt__InitialDate"><?=translate('Fecha de inicio')?>:</label>
                    <input type="text" class="form-control" id="txt__InitialDate" placeholder="<?=translate('Fecha de inicio')?>" required autocomplete="off" data-validate-empty>
                  </div>
              </div>
              <div class="col-sm-12 col-md-6 col-lg-6">
                  <div class="form-group">
                    <label for="txt__EndDate"><?=translate('Fecha final')?>:</label>
                    <input type="text" class="form-control" id="txt__EndDate" placeholder="<?=translate('Fecha final')?>" required autocomplete="off" data-validate-empty>
                  </div>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12">
                  <div class="form-group">
                    <label for="txt__Description"><?=translate('Descripción')?>:</label>
                    <textarea class="form-control" placeholder="<?=translate('Descripción de la iniciativa')?>" id="txt__Description" required autocomplete="off" data-validate-empty></textarea>
                  </div>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12">
                  <div class="form-group">
                    <label for="objetivos_list"><?=translate('Objetivos')?>:</label>
                    <div class="row">
                      <div class="col-xs-12">
                        <div class="form-group">
                          <select class="selectpicker form-control show-menu-arrow" id="objetivos_list" multiple>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12">
                  <div class="form-group">
                    <button type="button" class="btn btn-primary" id="btnShowObjetive"><?=translate('Añadir Objetivo')?></button>
                  </div>
                  <div class="form-group hide" id="inputObjetive">
                    <div class="row">
                      <div class="col-sm-12 col-md-12 col-lg-9">
                        <input type="text" name="new_objetive" id="txt__NewObjetive" class="form-control"  placeholder="<?=translate('Añade un nuevo objetivo')?>" >
                      </div>
                      <div class="col-sm-12 col-md-12 col-lg-3">
                        <button type="button" class="btn btn-primary" id="btnAddObjetive"><?=translate('Confirmar')?></button>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12">
                  <div class="form-group">
                    <label for="friends_list"><?=translate('Añadir Colaboradores')?>:</label>
                    <div class="row">
                      <div class="col-xs-12">
                        <div class="form-group">
                          <select class="selectpicker form-control show-menu-arrow" id="friends_list" multiple>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12">
                  <div class="form-group">
                    <label for="descripcion"><?=translate('Añadir Eventos')?>:</label>
                    <div class="row">
                      <div class="col-xs-12">
                        <div class="form-group">
                          <select class="selectpicker form-control show-menu-arrow" id="events_list" multiple>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="form-group">
                  <label for="cmb__Alcanze"><?=translate('Alcance')?>:</label>
                  <!--  onchange="seteaUbicacion(this.options[this.selectedIndex].text)"  -->
                  <select class="form-control" name="idAlcanze" id="cmb__Alcanze" value=""  >
                    <option value=""><?=translate('Selecciona una opción')?></option>
                    <?php
                    $alcanzes = $DaoUbicaciones->getAlcanzes();
                    foreach ($alcanzes as $alcanze ) {
                      echo '<option value="'.$alcanze['id'].'" name="'.$alcanze['nombre'].'">'.$alcanze['nombre'].'</option>';
                    }
                    ?>
                  </select>
                </div>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="form-group">
                  <label for=""><?=translate('Ubicacion(es) de alcance')?>:</label>
                  <button id="btn_agregar_ubicacion" type="button" class="btn btn-primary pull-right" onclick='$("#newUbication").slideToggle("fast");'>
                    <?=translate('Agregar ubicación')?>
                  </button>
                  <div id="newUbication" class="row margin-row-top" style="display:none;" id="cmb__Country_field">
                    <div class="col-lg-6 col-md-6">
                      <label for="cmb__Country"><?=translate('País')?>:</label>
                      <select class="form-control" id="cmb__Country" ></select>
                    </div>
                    <div class="col-lg-6 col-md-6" id="cmb__State_field">
                      <label for="cmb__State"><?=translate('Estado')?>:</label>
                      <select class="form-control" id="cmb__State" ></select>
                    </div>
                    <div class="col-lg-6 col-md-6 margin-row-top" id="cmb__Municipe_field">
                      <label for="cmb__Municipe"><?=translate('Municipio')?>:</label>
                      <select class="form-control" id="cmb__Municipe" ></select>
                    </div>
                    <div class="col-lg-6 col-md-6 margin-row-top" id="cmb__city_field">
                      <label for="cmb__city"><?=translate('Localidad')?>:</label>
                      <select class="form-control" id="cmb__city"></select>
                    </div>
                    <div class="col-lg-6 col-md-6 margin-row-top" id="cmb__Influencia_field">
                      <label for="cmb__Influencia"><?=translate('¿Influye en el ecosistema?')?>:</label>
                      <select class="form-control" id="cmb__Influencia">
                        <option value="1" ><?=translate('Con Influencia')?> </option>
                        <option value="0" ><?=translate('Sin Influencia')?> </option>
                      </select>
                    </div>
                    <div class="col-xs-6 col-lg-6 margin-row-top">
                      <button id="addUbication" type="button" class="btn btn-primary pull-right">
                        <?=translate('Guardar')?>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="form-group">
                  <table class="table table-bordered bihaiv_table margin-row-top hide" id="otrasUbicaciones">
                    <thead>
                      <tr>
                        <td><?=translate('País')?></td>
                        <td><?=translate('Estado')?></td>
                        <td><?=translate('Municipio')?></td>
                        <td><?=translate('Localidad')?></td>
                        <td><?=translate('Influye')?></td>
                        <th><?=translate('Eliminar')?></th>
                      </tr>
                    </thead>
                    <tbody id="lstUbications">
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12">
                  <div class="dropzone" id="initiativePhoto"></div>
              </div>
              <div id="row-msg" class="col-sm-12 col-md-12 col-lg-12 margin-row-top" style="display:none;">
                  <div class="alert" role="alert" id="panel-alert">
                    <span id="icon"> </span> <span id="msg"> </span>
                  </div>
              </div>
              <input type="hidden" id="txt__user">
              <input type="hidden" id="thumbnail">
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><?=translate('Cerrar')?> <i class="fa fa-times"></i></button>
            <button id="btnSave" type="submit" class="btn btn-primary"><?=translate('Guardar')?> <i class="fa fa-floppy-o"></i></button>
          </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>

  // Ubicaciones a objeto
  var arreglo = []

  // Elimina ubicación
  function delUbication(e, index){
    e.preventDefault();

    arreglo.splice(index, 1);
    pintaTabla();
  }

  function Ubicacion(pais, estado, municipio, localidad, influencia, pais_nombre, estado_nombre, municipio_nombre, localidad_nombre, influencia_nombre) {
    this.pais_id = pais;
    this.estado_id = estado;
    this.municipio_id = municipio;
    this.localidad_id = localidad;
    this.influencia = influencia;

    this.pais_nombre = pais_nombre;
    this.estado_nombre = estado_nombre;
    this.municipio_nombre = municipio_nombre;
    this.localidad_nombre = localidad_nombre;
    this.influencia_nombre = influencia_nombre;
  }

  function pintaTabla(){
    var row = "";
    $.each(arreglo, function(k,v){
      row += "<tr>"
      row += "  <td>" + ((v.pais_id) ? v.pais_nombre: "") + "</td>"
      row += "  <td>" + ((v.estado_id) ? v.estado_nombre : "") + "</td>"
      row += "  <td>" + ((v.municipio_id) ? v.municipio_nombre: "") + "</td>"
      row += "  <td>" + ((v.localidad_id) ? v.localidad_nombre: "") + "</td>"
      row += "  <td>" + ((v.influencia) ? v.influencia_nombre : "") + "</td>"
      row += "  <td><span class='btn btn-primary' onclick='delUbication(event, " + k + ", 1)' name='" + k + "'>X</span></td>"
      row += "</tr>";
    });
    $('#lstUbications').html(row);
  }

  function initUbicaciones(){

    $("#cmb__Alcanze").on("change", function(e){
      e.preventDefault();
      seteaUbicacion(this.options[this.selectedIndex].text);
    });

    // Ubicaciones
    var cmb__Country        = $("#cmb__Country");
    var cmb__State          = $("#cmb__State");
    var cmb__Municipe       = $("#cmb__Municipe");
    var cmb__city           = $("#cmb__city");
    var cmb__Influencia     = $("#cmb__Influencia");

    var pais_id_field       = $("#cmb__Country_field");
    var estado_id_field     = $("#cmb__State_field");
    var municipio_id_field  = $("#cmb__Municipe_field");
    var localidad_id_field  = $("#cmb__city_field");
    var influencia_field    = $("#cmb__Influencia_field");

    // Carga Paises
    $.post('api/front/paises.php', {'method' : 'all'})
    .done(function( data ){
      cmb__Country.html('');
      var option = '<option value="">Selecciona un país</option>';
      $.each(JSON.parse(data), function(k,v){
        option += '<option id="' + v.prefijo + '" value="' + v.id + '">' + v.nombre + '</option>';
      })
      cmb__Country.html(option);
      option = "";
    });

    cmb__Country.on('change',function(e){
      e.preventDefault();

      var id_country = $(this).val();
      if(estado_id_field.length){

        cmb__State.html('');
        $.post('api/front/estados.php', {'method' : 'byPaisId', 'id' : id_country})
        .done(function(data){
          var option = '<option value="">Selecciona un estado</option>';
          $.each(JSON.parse(data), function(k,v){
            option += '<option id="' + v.abrev + '" value="' + v.id + '">' + v.nombre + '</option>';
          });
          cmb__State.html(option);
          option = "";
        })
      }
    });

    cmb__State.on('change',function(e){
      e.preventDefault();

      var id_state = $(this).val();
      if(municipio_id_field.length){

        cmb__Municipe.html('');
        $.post("api/front/municipios.php", {'method': 'byEstadoId', 'id' : id_state})
        .done(function(data){
        var option = '<option value="">Selecciona un municipio</option>';
          $.each(JSON.parse(data), function(k,v){
            option += '<option id="' + v.clave + '" value="' + v.id + '">' + v.nombre + '</option>';
          });
          cmb__Municipe.html(option);
          option = "";
        });
      }
    });

    cmb__Municipe.on('change',function(e){
      e.preventDefault();

      var id_municipe = $(this).val();
      if(localidad_id_field.length){

        cmb__city.html('');
        $.ajax({
          url   : 'api/front/localidades.php',
          type  : 'POST',
          data  : { method : 'byMunicipioId', id : id_municipe},
          async : false,
        })
        .done(function(data){
        var option = '<option value="">Selecciona una localidad</option>';
          $.each(JSON.parse(data), function(k,v){
            option += '<option id="' + v.clave + '" value="' + v.id + '">' + v.nombre + '</option>';
          });
          cmb__city.html(option);
          option = "";
        })
        .fail(function(err){
          console.log( err );
        })
      }
    });

    function seteaUbicacion(alcanze){
      // Esconde
      if(!pais_id_field.hasClass('hide')){
        pais_id_field.addClass('hide');
      }
      if(!estado_id_field.hasClass('hide')){
        estado_id_field.addClass('hide');
      }
      if(!municipio_id_field.hasClass('hide')){
        municipio_id_field.addClass('hide');
      }
      if(!localidad_id_field.hasClass('hide')){
        localidad_id_field.addClass('hide');
      }
      if(!influencia_field.hasClass('hide')){
        influencia_field.addClass('hide');
      }

      // nacional
      if (alcanze === 'NACIONAL'){
        console.log(pais_id_field);
        if(pais_id_field.hasClass('hide')){
          pais_id_field.removeClass('hide');
        }
        if(!estado_id_field.hasClass('hide')){
          estado_id_field.addClass('hide');
        }
        if(!municipio_id_field.hasClass('hide')){
          municipio_id_field.addClass('hide');
        }
        if(!localidad_id_field.hasClass('hide')){
          localidad_id_field.addClass('hide');
        }
        if(!influencia_field.hasClass('hide')){
          influencia_field.addClass('hide');
        }
      }
      // regional
      else if (alcanze === 'REGIONAL'){
        if(pais_id_field.hasClass('hide')){
          pais_id_field.removeClass('hide');
        }
        if(estado_id_field.hasClass('hide')){
          estado_id_field.removeClass('hide');
        }
        if(!municipio_id_field.hasClass('hide')){
          municipio_id_field.addClass('hide');
        }
        if(!localidad_id_field.hasClass('hide')){
          localidad_id_field.addClass('hide');
        }
        if(!influencia_field.hasClass('hide')){
          influencia_field.addClass('hide');
        }
      }
      // estatal
      else if (alcanze === 'ESTATAL'){
        if(pais_id_field.hasClass('hide')){
          pais_id_field.removeClass('hide');
        }
        if(estado_id_field.hasClass('hide')){
          estado_id_field.removeClass('hide');
        }
        if(!municipio_id_field.hasClass('hide')){
          municipio_id_field.addClass('hide');
        }
        if(!localidad_id_field.hasClass('hide')){
          localidad_id_field.addClass('hide');
        }
        if(!influencia_field.hasClass('hide')){
          influencia_field.addClass('hide');
        }
      }
      // local
      else if (alcanze === 'LOCAL'){
        if(pais_id_field.hasClass('hide')){
          pais_id_field.removeClass('hide');
        }
        if(estado_id_field.hasClass('hide')){
          estado_id_field.removeClass('hide');
        }
        if(municipio_id_field.hasClass('hide')){
          municipio_id_field.removeClass('hide');
        }
        if(localidad_id_field.hasClass('hide')){
          localidad_id_field.removeClass('hide');
        }
        if(influencia_field.hasClass('hide')){
          influencia_field.removeClass('hide');
        }
      }
    }
    seteaUbicacion(document.getElementById('cmb__Alcanze').options[document.getElementById('cmb__Alcanze').selectedIndex].text);

    // Agrega localidades
    $("#addUbication").on("click", addToLstUbicacion);

    function addToLstUbicacion(e){
      e.preventDefault();

      var pais_text =  ((!pais_id_field.hasClass('hide')) ? document.getElementById('cmb__Country').options[document.getElementById('cmb__Country').selectedIndex].text : "" );
      var estado_text =  ((!estado_id_field.hasClass('hide')) ? document.getElementById('cmb__State').options[document.getElementById('cmb__State').selectedIndex].text : "" );
      var municipio_text =  ((!municipio_id_field.hasClass('hide')) ? document.getElementById('cmb__Municipe').options[document.getElementById('cmb__Municipe').selectedIndex].text : "" );
      var localidad_text =  ((!localidad_id_field.hasClass('hide')) ? document.getElementById('cmb__city').options[document.getElementById('cmb__city').selectedIndex].text : "" );
      var influencia_text =  ((!influencia_field.hasClass('hide')) ? document.getElementById('cmb__Influencia').options[document.getElementById('cmb__Influencia').selectedIndex].text : "" );

      if(pais_text != ""){
        var ubicacion = new Ubicacion(  cmb__Country.val(), 
                                        cmb__State.val(), 
                                        cmb__Municipe.val(), 
                                        cmb__city.val(), 
                                        cmb__Influencia.val(),
                                        pais_text,
                                        estado_text,
                                        municipio_text,
                                        localidad_text,
                                        influencia_text
                                      );
        arreglo.push(ubicacion);
      }

      // Limpieza
      cmb__State.val(""); 
      cmb__Municipe.val(""); 
      cmb__city.val(""); 
      cmb__Influencia.val("");

      var otrasUbicaciones = $('#otrasUbicaciones');
      if(arreglo.length > 0){
        otrasUbicaciones.removeClass("hide");
      } else {
        if(!otrasUbicaciones.hasClass("hide")){
          otrasUbicaciones.addClass("hide");
        }
      }

      pintaTabla();

    }
  }

  $(function () {

    var timeOnlyExampleEl = document.getElementById('timeValidator');
    var timeOnlyDatepair = new Datepair(timeOnlyExampleEl);

    // Listado de objetivos
    var btnShowObjetive = $('#btnShowObjetive');
    var btnAddObjetive  = $('#btnAddObjetive');
    var inputObjetive   = $('#inputObjetive');
    $objetivos_list     = $('#objetivos_list');
    $friends_list       = $('#friends_list');
    $events_list        = $('#events_list');

    btnShowObjetive.on('click',showObjetive);
    function showObjetive(e){
      e.preventDefault();

      if(inputObjetive.hasClass('hide')){
        inputObjetive.removeClass('hide');
      }
    }

    btnAddObjetive.on('click', addObjetive);
    function addObjetive(e){
      e.preventDefault();

      var descripcion = $('#txt__NewObjetive').val();
      if(descripcion == "" || descripcion == null){
        setFlash('<?=translate('label errordescobjetivo')?>', 'error');
        return false;
      }

      $.post('api/front/objetivo.php', {'method': 'save', 'descripcion': descripcion})
        .done(function( _res ) {
          console.log(_res);
          if(+_res > 0){
            setFlash('<?=translate('label addobjetivo')?>', 'success');
            $('#txt__NewObjetive').val('');
            // Destruir y construir listado
            $objetivos_list.selectpicker('destroy');
            cargarListaObjetivos();
          } else {
            setFlash('El objetivo no se ha añadido: ' + _res, 'error');
          }
        })
        .error(function( _err ){
          console.log( _err );
        });
    }

    function cargarListaObjetivos(){
      $.post('api/front/objetivo.php', {'method': 'all'})
        .done(function( _res ) {
          var options = '';
          $objetivos_list.html('');
          $.each(_res,function(index, value){
            options += '<option value=' + value.id + '>' + value.descripcion + '</option>';
          });
          $objetivos_list.append(options);
          $objetivos_list.selectpicker({
            style: 'btn-primary',
            size: 4,
            liveSearch: true,
            width: "100%",
            maxOptions: 5,
            actionsBox: false,
            title: '<?=translate('label listaobjetivos') ?>.',
            tickIcon: 'fa fa-key'
          });
        })
        .error(function( _err ){
          console.log( _err );
        });
    }

    // Carga de sugerencias de colaboradores
    function cargarListaSugerencias(){
      var user_id = $("#txt__user").val();
      $.post('api/front/sugerencia.php', {'method': 'actorsByEnfoque', 'user_id': user_id, 'friends_inyect': true, 'same_rol': true })
        .done(function( _res ) {
          var _res = JSON.parse(_res);
          var options = '';
          $friends_list.html('');
          $.each(_res,function(index, value){
            options += '<option value=' + value.user_id + '>' + value.user_name + '</option>';
          });
          $friends_list.append(options);
          $friends_list.selectpicker({
            style: 'btn-primary',
            size: 4,
            liveSearch: true,
            width: "100%",
            actionsBox: true,
            title: '<?=translate('label listainvitados') ?>.',
            tickIcon: 'fa fa-user'
          });
        })
        .error(function( _err ){
          console.log( _err );
        });
      }

    // Carga de eventos
    function cargarListaEventos(){
    $.post('api/front/events.php', {'method' : 'all', 'action': 'getAllEvents'})
      .done(function( _res ) {
        var _res = JSON.parse(_res);
        var options = '';
        $events_list.html('');
        $.each(_res,function(index, value){
          options += '<option value=' + value._idevento + '>' + value._titulo + '</option>';
        });
        $events_list.append(options);
        $events_list.selectpicker({
          style: 'btn-primary',
          size: 4,
          liveSearch: true,
          width: "100%",
          actionsBox: true,
          title: '<?=translate('label listaeventos') ?>.',
          tickIcon: 'fa fa-calendar'
        });
      })
      .error(function( _err ){
        console.log( _err );
      });
    }

    // Cuando el modal se ha terminado de dibujar
    $('#m-new-initiative').on("shown.bs.modal",function(){

      cargarListaObjetivos();
      cargarListaSugerencias();
      cargarListaEventos();

      // Inicia configuración de initUbicaciones
      initUbicaciones();

      $("#txt__InitialDate").datepicker({ dateFormat: 'yy-mm-dd' });
      $("#txt__EndDate").datepicker({ dateFormat: 'yy-mm-dd' });
      $('#txt__StartHour').timepicker({ 'step': 30 });
      $("#txt__EndHour").timepicker({'step' : 30});
      $('#timeValidator .time').timepicker({
        'showDuration': true,
        'timeFormat': 'g:ia'
      });
    });

    //PHOTOS
    var iniciativa = new Iniciativas();
    Dropzone.autoDiscover = false;
    var fileList = new Array;
    var i =0;
    $("div#initiativePhoto").dropzone({
      acceptedFiles : '.jpeg, .png, .gif, .jpg',
      dictDefaultMessage: "¡<?=translate('label cargafoto') ?>!",
      addRemoveLinks: true,
      init: function() {

          // Hack: Add the dropzone class to the element
          $(this.element).addClass("dropzone");

          this.on("success", function(file, serverFileName) {
              fileList[i] = {"serverFileName" : serverFileName, "fileName" : file.name,"fileId" : i };
              $('#thumbnail').val(fileList[i].serverFileName);
              i++;

          });
          this.on("removedfile", function(file) {
              var rmvFile = "";
              for(f=0;f<fileList.length;f++){

                  if(fileList[f].fileName == file.name)
                  {
                      rmvFile = fileList[f].serverFileName;
                  }
              }
              if (rmvFile){
                  $.ajax({
                      url: "api/front/a-deleteitems.php",
                      type: "POST",
                      data: { "fileList" : rmvFile }
                  });
              }
          });
      },
      url: "api/front/i-uploads.php"
    });



    //SAVE NEW
    $(document).off('submit').on('submit', '#saveLead', function(e){
        e.preventDefault();

        /* validate dates */
        var startDate = $("#txt__InitialDate").datepicker({ dateFormat: "yy-mm-dd" }).datepicker('getDate');
        var endDate = $("#txt__EndDate").datepicker({ dateFormat: "yy-mm-dd" }).datepicker('getDate');
         var currentDate = new Date();
        //setFlash(startDate,'warning');
        if( endDate<currentDate){
          setFlash("<?= translate('label errorfechalfinal')?>", 'warning');
          return false;
        }
        /* valida fecha final con inicial */
        if( (moment(startDate).format('Y-M-D')) > (moment(endDate).format('Y-M-D')) ) {
          setFlash("<?=translate('label errorfechainicial')?>", 'warning');
          return false;
        }

        if ( !isEmptyString( $(this).attr('id') ) ){

          if($objetivos_list.val() == null || $objetivos_list.val().length < 3){
            setFlash('<?=translate('label error3maxObjetivo')?>', 'warning');
            return false;
          } else if($objetivos_list.val() == null || $objetivos_list.val().length > 5){
            setFlash('<?=translate('label error5maxObjetivo')?>', 'warning');
            return false;
          }

          var data = {
            '_title'        : $("#txt__Name").val(),
            '_initialDate'  : $("#txt__InitialDate").val(),
            '_endDate'      : $("#txt__EndDate").val(),
            '_description'  : $("#txt__Description").val(),
            '_actors'       : $friends_list.val(),
            '_initialHour'  : $("#txt__StartHour").val(),
            '_endHour'      : $("#txt__EndHour").val(),
            '_photoCover'   : $('#thumbnail').val(),
            '_userid'       : $("#txt__user").val(),
            '_objetivos'    : $objetivos_list.val(),
            '_events'       : $events_list.val(),
            '_ubications'   : arreglo,
            '_method'       : 'save_new'
          }
          iniciativa._set(data);
        }
    });

  });
  function fechaCorrecta(fecha){
    //Split de las fechas recibidas para separarlas
    var x = fecha.split("-");
    //Cambiamos el orden al formato americano, de esto dd/mm/yyyy a esto mm/dd/yyyy
    fecha = x[1] + "-" + x[0] + "-" + x[2];
    setFlash(fecha, 'warning');
    return fecha;
    //Comparamos las fechas
}
</script>
