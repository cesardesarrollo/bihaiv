<?php 
session_start();
require('../../../core/lang/lang.php');
?>
<div class="modal fade" id="m-info-rol" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content modal-border-radius">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?=translate('Información del experto')?></h4>
      </div>
      <div class="modal-body no-padding">
          <div class="Img-modal">
            <figure class="figure">
                <img src="assets/img/home/PORTADA.jpg" alt="">
            </figure>
          </div>
          <figure class="figure">
              <img class="avatarv2 img-responsive" id="img-expert" alt="">
          </figure>
          <div class="modal-padding">
            <h3><?=translate('Experiencia')?></h3>
            <hr>
            <div class="form-horizontal">
              <div class="form-group">
                <label for="" class="col-sm-3 control-label"><?=translate('Ramo')?>:</label>
                <div class="col-sm-9">
                  <label><span id="ramo"></span></label>
                </div>
              </div>
              <div class="form-group">
                <label for="" class="col-sm-3 control-label"><?=translate('Años de experiencia')?>:</label>
                <div class="col-sm-9">
                  <label class="control-label"><span id="experiencia"></span></label>
                </div>
              </div>
              <div class="form-group">
                <label for="" class="col-sm-3 control-label"><?=translate('Cargo actual')?>:</label>
                <div class="col-sm-9">
                  <label class="control-label"><span id="cargo_actual"></span></label>
                </div>
              </div>
              <div class="form-group">
                <label for="" class="col-sm-3 control-label"><?=translate('Cargo anterior')?>:</label>
                <div class="col-sm-9">
                  <label class="control-label"><span id="cargo_anterior"></span></label>
                </div>
              </div>
            </div>
            <h3><?=translate('Biografía')?></h3>
            <hr>
            <p class="text more" id="bio"></p>
          </div>
      </div>
      <div class="modal-footer">
        <ol class="breadcrumb transparent no-margin no-padding-top no-padding-bottom">
          <li>
            <a href="#">
              <i class="fa fa-facebook facebook icon-xl"></i>
            </a>
          </li>
          <li>
            <a href="#">
              <i class="fa fa-twitter twitter icon-xl"></i>
            </a>
          </li>
          <li>
            <a href="#">
              <i class="fa fa-linkedin linkedin icon-xl"></i>
            </a>
          </li>
        </ol>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<style type="text/css">
  .morecontent span {
    display: none;
  }
  .morelink {
      display: block;
  }
</style>