<?php 
session_start();
require_once('../../../core/lang/lang.php');
require_once('../../../core/modules/index/model/DaoRoles.php');
require_once('../../../core/modules/index/model/DaoUsuarios.php');
require_once('../../../core/app/debuggin.php');
?>
<div id="m-new-topic" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form id="saveDiscusion">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><?=translate('Nuevo tema de discusión')?></h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-xs-12 col-md-6 col-lg-6">
              <div class="form-group">
                <label for="txt__Title">*<?=translate('Título')?></label>
                <input type="text" id="txt__Title" class="form-control" required>
              </div>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-6">
              <div class="form-group">
                <label for="cmb__Role">*<?=translate('Dirigido a')?>:</label>
                <select class="form-control" id="cmb__Role" required>
                  <option value=""><?=translate('Selecciona una opción')?></option>
                  <?php
                  $DaoRoles = new DaoRoles();
                  $roles = $DaoRoles->getAll();
                  foreach ($roles as $rol) {
                    ?><option value="<?php echo $rol->getId(); ?>"><?php echo utf8_decode($rol->getNombre()); ?></option><?php
                  } ?>
                </select>
              </div>
            </div>
            <div class="col-xs-12 col-md-12 col-lg-12">
              <div class="form-group">
                <label for="cmb__Privacy"><?=translate('privacidad')?>:</label>
                <select class="form-control" id="cmb__Privacy" required>
                  <option value=""><?=translate('Selecciona una opción')?></option>
                  <option value="PUBLICO"><?=translate('Público')?></option>
                  <option value="PRIVADO"><?=translate('Privado')?></option>
                </select>
              </div>
            </div>
            <div class="col-xs-12 col-md-12 col-lg-12" style="display:none;" id="container-actores">
              <div class="form-group">
                <label for="cmb__Privacy_"><?=translate('Invitar actores')?>:</label>
                <select class="form-control" multiple id="invitadosSelect" data-live-search="true">
                  <?php
                  // HERE COMES NEW CHALLNGER
                  $DaoUsuarios  = new DaoUsuarios();
                  $usuarios     = $DaoUsuarios -> getAll();
                  ?>
                  <?php foreach ($usuarios as $k => $usuario): ?>
                    <option value="<?= $usuario['id']; ?>"><?php echo utf8_decode($usuario['name']).' '.utf8_decode($usuario['lastname']); ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="col-xs-12 col-md-12 col-lg-12">
              <div class="form-group">
                <label for="txt__Description"><?=translate('Descripción')?></label>
                <textarea class="form-control" id="txt__Description" required></textarea>
              </div>
            </div>
          </div>
           <div id="row-msg" class="col-sm-12 col-md-12 col-lg-12 margin-row-top" style="display:none;">
              <div class="alert" role="alert" id="panel-alert">
                <span id="icon"> </span> <span id="msg"> </span>
              </div>
          </div>
          <input type="hidden" id="txt__user">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal"><?=translate('Cerrar')?></button>
          <button type="submit" class="btn btn-primary"><?=translate('Guardar')?></button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
  var discusion = new Discusiones();
  //SAVE NEW
  $(document).off('submit').on('submit', '#saveDiscusion', function(e){
    /* Selectors */
    $userId = $("#txt__user");
    $title = $("#txt__Title");
    $description = $("#txt__Description");
    $role = $("#cmb__Role");
    $privacy = $("#cmb__Privacy");
    $invitados = $('#invitadosSelect');
    e.preventDefault();
    // Validate inputs
    // Titulo
    if(!$title.val()){
      setFlash('<?=translate("Agrega un titulo antes de continuar")?>', 'warning');
      return false;
    }
    // Roles
    if(!$role.val()){
      setFlash('<?=translate("Selecciona a quien va dirigido antes de continuar")?>', 'warning');
      return false;
    }
    // Privacidad
    if(!$privacy.val()){
      setFlash('<?=translate("Selecciona a quien va dirigido antes de continuar")?>', 'warning');
      return false;
    }else if ($privacy.val() == 'PRIVADO') {
      if( $invitados.selectpicker('val') == null ){
        setFlash('<?=translate("Selecciona por lo menos un actor para invitar al tema")?>', 'warning');
        return false;
      }
    }
    // Descripcion
    if(!$description.val()){
      setFlash('<?=translate("Agrega una descripción antes de continuar")?>', 'warning');
      return false;
    }
    if ( !isEmptyString( $(this).attr('id') ) ){
      var data = {
        '_roles'        : $role.val(),
        '_privacy'      : $privacy.val(),
        '_description'  : $description.val(),
        '_title'        : $title.val(),
        '_userid'       : $userId.val(),
        '_method'       : 'save',
        '_invitados'    : $invitados.selectpicker('val')
      }

      console.log(data);
      discusion._set(data);
    }
  });
  // HERE COMES NEW CHALLENGER
  $(function(){
    // SELECTOR VARS
    $selectPrivacidad = $('#cmb__Privacy');
    $containerActores = $('#container-actores');

    $selectPrivacidad.change(function(){
      if($(this).val() == 'PRIVADO'){
        $containerActores.show();
      }else{
        $containerActores.hide();
      }
    });
    // MULTI SELECT
    $('#invitadosSelect').selectpicker();
  })
</script>
