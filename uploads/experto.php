<?php
require_once 'includes/header.php';
?>
<div class="container-fluid">
    <div class="Wall margin-row-bottom">
        <div class="Wall__header z-depth-1">
            <h3 class="display-inline text-white bolder">Experto</h3>
        </div>
        <div class="Wall__content large padding">
            <div class="row">
                <div class="col-xs-12 col-md-12 col-lg-12" id="box-principal"></div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="nonce" value="<?php echo $_REQUEST['nonce']?>"/>
<input type="file" id="files" name="files[]" multiple="">
<?php
require_once 'includes/footer.php';
?>
