/*
 *	CLASE colaboracion
 *	===============
 */
function Colaboraciones(){
	this.colaboracion = {};
	this.base_url = "api/front/colaboracion.php";
}
/*
 *	TODAS LAS colaboracion POR USUARIO
 *	@params : {id_usuario logueado || usuario visitado}
 *	=====================================================
 */
Colaboraciones.prototype._getall = function(){
	var data = {};
	var self = this;
	//AJAX REQUEST
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		dataType: 'JSON',
		data 	: self.colaboracion
	})
	.done( function( _res ){
		self._draw(_res);
	})
	.fail( function( _err ){
		$("#collaborations").html(_err.responseText);
	})
};
/*
 *	colaboracion POR ID
 *	==========================
 */
Colaboraciones.prototype._byId = function(){
	var self = this;
	console.log( self.colaboracion );
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		async 	: false,
		dataType: 'JSON',
		data 	: self.colaboracion
	})
	.done(function( _res ){
		console.log( 'Obtiene colaboración' );
	})
	.fail(function( _err ){
		console.log( _err );
	})
};

/*
 *	EDITAR NUEVA colaboracion
 *	@params : {info_aportacion}
 *	==============================
 */
Colaboraciones.prototype._update = function(){
	var self = this;
	console.log(self.colaboracion);
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		async 	: false,
		data 	: self.colaboracion,
		dataType: 'JSON'
	})
	.done(function( _res ){
		console.log(_res);
	})
	.fail(function( _err ){
		console.log( _err );
	})
};


Colaboraciones.prototype._set = function( _data ){
	this.colaboracion.estatus 			= _data._estatus || null;
	this.colaboracion.idColaboracion 	= _data._idColaboracion || null;
	this.colaboracion.user_id 			= _data._user_id || null;
	this.colaboracion.method 			= _data._method || null;
	if( this.colaboracion.method === 'all')
	{
		this._getall();
	}
	else if( this.colaboracion.method === 'byId')
	{
		this._byId();
	}
	else if( this.colaboracion.method === 'update')
	{
		this._update();
	}
};

Colaboraciones.prototype._draw = function( _data ){
	if ( _data.length > 0 ){
		var template = Handlebars.compile( $("#collaboration-template").html() );
		var html = template( _data );
		$("#collaborations").html( html );
	} else {
		$("#collaborations").html(_data);
	}
};
