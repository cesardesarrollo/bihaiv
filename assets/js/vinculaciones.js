/*
 *	CLASE Vinculaciones
 *	===============
 */
function Vinculaciones(){
	this.vinculacion = {};
	this.base_url = "api/front/vinculacion.php";
}

/*
 * 	NUEVA VINCULACIÓN
 *	@params : {informacion vinculación}
 *	=======================================
 */
Vinculaciones.prototype._save = function(){
	var self = this;
	var id_user = self.vinculacion.user_id;
	
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		async 	: false,
		data 	: self.vinculacion,
		dataType: 'JSON'
	})
	.done(function( _res ){
		self.vinculacion = {};

		$.each(_res,function(index, value){
			if( value.status ){
				$("#row-msg").slideToggle('slow');
				$("#panel-alert").addClass('alert-success');
				$("#icon").html('<i class="fa fa-check-square"></i>');
				$("#msg").html( value.msg );
			} else {
				$("#row-msg").slideToggle('slow');
				$("#panel-alert").addClass('alert-danger');
				$("#icon").html('<i class="fa fa-times-circle"></i>');
				$("#msg").html( value.msg );
			}
		});

		setTimeout(function(){
			$("#m-sugest-profile").modal('hide');
		}, 3000);

	})
	.fail(function( _err ){
		console.log( _err );
	})
};

/*
 *	SET DATA INICIATIVA
 *	=====================
 */
Vinculaciones.prototype._set = function( _data ){	
	this.vinculacion.vincularA 		= _data._vincularA || null;
	this.vinculacion.vincularCon 	= _data._vincularCon || null;
	this.vinculacion.user_id		= _data._vinculaUserId || null;
	this.vinculacion.receptor_id	= _data._receptor || null;
	this.vinculacion.method 		= _data._method || null;
	this.vinculacion.accion 		= _data._action || null;
	if( this.vinculacion.method === 'save' )
	{
		this._save();
	} 

};
