function Reportar(){
	this.reporte = {};
	this.base_url = 'api/front/reporte.php';
}

Reportar.prototype._report = function(){
	var data = {};
	var self = this;
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		datatype: 'JSON',
		data 	: self.reporte
	})
	.done(function( _res ){
		self.reporte = {};
		_res = JSON.parse(_res);

		if( _res.status ){
			$("#row-msg").slideToggle('fast');
			$("#panel-alert").addClass('alert-success');
			$("#icon").html('<i class="fa fa-check-square"></i>');
			$("#msg").html( _res.msg );
			$("#btnAddEvent").css('display', 'none');
			setTimeout(function(){
				$("#m-report-user").modal('hide');
			}, 2000);

		} else {
			$("#row-msg").slideToggle('fast');
			$("#panel-alert").addClass('alert-danger');
			$("#icon").html('<i class="fa fa-times-circle"></i>');
			$("#msg").html( _res.msg );
			$("#btnAddEvent").css('display', 'none');
			setTimeout(function(){
				$("#m-report-user").modal('hide');
			}, 2000);
		}
	})
	.fail(function( _err ){
		console.log( _err );
	});
};

/*
 *	SET REPORT
 *	============
 */
Reportar.prototype._set = function( _data ){
	this.reporte = {};
	this.reporte.receptor_id = _data._receptor || null;
	this.reporte.comment 	 = _data._msg || null;
	this.reporte.type 		 = _data._type || null;
	this.reporte.method 	 = _data._method || null;
	if( this.reporte.method == 'save'){
		this._report();
	}
}