/*
 *	USER CLASS
 *	=============
 */
function User(){
	this.user = {};
	this.base_url = "api/front/user.php";
}
/*
 *	RETRIEVE DATA USER
 *	======================
 */
User.prototype._getdata = function(){
	var data = {};
	var self = this;
	$.ajax({
		url 	: self.base_url,
		method : 'GET',
		dataType : 'JSON'
	})
	.done( function( _res ){
		data._role = _res.role;
		data._company = _res.company;
		data._description = _res.description;
		self._set( data );
	})
	.fail( function( _err ){
		console.log( _err );
	})

};
/*
 *	SET DATA USER
 *	==================
 */
User.prototype._set = function( _data ) {
	this.user.role = _data._role;
	this.user.company = _data._company;
	this.user.description = _data._description;
	//CALL FUNCTION TO DRAW TEMPLATE
	this._draw();
};
/*
 *	DRAW TEMPLATE
 *	================
 */
User.prototype._draw = function(){
	$("#role").html( this.user.role );
	$("#company").html( this.user.company );
	$("#description").html( this.user.description );
};
