/*
 *	CLASE SUGERENCIAS
 *	===============
 */
function Sugerencias(){
	this.sugerencia = {};
	this.base_url = "api/front/sugerencia.php";
}
/*
 *	TODAS LAS SUGERENCIAS POR USUARIO
 *	@params : {id_usuario logueado || usuario visitado}
 *	=====================================================
 */
Sugerencias.prototype._getall = function(){
	var data = {};
	var self = this;
	//AJAX REQUEST
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		dataType: 'JSON',
		data 	: self.sugerencia
	})
	.done( function( _res ){
		self._draw(_res);
	})
	.fail( function( _err ){
		$("#sugerencia").html(_err.responseText);
		console.log( _err );
	})
};
/*
 *	SET DATA LEAD
 *	==================
 */
Sugerencias.prototype._set = function( _data ){
	this.sugerencia = {};
	this.sugerencia.method 		= _data._method || null;
	this.sugerencia.user_id		= _data._userid || null;

	if( this.sugerencia.method === 'all')
	{
		this._getall();
	}

};
/*
 *	DRAW TEMPLATE HANDLEBARS
 * 	===========================
 */
Sugerencias.prototype._draw = function( _data ){
	if ( _data.length > 0 ){
		var template = Handlebars.compile( $("#sugerencia-template").html() );
		var html = template( _data );
		$("#sugerencia").html( html );
	} else {
		
	}
};


/*
 *	HELPER
 *	=========================
 */
Handlebars.registerHelper('if', function(conditional, options) {
  if(conditional!=="") {
    return options.fn(this);
  } else {
    return options.inverse(this);
  }
});
