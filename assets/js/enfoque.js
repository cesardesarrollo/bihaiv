/*
 *	CLASE ENFOQUE
 *	===============
 */
function Enfoque(){
	this.enfoque = {};
	this.base_url = "api/front/enfoque.php";
	this.userid = '';
}
/*
 *	TODAS LAS ENFOQUE POR USUARIO
 *	@params : {id_usuario logueado || usuario visitado}
 *	=====================================================
 */
Enfoque.prototype._getall = function(){
	var data = {};
	var self = this;
	//AJAX REQUEST
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		dataType: 'JSON',
		data 	: self.enfoque
	})
	.done( function( _res ){
		if( _res.length <= 5 )
			$("#divAddFocus").css('display', 'inline-block');
		self._draw(_res);
	})
	.fail( function( _err ){
		$("#divAddFocus").css('display', 'inline-block');
		$("#focus").html(_err.responseText);
	})
};
/*
 *	ENFOQUE POR ID
 * 	@params : {id_aportacion}
 *	==========================
 */
Enfoque.prototype._byId = function(){
	var self = this;
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		async 	: false,
		dataType: 'JSON',
		data 	: self.enfoque
	})
	.done(function( _res ){
		console.log( _res );
		self._drawById(_res);
	})
	.fail(function( _err ){
		console.log( _err );
	})
};
/*
 * 	NUEVA ENFOQUE
 *	@params : {informacion enfoque}
 *	=======================================
 */

Enfoque.prototype._save = function(){
	var self = this;
	var user_id = self.enfoque.user_id;
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		async 	: false,
		data 	: self.enfoque
	})
	.done(function( _res ){
		var r = JSON.parse(_res);
		if( r.status ){
			$("#row-msg").slideToggle('fast');
			$("#panel-alert").addClass('alert-success');
			$("#icon").html('<i class="fa fa-check-square"></i>');
			$("#msg").html( r.msg );
			$("#btnAddEvent").css('display', 'none');
			setTimeout(function(){
				$("#m-new-focus").modal('hide');
				self.enfoque = {};
				var params = {};
		      	params._method = "all";
		      	params._userid = user_id;
				self._set(params);
			}, 2000);
		} else {
			$("#row-msg").slideToggle('fast');
			$("#panel-alert").addClass('alert-danger');
			$("#icon").html('<i class="fa fa-times-circle"></i>');
			$("#msg").html( r.msg );
			$("#btnAddEvent").css('display', 'none');
			setTimeout(function(){
				$("#m-new-focus").modal('hide');
			}, 2000);
		}
	})
	.fail(function( _err ){
		console.log( _err );
	})
};
/*
 *	EDITAR NUEVA ENFOQUE
 *	@params : {info_aportacion}
 *	==============================
 */
Enfoque.prototype._update = function(){
	var self = this;
	var user_id = self.enfoque.user_id;
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		async 	: false,
		data 	: self.enfoque,
		dataType: 'JSON'
	})
	.done(function( _res ){
		var r = _res;
		if( r.status ){
			$("#row-msg").slideToggle('fast');
			$("#panel-alert").addClass('alert-success');
			$("#icon").html('<i class="fa fa-check-square"></i>');
			$("#msg").html( r.msg );
			$("#btnAddEvent").css('display', 'none');
			setTimeout(function(){
				$("#modal-info-focus").modal('hide');
				self.enfoque = {};
				var params = {};
		      	params._method = "all";
		      	params._userid = user_id;
				self._set(params);
			}, 2000);
		} else {
			$("#row-msg").slideToggle('fast');
			$("#panel-alert").addClass('alert-danger');
			$("#icon").html('<i class="fa fa-times-circle"></i>');
			$("#msg").html( r.msg );
			$("#btnAddEvent").css('display', 'none');
			setTimeout(function(){
				$("#modal-info-focus").modal('hide');
			}, 2000);
		}
	})
	.fail(function( _err ){
		console.log( _err );
	})
};
/*
 *	ELIMINAR ENFOQUE
 *	@param : id_aportacion
 *	=========================
 */
Enfoque.prototype._delete = function(){
	var self = this;
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		async 	: false,
		dataType: 'JSON',
		data 	: self.enfoque
	})
	.done(function( _res ){
		console.log( _res );
	})
	.fail(function( _err ){
		console.log( _err );
	})
};
Enfoque.prototype._update_rating = function(){
	var self = this;
	$.ajax({
		url 			: self.base_url,
		method 		: 'POST',
		async 		: false,
		data 			: self.enfoque,
		dataType	: 'JSON'
	})
	.done(function( _res ){
		setFlash(_res.msg, _res.class)
		console.log( _res );
	})
	.fail(function( _err ){
		console.log( _err );
	})
};
/*
 *	SET DATA ENFOQUE
 *	==================
 */
Enfoque.prototype._set = function( _data ){
	this.enfoque = {};
	this.enfoque.id 			= _data._id || null;
	this.enfoque.descripcion 	= _data._description || null;
	this.enfoque.tipo_enfoque 	= _data._tipo_enfoque || null;
	this.enfoque.method 		= _data._method || null;
	this.enfoque.qualification 	= _data._qualification || null;
	this.enfoque.user_id		= _data._userid || null;
	this.enfoque.tipo		 	= _data._type || null;
	this.enfoque.post_id		= _data._post_id || null;
	this.userid 				= _data._userid;
	if( this.enfoque.method === 'all')
	{
		this._getall();
	}
	else if( this.enfoque.method === 'byId')
	{
		this._byId();
	}
	else if( this.enfoque.method === 'save' )
	{
		this._save();
	}
	else if( this.enfoque.method === 'edit')
	{
		this._update();
	}
	else if( this.enfoque.method === 'delete')
	{
		this._delete()
	}
	else if( this.enfoque.method === 'update_rating')
	{
		this._update_rating();
	}

};
/*
 *	DRAW TEMPLATE HANDLEBARS
 * 	===========================
 */
Enfoque.prototype._draw = function( _data ){
	if ( _data.length > 0){
		var template = Handlebars.compile( $("#focus-template").html() );
		var html = template( _data );
		$("#focus").html( html ).fadeIn();
	} else {
		
	}
};

Enfoque.prototype._drawById = function( _data ){
	var self = this;
	$("#modal-launcher").load('views/user/modals/m-info-focus.php?token=' + Math.random(), function(e){
		$("#modal-info-focus").modal({
			show : true,
			backdrop : 'static',
			keyboard : false
		});
		$("#txt__Description").val( _data.texto );
		$("#txt__user").val( self.userid );
		$("#txt__post").val( _data.Post_idPost );
		$("#txt__id").val( _data.id );
	})
}

/*
 *	HANDLEBARS HELPER
 *	===================
 */
Handlebars.registerHelper('ifEnfoque', function(_statusPerCreationDate, _statusPerDay, options){
	if(_statusPerCreationDate === "true" && _statusPerDay === "true"){
		return options.fn(this);
	} else {
		return options.inverse(this);
	}
})
