
<?php
require_once '../core/modules/index/model/DaoEstados.php';
require_once '../core/modules/index/model/DaoExperto.php';
require_once '../core/modules/index/model/DTO/Experto.php';
require_once '../core/modules/index/model/DaoRedesExperto.php';
require_once '../core/modules/index/model/DTO/RedesExperto.php';

require_once '../core/app/defines.php';
require_once '../core/controller/Database.php';
require_once '../core/controller/Executor.php';
require_once '../core/controller/Model.php';
require_once '../core/modules/index/model/UserData.php';

if (isset($_POST['action']) && $_POST['action'] == "updatePage") {
    $DaoExperto = new DaoExperto();
    $DaoEstados = new DaoEstados();
    $UserData = new UserData();

    $nombre = "";
    $email = "";
    $ubicacion = "";
    $organizacion = "";
    $tipoOrganizacion = "";
    $ramoExperiencia = "";
    $aniosExperiencia = "";
    $cargoActual = "";
    $cargoAnterior = "";
    $bibliografia = "";
    $imagen = "";
    $ban=0;
    $telefono="";
    $facebook="";
    $twitter="";
    $linkedin="";
    if (isset($_POST['nonce']) && strlen($_POST['nonce']) > 0) {
        $experto = $DaoExperto->getByNonce($_REQUEST['nonce']);
        if($experto->getId()>0) {
            $ban=1;
            $user = $UserData->getById($experto->getUsuarios_idUsuarios());
            $nombre = $user->name;
            $email = $user->email;
            $llaveImagen= $user->imagen;

            $ubicacion = $experto->getUbicacion();
            $organizacion = $experto->getOrganizacion();
            $tipoOrganizacion = $experto->getTipoOrganizacion();
            $ramoExperiencia = $experto->getRangoExperiencia();
            $aniosExperiencia = $experto->getAniosExperiencia();
            $cargoActual = $experto->getCargoActual();
            $cargoAnterior = $experto->getCargoAnterior();
            $bibliografia = $experto->getBibliografia();

            if (strlen($user->imagen) > 0) {
                $imagen = 'style="background-image: URL(../admin/files/' . $user->imagen . ')"';
            }
            ?>
        <form id="form_experto">
            <div class="col-xs-2 col-md-2 col-lg-2">
                <output id="list" class="col-xs-12 col-md-12 col-lg-12 box-img" style="padding:0;"></output>
                <!--<input  class="col-xs-12 col-md-12 col-lg-12 btn btn-primary"  type="file" id="imagen" name="files[]" multiple="" value="Cargar imagen">-->
                <button  class="col-xs-12 col-md-12 col-lg-12 btn btn-primary" onclick="mostrarFinder()">Cargar imagen</button>
                <script>
                    function archivo(evt) {
                        var files = evt.target.files; // FileList object
                        // Obtenemos la imagen del campo "file".
                        for (var i = 0, f; f = files[i]; i++) {
                        //Solo admitimos imágenes.
                        if (!f.type.match('image.*')) {
                            continue;
                        }
                        var reader = new FileReader();
                        reader.onload = (function(theFile) {
                            return function(e) {
                                // Insertamos la imagen
                                document.getElementById("list").innerHTML = ['<img class="img-responsive" style="margin:0" src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');
                            };
                        })(f);
                        reader.readAsDataURL(f);
                        }
                    }
                    
                    document.getElementById('files').addEventListener('change', archivo, false);
                </script>
            </div>
            <div class="col-xs-10 col-md-10 col-lg-10 seccion">
                <div class="form-group">
                    <label for="nombre"><span class="requerido">*</span>Nombre:</label>
                    <input type="text" class="form-control" id="nombre" maxlength="100" required value="<?php echo $nombre; ?>">
                </div>
                <div class="form-group">
                    <label for="email"><span class="requerido">*</span>Email:</label>
                    <input type="email" class="form-control" id="email" required value="<?php echo $email; ?>">
                </div>
                <div class="form-group">
                    <label for="pass"><span class="requerido">*</span>Contraseña:</label>
                    <input type="password" class="form-control" id="pass" minlength="8"  required>
                </div>
                <div class="form-group">
                    <label for="confirmar"><span class="requerido">*</span>Confirmar contraseña:</label>
                    <input type="password" class="form-control" id="confirmar" minlength="8" required>
                </div>
                <div class="form-group">
                    <label for="ubicacion"><span class="requerido">*</span>Ubicación:</label>
                    <select class="form-control" id="ubicacion">
                        <option value="0">Seleccione uno...</option>
                        <?php
                        foreach ($DaoEstados->getAll() as $estado) {
                            ?>
                            <option value="<?php echo $estado->getId() ?>" <?php if ($ubicacion == $estado->getId()) { ?> selected="selected" <?php } ?>><?php echo $estado->getNombre() ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="organizacion"><span class="requerido">*</span>Organización a la que pertenece:</label>
                    <input type="text" class="form-control" id="organizacion"  required  maxlength="70" value="<?php echo $organizacion; ?>">
                </div>
                <div class="form-group">
                    <label for="tipo-organizacion"><span class="requerido">*</span>Tipo de organización:</label>
                    <input type="text" class="form-control" id="tipo-organizacion"  required value="<?php echo $tipoOrganizacion; ?>">
                </div>
            </div>
            <div class="col-xs-12 col-md-12 col-lg-12 seccion" id="box-informacion-experto">
                <div class="form-group">
                    <label for="ramo-experiencia"><span class="requerido">*</span>Ramo de experiencia:</label>
                    <input type="text" class="form-control" id="ramo-experiencia" maxlength="100" required value="<?php echo $ramoExperiencia; ?>">
                </div>
                <div class="form-group">
                    <label for="anios-experiencia"><span class="requerido">*</span>Años de experiencia en el sector:</label>
                    <input type="text" class="form-control" id="anios-experiencia"  required value="<?php echo $aniosExperiencia; ?>">
                </div>
                <div class="form-group">
                    <label for="cargo-actual"><span class="requerido">*</span>Cargo actual</label>
                    <input type="text" class="form-control" id="cargo-actual" maxlength="70" required value="<?php echo $cargoActual; ?>">
                </div>
                <div class="form-group">
                    <label for="cargo-anterior"><span class="requerido"> </span>Cargo anterior</label>
                    <input type="text" class="form-control" id="cargo-anterior" maxlength="70" required value="<?php echo $cargoAnterior; ?>">
                </div>
            </div>
            <div class="col-xs-12 col-md-12 col-lg-12 seccion" id="box-informacion-experto">
                <div class="form-group">
                    <label for="bibliografia"><span class="requerido">*</span>Bibliografía:</label>
                    <textarea  class="form-control" id="bibliografia" maxlength="500" required placeholder="(500 caracteres)"><?php echo $bibliografia; ?></textarea>
                </div>
            </div>
            <div class="col-xs-12 col-md-12 col-lg-12 seccion" id="box-informacion-experto">
                <div class="form-group">
                    <i class="fa fa-facebook-square" aria-hidden="true"></i> Facebook</label>
                    <input type="text" class="form-control" id="facebook" placeholder="Ej. http://www.facebook.com/mifanpage" value="<?php echo $facebook; ?>">
                </div>
                <div class="form-group">
                    <i class="fa fa-twitter-square" aria-hidden="true"></i> Twitter</label>
                    <input type="text" class="form-control" id="twitter" placeholder="Ej. http://www.twitter.com/miusuario" value="<?php echo $twitter; ?>">
                </div>
                <div class="form-group">
                    <i class="fa fa-linkedin-square" aria-hidden="true"></i> Linkedln</label>
                    <input type="text" class="form-control" id="linkedin" placeholder="Ej. http://www.linkedin.com/in/miperfil"value="<?php echo $linkedin; ?>">
                </div>
                <div class="form-group">
                    <i class="fa fa-phone-square" aria-hidden="true"></i> Teléfono</label>
                    <input type="text" class="form-control" id="telefono" required value="<?php echo $telefono; ?>">
                </div>
            </div>
            <button type="submit" class="btn btn-primary pull-right">Guardar</button>
            <input type="hidden" id="id-experto" value="<?php echo $experto->getId()?>">
        </form>
        
        <script>
            $(function(){
                $('#form_experto').on('submit', function(e){
                e.preventDefault();

                    // Valida cadenas para redes
                    facebook = $('#facebook').val();
                    twitter  = $('#twitter').val();
                    linkedin = $('#linkedin').val();

                    var str_http = /http/;

                    if(facebook!==""){
                        if(!str_http.test(facebook)){
                            alert("debes ingresar toda la URL de tu cuenta de Facebook ej. http://www.facebook.com/mifanpage");
                            $('#twitter').focus();
                            return false;
                        }
                    }
                    if(twitter!==""){
                        if(!str_http.test(twitter)){
                            alert("debes ingresar toda la URL de tu cuenta de Twitter ej. http://www.twitter.com/miusuario");
                            $('#facebook').focus();
                            return false;
                        }
                    }
                    if(linkedin!==""){
                        if(!str_http.test(linkedin)){
                            alert("debes ingresar toda la URL de tu cuenta de LinkedIn ej. http://www.linkedin.com/in/miperfil");
                            $('#linkedin').focus();
                            return false;
                        }
                    }
                saveExperto();
                })
            })
        </script>

<?php
        }
    }
    if($ban==0){
        ?>
        <div class="col-xs-12 col-md-12 col-lg-12 seccion">
            <p style="margin-top: 50px;">Clave de acceso no válida</p>
            <a href="<?php echo APP_PATH; ?>home/login.php"><button type="submit" class="btn btn-primary">Salir</button></a>
        </div>
        <?php
    }
}

if (isset($_GET['action']) && $_GET['action'] == "uploadAttachment") {
    if (count($_FILES) > 0) {
        if ($_FILES["file"]["error"] > 0) {
            echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
        } else {

            $DaoExperto = new DaoExperto();
            $experto = $DaoExperto->getByNonce($_GET['nonce']);

            $UserData = new UserData();
            $user = $UserData->getById($experto->getUsuarios_idUsuarios());
            if (strlen($user->imagen) > 0) {
                //Eliminar la foto anterior
                unlink('../admin/files/' . $user->imagen);
            }
            //Agregar nuevo foto
            $namefile = $DaoExperto->generarKey() . ".jpg";
            $UserData->imagen = $namefile;
            $UserData->id = $experto->getUsuarios_idUsuarios();
            $UserData->updateImagenUser();

            $ubicacion = "../admin/files/" . $namefile;
            move_uploaded_file($_FILES["file"]["tmp_name"], $ubicacion);
        }
    } elseif (isset($_GET['action'])) {

        if (isset($_FILES["file"]["error"]) && $_FILES["file"]["error"] > 0) {

            echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
        } else {


            if (isset($_GET['base64'])) {
                // If the browser does not support sendAsBinary ()
                $dataFile = file_get_contents('php://input');
                if (isset($_POST['fileExplorer'])) {
                    //If the browser support readAsArrayBuffer ()
                    //PHP handles spaces in base64 encoded string differently
                    //so to prevent corruption of data, convert spaces to +
                    $dataFile = $_POST['fileExplorer'];
                    $dataFile = str_replace(' ', '+', $dataFile);
                }
                $content = base64_decode($dataFile);
            }

            $DaoExperto = new DaoExperto();
            $experto = $DaoExperto->getByNonce($_GET['nonce']);

            $UserData = new UserData();
            $user = $UserData->getById($experto->getUsuarios_idUsuarios());
            if (strlen($user->imagen) > 0) {
                //Eliminar la foto anterior
                unlink('../admin/files/' . $user->imagen);
            }
            //Agregar nuevo foto
            $namefile = $DaoExperto->generarKey() . ".jpg";
            $UserData->imagen = $namefile;
            $UserData->id = $experto->getUsuarios_idUsuarios();
            $UserData->updateImagenUser();

            $ubicacion = "../admin/files/" . $namefile;
            file_put_contents($ubicacion, $content);
            echo $namefile;
        }
    }
}

if(isset($_POST['action']) && $_POST['action']=="updateNonce"){
   $DaoExperto= new DaoExperto();
   $experto=$DaoExperto->getById($_POST['id']);
   $experto->setNonce('');
   $experto->setAcepto(1);
   $DaoExperto->update($experto);
}

if(isset($_POST['action']) && $_POST['action']=="saveRedes"){
    $DaoRedesExperto= new DaoRedesExperto();
    $RedesExperto=$DaoRedesExperto->getByNombre($_POST['id'],'facebook');
    if($RedesExperto->getId()==0){
       $RedesExperto= new RedesExperto();
       $RedesExperto->setNombre('facebook');
       $RedesExperto->setUrl($_POST['urlFacebook']);
       $RedesExperto->setExperto_idExperto($_POST['id']);
       $DaoRedesExperto->add($RedesExperto);
    }else{
       $RedesExperto->setUrl($_POST['urlFacebook']);
       $DaoRedesExperto->update($RedesExperto);
    }

    $RedesExperto=$DaoRedesExperto->getByNombre($_POST['id'],'twitter');
    if($RedesExperto->getId()==0){
       $RedesExperto= new RedesExperto();
       $RedesExperto->setNombre('twitter');
       $RedesExperto->setUrl($_POST['urlTwitter']);
       $RedesExperto->setExperto_idExperto($_POST['id']);
       $DaoRedesExperto->add($RedesExperto);
    }else{
       $RedesExperto->setUrl($_POST['urlTwitter']);
       $DaoRedesExperto->update($RedesExperto);
    }

    $RedesExperto=$DaoRedesExperto->getByNombre($_POST['id'],'linkedin');
    if($RedesExperto->getId()==0){
       $RedesExperto= new RedesExperto();
       $RedesExperto->setNombre('linkedin');
       $RedesExperto->setUrl($_POST['urlLinkedin']);
       $RedesExperto->setExperto_idExperto($_POST['id']);
       $DaoRedesExperto->add($RedesExperto);
    }else{
       $RedesExperto->setUrl($_POST['urlLinkedin']);
       $DaoRedesExperto->update($RedesExperto);
    }
    echo $_POST['id'];
}
