</div>
</div><!--END SITE CANVAS-->
</div>		




<!--SCRIPTS-->
<script src="../res/jquery/jquery.min.js"></script>
<script src="../res/bootstrap/js/bootstrap.min.js"></script>
<script src="../res/owl/owl.carousel.min.js"></script>
<script src="../res/handlebars/handlebars.js"></script>
<script src="../res/callendar/lib/moment.min.js"></script>
<script src="../res/callendar/lib/jquery-ui.custom.min.js"></script>
<script src="../res/callendar/fullcalendar.min.js"></script>
<script src="js/scripts.js"></script>
<?php
if (file_exists("js/" . $pagina . ".js")) {
    ?>
    <script src="js/<?php echo $pagina; ?>.js?a=2"></script> 
    <?php
}
?>
<script>
    $(function () {

        // Toggle Nav on Click
        $('.toggle-nav').click(function () {
            // Calling a function in case you want to expand upon this.
            toggleNav();
        });


    });


    /*========================================
     =            CUSTOM FUNCTIONS            =
     ========================================*/
    function toggleNav() {
        if ($('#site-wrapper').hasClass('show-nav')) {
            // Do things on Nav Close
            $('#site-wrapper').removeClass('show-nav');
        } else {
            // Do things on Nav Open
            $('#site-wrapper').addClass('show-nav');
        }

        //$('#site-wrapper').toggleClass('show-nav');
    }
</script>
</body>
</html>