<?php
header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
$pagina = $_SERVER['SCRIPT_FILENAME'];
$pagina = substr($pagina, 0, strpos($pagina, ".php"));
while (strpos($pagina, "/") !== false) {
    $pagina = substr($pagina, strpos($pagina, "/") + 1);
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <title>Bihaiv - Panel de control</title>
        <link href='https://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="../res/icons/css/font-awesome.min.css">
        <link rel="stylesheet" href="../res/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../res/owl/assets/owl.carousel.css">
        <link rel="stylesheet" href="../res/callendar/fullcalendar.min.css">
        <link rel="stylesheet" href="../assets/css/base.css">
        <?php
        if (file_exists("css/" . $pagina . ".css")) {
            ?>
            <link rel="stylesheet" href="css/<?php echo $pagina; ?>.css?a=2">
            <?php
        }
        ?>
    </head>
    <body>
        <section class="Profile__header z-depth-1">
            <div class="container-fluid">
                <div class="row">
                    <div class="box-logo">
                            <figure class="figure">
                                <img src="../assets/img/home/logo_behaiv.png" alt="Behaiv.com" height="50">
                            </figure>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--SITE WRAPPER-->
    <div class="site-wrapper">
        <div class="site-canvas">

            <div class="message-alert z-depth-3">
                <div class="icon">
                    <i class=""></i>
                </div>
                <div class="message">
                    <p></p>
                </div>
            </div>
            <!--CONTENT APPLICATION-->
            <div class="content-app">