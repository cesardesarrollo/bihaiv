$(document).ready(function () {
    updatePage()

})

function updatePage() {
    var params = new Object()
    params.action = "updatePage";
    params.nonce = $('#nonce').val()
    $.post("experto_aj.php", params, function (resp) {
        $('#box-principal').html(resp)
    })
}

function updateNonce(idExperto) {
    var params = new Object()
    params.action = "updateNonce";
    params.id = idExperto
    $.post("experto_aj.php", params, function (resp) {
           alert("Datos guardados con éxito")
           window.location = "../home/login.php"
    })
}


function saveExperto() {
    var params = new Object()
    params.method = "edit"
    params.id = $('#id-experto').val()
    params.nonce = $('#nonce').val()
    params.nombre = $('#nombre').val()
    params.email = $('#email').val()
    params.ubicacion = $('#ubicacion option:selected').val()
    params.organizacion = $('#organizacion').val()
    params.tipoOrganizacion = $('#tipo-organizacion').val()
    params.ramoExperiencia = $('#ramo-experiencia').val()
    params.aniosExperiencia = $('#anios-experiencia').val()
    params.cargoActual = $('#cargo-actual').val()
    params.cargoAnterior = $('#cargo-anterior').val()
    params.bibliografia = $('#bibliografia').val()
    params.password = $('#pass').val()
    params.acepto = 1
    var error = 0
    var errores = new Array()
    if ($('#nombre').val().length == 0) {
        error = "Nombre requerido"
        errores.push(error);
    }
    if ($('#email').val().length == 0) {
        error = "Email requerido"
        errores.push(error);
    }
    if ($('#ubicacion option:selected').val() == 0) {
        error = "Ubicación requerido"
        errores.push(error);
    }
    if($('#pass').val().length<8){
        error = "La contraseña tiene que ser mayor a 8 caracteres"
        errores.push(error);
    }
    if($('#pass').val()!=$('#confirmar').val()){
        error = "Las contraseñas no coinciden"
        errores.push(error);
    }
    if ($('#organizacion').val().length == 0) {
        error = "Organización requerido"
        errores.push(error);
    }
    if ($('#tipo-organizacion').val().length == 0) {
        error = "Tipo de organización requerido"
        errores.push(error);
    }
    if ($('#ramo-experiencia').val().length == 0) {
        error = "Ramo de experiencia requerido"
        errores.push(error);
    }
    if ($('#anios-experiencia').val().length == 0) {
        error = "Años de experiencia requerido"
        errores.push(error);
    }
    if ($('#cargo-actual').val().length == 0) {
        error = "Cargo actual requerido"
        errores.push(error);
    }
    if ($('#bibliografia').val().length == 0) {
        error = "Bibliografía requerido"
        errores.push(error);
    }

    if (errores.length == 0) {
            $.post("../api/admin/expertos.php", params, function (resp) {
                saveRedes(resp.id)

            },"json")
    } else {
        $string="Error\n\n";
        $.each(errores, function(key, val) {
            $string+="-"+val+"\n"
        })
        alert($string)
    }
}

function saveRedes(id) {
    var params = new Object()
    params.action = "saveRedes";
    params.id = id
    params.urlFacebook = $('#facebook').val()
    params.urlTwitter = $('#twitter').val()
    params.urlLinkedin = $('#linkedin').val()
    $.post("experto_aj.php", params, function (resp) {
        updateNonce(resp);
    })
}


//Funciones para subir los archivos al serever
function mostrarFinder() {
    $('#files').click()
}

if ($('#files').length) {
    document.getElementById('files').addEventListener('change', handleFileSelect, false);
}


function handleFileSelect(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    //Check for the various File API support.
    if (window.File && window.FileReader && window.FileList && window.Blob) {
        // Great success! All the File APIs are supported.

        // FileList object

        if (evt.dataTransfer) {
            var files = evt.dataTransfer.files
        } else {
            var files = evt.target.files;
        }

        var count = 0;
        var bin, name, type, size


        //Funcion loadStartImg
        var loadStartImg = function () {
        }

        //Funcion loadEndImg
        var loadEndImg = function (e) {
            var xhr
            if (window.XMLHttpRequest) {
                xhr = new XMLHttpRequest();
            } else if (window.ActiveXObject) {
                xhr = new ActiveXObject("Microsoft.XMLHTTP");
            }
            //var bin = reader.result;

            // progress bar loadend
            var eventSource = xhr.upload || xhr;
            eventSource.addEventListener("progress", function (e) {
                var pc = parseInt((e.loaded / e.total * 100));
                //$(ObjX).text("Adjuntando...")
                var mns = 'Cargando ...' + pc + '%'
                if (pc == 100) {
                    //$(ObjX).text("Adjuntar archivos")
                    //ocultar_error_layer()
                }
            }, false);



            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4 && xhr.status == 200) {


                    console.log(xhr);
                    $('#files').val('')
                    $('.box-img').attr('style','background-image: URL(files/'+xhr.responseText+')')
                    $('#llave-imagen').val(xhr.responseText)
                }
            }


            xhr.open('POST', 'experto_aj.php?action=uploadAttachment&nonce='+$('#nonce').val(), true);
            var boundary = 'xxxxxxxxx';
            var body = '--' + boundary + "\r\n";
            body += "Content-Disposition: form-data; name='upload'; filename='" + name + "'\r\n";
            body += "Content-Type: application/octet-stream\r\n\r\n";
            body += bin + "\r\n";
            body += '--' + boundary + '--';
            xhr.setRequestHeader('content-type', 'multipart/form-data; boundary=' + boundary);
            // Firefox 3.6 provides a feature sendAsBinary ()
            if (xhr.sendAsBinary != null) {
                xhr.sendAsBinary(body);
                // Chrome 7 sends data but you must use the base64_decode on the PHP side
            } else {

                xhr.open('POST', 'experto_aj.php?action=uploadAttachment&base64=ok&FileName=' + name + '&TypeFile=' + type+'&nonce='+$('#nonce').val(), true);
                xhr.setRequestHeader('UP-FILENAME', utf8_encode(name));
                xhr.setRequestHeader('UP-SIZE', size);
                xhr.setRequestHeader('UP-TYPE', type);
                //Encode BinaryString to base64
                if (reader.readAsBinaryString) {
                    xhr.send(window.btoa(bin));
                } else {
                    xhr.send("fileExplorer=" + window.btoa(bin));
                }
            }

            if (status) {
                //document.getElementById(status).innerHTML = '';
            }
        }

        //Funcion loadErrorImg
        var loadErrorImg = function (evt) {
            switch (evt.target.error.code) {
                case evt.target.error.NOT_FOUND_ERR:
                    setFlash('File Not Found!', 'warning');
                    break;
                case evt.target.error.NOT_READABLE_ERR:
                    setFlash('File is not readabl33e', 'warning');
                    break;
                case evt.target.error.ABORT_ERR:
                    break; // noop
                default:
                    setFlash('An error occurred reading this file.', 'warning');
            }
            ;
        }

        //Loop through the FileList and render image files as thumbnails.
        for (var i = 0, f; f = files[i]; i++) {

             // Only process image files.
             if (!f.type.match('image.*')) {
             continue;
             }

            var reader = new FileReader();
            var preview = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function (theFile) {
                return function (e) {
                    name = theFile.name
                    type = theFile.type
                    size = theFile.size

                    if (reader.readAsBinaryString) {
                        bin = e.target.result
                    } else {
                        //Explorer
                        //Convert ArrayBuffer to BinaryString
                        bin = "";
                        bytes = new Uint8Array(reader.result);
                        var length = bytes.byteLength;
                        for (var i = 0; i < length; i++) {
                            bin += String.fromCharCode(bytes[i]);
                        }
                    }
                };
            })(f);

            preview.onload = (function (theFile) {
                return function (e) {
                    // Render thumbnail.
                };
            })(f);


            if (reader.readAsBinaryString) {
                //Read in the image file as a binary string.
                reader.readAsBinaryString(f);
            } else {
                //Explorer
                //Contendrá los datos del archivo/objeto BLOB como un objeto ArrayBuffer.
                reader.readAsArrayBuffer(f)
            }


            //Read in the image file as a data URL.
            preview.readAsDataURL(f);

            // Firefox 3.6, WebKit
            if (reader.addEventListener) {
                //IE 10
                reader.addEventListener('loadend', loadEndImg, false);
                reader.addEventListener('loadstart', loadStartImg, false);
                if (status != null) {
                    reader.addEventListener('error', loadErrorImg, false);
                }

                // Chrome 7
            } else {
                reader.onloadend = loadEndImg;
                reader.onloadend = loadStartImg;
                if (status != null) {
                    reader.onerror = loadErrorImg;
                }
            }
        }

    } else {
        setFlash('The File APIs are not fully supported in this browser.', 'warning');
    }
}


function utf8_encode(argString) {

    if (argString === null || typeof argString === 'undefined') {
        return '';
    }

    var string = (argString + ''); // .replace(/\r\n/g, "\n").replace(/\r/g, "\n");
    var utftext = '',
            start, end, stringl = 0;

    start = end = 0;
    stringl = string.length;
    for (var n = 0; n < stringl; n++) {
        var c1 = string.charCodeAt(n);
        var enc = null;

        if (c1 < 128) {
            end++;
        } else if (c1 > 127 && c1 < 2048) {
            enc = String.fromCharCode(
                    (c1 >> 6) | 192, (c1 & 63) | 128
                    );
        } else if ((c1 & 0xF800) != 0xD800) {
            enc = String.fromCharCode(
                    (c1 >> 12) | 224, ((c1 >> 6) & 63) | 128, (c1 & 63) | 128
                    );
        } else { // surrogate pairs
            if ((c1 & 0xFC00) != 0xD800) {
                throw new RangeError('Unmatched trail surrogate at ' + n);
            }
            var c2 = string.charCodeAt(++n);
            if ((c2 & 0xFC00) != 0xDC00) {
                throw new RangeError('Unmatched lead surrogate at ' + (n - 1));
            }
            c1 = ((c1 & 0x3FF) << 10) + (c2 & 0x3FF) + 0x10000;
            enc = String.fromCharCode(
                    (c1 >> 18) | 240, ((c1 >> 12) & 63) | 128, ((c1 >> 6) & 63) | 128, (c1 & 63) | 128
                    );
        }
        if (enc !== null) {
            if (end > start) {
                utftext += string.slice(start, end);
            }
            utftext += enc;
            start = end = n + 1;
        }
    }

    if (end > start) {
        utftext += string.slice(start, stringl);
    }

    return utftext;
}
