function mostrarModal(html){
    $("#myModal").modal({
        show : true,
        backdrop : 'static',
        keyboard : false
    })
    $("#myModal").html(html)
}


function ocultarModal(){
    $("#myModal").modal('hide')
}
