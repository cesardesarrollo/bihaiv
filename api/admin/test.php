<!DOCTYPE html>
<html lang="en">
    <head></head>
    <body>
        <!--SCRIPTS-->
        <script src="../../res/jquery/jquery.min.js"></script>
        <script>
            /*
             *	CLASE APORTACIONES
             *	===============
             */
            function Aportacion(){
                    this.aportacion = {};
                    this.base_url = "roles.php";
            }
            /*
             *	TODAS LAS APORTACIONES POR USUARIO
             *	@params : {id_usuario logueado || usuario visitado}
             *	=====================================================
             */
            Aportacion.prototype._getall = function(){
                    var data = {};
                    var self = this;
                    //AJAX REQUEST
                    $.ajax({
                            url 	: self.base_url,
                            method 	: 'POST',
                            //dataType: 'JSON',
                            data 	: self.aportacion
                    })
                    .done( function( _res ){
                            //self._draw(_res);
                            $('body').html('<pre>'+_res+'</pre>')
                    })
                    .fail( function( _err ){
                            console.log( _err );
                    })
            };
            /*
             *	APORTACION POR ID
             * 	@params : {id_aportacion}
             *	==========================
             */
            Aportacion.prototype._byId = function(){
                    var self = this;
                    $.ajax({
                            url 	: self.base_url,
                            method 	: 'POST',
                            async 	: false,
                            //dataType: 'JSON',
                            data 	: self.aportacion
                    })
                    .done(function( _res ){
                            //console.log( _res );
                      $('body').html('<pre>'+_res+'</pre>')
                    })
                    .fail(function( _err ){
                            console.log( _err );
                    })
            };
            /*
             * 	NUEVA APORTACION
             *	@params : {informacion aportacion}
             *	=======================================
             */
            Aportacion.prototype._save = function(){
                    var self = this;
                    $.ajax({
                            url 	: self.base_url,
                            method 	: 'POST',
                            async 	: false,
                            data 	: self.aportacion,
                            //dataType: 'JSON'
                    })
                    .done(function( _res ){
                            $('body').html('<pre>'+_res+'</pre>')
                    })
                    .fail(function( _err ){
                            console.log( _err );
                    })
            };
            /*
             *	EDITAR NUEVA APORTACION
             *	@params : {info_aportacion}
             *	==============================
             */
            Aportacion.prototype._edit = function(){
                    var self = this;
                    $.ajax({
                            url 	: self.base_url,
                            method 	: 'POST',
                            async 	: false,
                            data 	: self.aportacion,
                           // dataType: 'JSON'
                    })
                    .done(function( _res ){
                           $('body').html('<pre>'+_res+'</pre>')
                    })
                    .fail(function( _err ){
                            console.log( _err );
                    })
            };
            /*
             *	ELIMINAR APORTACION
             *	@param : id_aportacion
             *	=========================
             */
            Aportacion.prototype._delete = function(){
                    var self = this;
                    $.ajax({
                            url 	: self.base_url,
                            method 	: 'POST',
                            async 	: false,
                           // dataType: 'JSON',
                            data 	: self.aportacion
                    })
                    .done(function( _res ){
                           $('body').html('<pre>'+_res+'</pre>')
                    })
                    .fail(function( _err ){
                            console.log( _err );
                    })
            };
            /*
             *	SET DATA LEAD
             *	==================
             */
            Aportacion.prototype._set = function( _data ){
                    this.aportacion.nombre 		= _data._title || null;
                    this.aportacion.descripcion = _data._description || null;
                    this.aportacion.method 		= _data._method || null;
                    this.aportacion.id 		= _data._id || null;


                    if( this.aportacion.method === 'all')
                    {
                            this._getall();
                    }
                    else if( this.aportacion.method === 'byId')
                    {
                            this._byId();
                    }
                    else if( this.aportacion.method === 'save' )
                    {
                            this._save();
                    } 
                    else if( this.aportacion.method === 'edit')
                    {
                            this._edit();
                    }
                    else if( this.aportacion.method === 'delete')
                    {
                            this._delete()
                    }

            };
            /*
             *	DRAW TEMPLATE HANDLEBARS
             * 	===========================
             */
            Aportacion.prototype._draw = function( _data ){
                    var template = Handlebars.compile( $("#lead-template").html() );
                    var html = template( _data );
                    $("#leads").html( html );
            };
            
            
            var s= new Aportacion()
            var params= new Object()
<<<<<<< HEAD
                params._title="6666";
                params._description="fdsfsdfsdf";
                params._method="delete"
=======
                params._title="dfsdf";
                params._description="sdfsdf";
                params._method="save"
>>>>>>> bd0e5dfad196b471b12bd479ad70def261db999b
                params._id="1"
                
                s._set(params)

        </script>
    </body>
</html>