<?php
session_start();
require_once('../../core/autoload.php');
require_once('../../core/app/debuggin.php');

require_once('../..//core/modules/index/model/DaoOrganizacion.php');
require_once('../..//core/modules/index/model/DTO/Organizacion.php');


$DaoOrganizacion = new DaoOrganizacion();
$Organizacion = new Organizacion();

$action = $_POST['method'];
switch($action){

  case "update_rating":
    $toSend = $_POST;
    $org = $DaoOrganizacion->getByUserId( $_POST['user_id'] );
    unset($toSend['user_id']);
    $toSend['organizacion_id'] = $org -> id;
    $id = $DaoOrganizacion -> updateRating($toSend);
    if( $id > 0 ){
      json(array(
        'status'      => true,
        'msg'         => "Calificación guardada",
        'class'       => 'success'
      ));
    }else{
      json( array(
        'status'  => false,
        'msg'     => "Ah ocurrido un error intentando calificar esta aportación, intentalo más tarde.",
        'class'   => 'warning'
      ));
    }
  break;

  case "get_rating":
    $toSend = $_POST;
    $org = $DaoOrganizacion->getByUserId( $_POST['user_id'] );
    unset($toSend['user_id']);
    $toSend['organizacion_id'] = $org -> id;
    $DaoOrganizacion -> getRating($toSend);
    json($DaoOrganizacion -> response);
  break;
}
