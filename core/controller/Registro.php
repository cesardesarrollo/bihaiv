<?php

class Registro extends Cuestionario{

	public static function cuestionarie_exist($nivel) {
		$CuestionarioData_ = new CuestionarioData();
		$Cuestionario_ = $CuestionarioData_->getCuestionario($nivel);
		if (is_object($Cuestionario_)){
			return $Cuestionario_->activo;
		} else {
			return false;
		}
	}

	public static function enviar_email($tipo,$clasificacion_sistema,$user_id,$cuestionario_dtl,$user_id_ref, $dias){
			
		$user = UserData::getById($user_id);
		$date = date('Y-m-d H:i:s');
		$url_cuestionarios = parent::url_cuestionarios();

		$CuestionariosUsuarioData = new CuestionariosUsuarioData();
		$CuestionarioData = new CuestionarioData();
		$EstatusCuestionarioData = new EstatusCuestionarioData();
		$DaoEvaluacion = new DaoEvaluacion();
		$DaoOrganizacion = new DaoOrganizacion();
		$DaoRoles = new DaoRoles();

		switch($tipo){

			case "cuestionario_perfilamiento":

				$mensaje = '
					<p>Hola  %s, Bienvenido:</p>
					<p style="text-align:justify;">
						Bihaiv nace de comprender la necesidad de incrementar la colaboración de los distintos actores del ecosistema de Emprendimiento de Alto Impacto a través de la vinculación y articulación de los mismos, con el objetivo de alcanzar mayores niveles de madurez del mismo para el beneficio de los emprendedores que dependen de un entorno que les permite mejorar las posibilidades de éxito de sus proyectos. 
					</p>
					<p style="text-align:justify;">
						Por esta razón, para ser un miembro activo debe cumplirse un proceso de registro que evalúa los roles y capacidades que pueden desarrollar cada una de las organizaciones interesadas en ser parte de la plataforma y así garantizar el cumplimiento del principal objetivo.
					</p>
					<p style="text-align:justify;">
						Si tu organización busca formar parte de Bihaiv, debes seguir los siguientes pasos:
						<ul style="list-style:none;">
							<li style="text-align:justify;">
								1- Responder la evaluación de perfil, con ella podrás conocer qué tipo de actor representa la organización a la que perteneces. Esto tendrás que hacerlo antes de 5 días luego de recibir este correo, de lo contrario el enlace caducará. (encontrarás el link al final de este correo)
							</li>
							<li style="text-align:justify;">
								2- Luego de recibir tu respuesta al primer cuestionario podrás postularte para ser miembro de Bihaiv, contestando un segundo formulario con información más específica sobre tu organización. Esta información será enviada a miembros ya pertenecientes (uno por cada tipo de actor) que fungirán como evaluadores pares y determinarán la entrada a la red dependiendo del nivel de incidencia de tu organización en el Ecosistema de Emprendimiento de Alto Impacto.
							</li>
							<li style="text-align:justify;">
								3- Al finalizar estos dos pasos, recibirás un correo de bienvenida a la plataforma, o en su caso una retroalimentación para tu organización.
							</li>
							<li style="text-align:justify;">
								<ul style="list-style:none;">
									<li style="text-align:justify;">
										3.1- El objetivo de la retroalimentación de Bihaiv, en caso de que tu solicitud sea rechazada, tiene el objetivo dar a conocer las áreas de oportunidad que tiene la organización solicitante a corto, mediano y largo plazo para ser considerado un actor del Ecosistema de Emprendimiento de Alto Impacto.
									</li>
									<li style="text-align:justify;">
										3.2- Si consideras que el resultado en la evaluación y la retroalimentación no corresponden a tu percepción, puedes solicitar una revisión, que automáticamente pasará a un nuevo proceso, donde el Consejo de Expertos de Bihaiv que desarrollará un dictamen inapelable.
									</li>
									<li style="text-align:justify;">
										3.3- Es posible hacer una nueva solicitud de ingreso luego de haber trabajado en la retroalimentación de la primera solicitud. Esto puede hacerse luego de seis meses de haberse postulado por primera vez.
									</li>
								</ul>
							</li>
						</ul>
					</p>
					<p style="text-align:justify;">
						Para todos los que conformamos Bihaiv, cada miembro representa una colaboración esencial, por lo que si consideras que la organización a la que representas está lista para unirse a esta gran red, te invitamos a dar el primer paso haciendo 
						<a href="%sperfilamiento.php?user_id=%d&fech_act=%s&nivel=1">click aquí para acceder a tu evaluación de perfil</a>. No dejes pasar mucho tiempo ¡El enlace caduca en 5 días!  
					</p>';

				$mensaje = sprintf($mensaje,$user->name,$url_cuestionarios,$user->id,$date);

				//Se crea un cuestionario para el usuario
				$nivel = 1;
				$Cuestionario = $CuestionarioData->getCuestionario($nivel);

				// Valida que no haya sido ya creado un CuestionarioUsuario
				$validacionCuestionariosUsuario = $CuestionariosUsuarioData->getByUserCuestionario($user_id,$Cuestionario->idCuestionario);
				if(count($validacionCuestionariosUsuario) == 0){

					$CuestionariosUsuarioData->set($Cuestionario->idCuestionario,$user_id,'Sin rol');
					$CuestionariosUsuarioCreado = $CuestionariosUsuarioData->add();

					// Guardar seguimiento/estatus
					$data = array("idCuestionariosUsuario"=>$CuestionariosUsuarioCreado[1],"estatus"=>0);
					$EstatusCuestionarioData->set($data);
					$EstatusCuestionarioCreado = $EstatusCuestionarioData->add();

					// Si fue creado un registro
					if ($EstatusCuestionarioCreado[0] == 1) {
						// Enviarlo
						Core::mail($user->email,'Bienvenido a Bihaiv',$mensaje, strip_tags($mensaje));
						Core::alert("¡Bienvenido a Bihaiv!. Un correo se ha enviado con un enlace para contestar un cuestionario de perfilamiento.");	
					}

				}
				
			break;


			case "resultado_perfilamiento":


				$mensaje = '<h1>¡Hola, %s!</h1>
				<p>
					Muchas gracias por querer sumarte parte a la construcción de una nueva 
					inteligencia social para el Ecosistema de Emprendimiento de Alto Impacto. 
					Tu primera evaluación ha reconocido a tu organización con el perfil de actor como %s.
				</p>
				<p>
					Para   continuar   con   el   proceso   de   postulación,   debes   acceder   al   segundo 
					cuestionario  que  será  valorado  por  miembros  pares  de  la red,  un  actor  con  el 
					mismo nivel de alcance que definas para tu perfil, por cada tipo de rol. 
				</p>
				<p>
					Ellos  brindarán  un  estatus final a la  organización  que  representas  dentro  de  la comunidad Bihaiv.
				</p>
				<p>
					<a href="%s">Ingresa a la segunda evaluación y postulación dando click aquí</a>.  
				</p>
				<p>
					El  resultado  obtenido  del  segundo  cuestionario  se  te  hará  saber  a  este  mismo 
					correo en un periodo de hasta 15 días. En él sabrás si tu solicitud de ingreso fue aprobada.
				</p>
				<p>
					¡Apresúrate! La liga caduca en 5 días.
				</p>';

				//Se crea un cuestionario para el usuario
				$nivel = 2;
				$Cuestionario = $CuestionarioData->getCuestionario($nivel);

				if(!is_object($Cuestionario)){
					Core::alert("Ha ocurrido un problema, no se encuentra un cuestionario de vinculación habilitado, regrese mas tarde o reviselo con el administrador de Bihaiv.");
					exit;
				}

				$obj = $CuestionariosUsuarioData->set($Cuestionario->idCuestionario,$user_id,$clasificacion_sistema);
				$CuestionariosUsuarioCreado = $CuestionariosUsuarioData->add();
				$enlace = $url_cuestionarios."perfilamiento.php?user_id=".$user_id."&fech_act=".$date."&nivel=2";
				//$mensaje = wordwrap($mensaje, 200, "<br/>");
				$mensaje = sprintf($mensaje, $user->name, $clasificacion_sistema,$enlace);

				// Si fue creado un registro
				if ($CuestionariosUsuarioCreado[0]=='1') {
					// Guardar seguimiento/estatus
					$data = array("idCuestionariosUsuario"=>$CuestionariosUsuarioCreado[1],"estatus"=>1);
					$EstatusCuestionarioData->set($data);
					$EstatusCuestionarioCreado = $EstatusCuestionarioData->add();
					if ($EstatusCuestionarioCreado[0]=='1') {
						// Enviarlo
						//Core::mail("felipe.murillo.urbina@hotmail.com",'Resultado de perfilamiento en Bihaiv',$mensaje, strip_tags($mensaje));
						Core::mail($user->email,'Resultado de perfilamiento en Bihaiv',$mensaje, strip_tags($mensaje));	
					}

				}

			break;


			case "resultado_evaluacion":

				// El mensaje
				$mensaje = '		
				<h1>
					Ahora estas siendo evaluado en Bihaiv.
				</h1>
				<p>
					El  resultado  obtenido  del  segundo  cuestionario  se  te  hará  saber  a  este  mismo 
					correo en un periodo de hasta 15 días. En él sabrás si tu solicitud de ingreso fue aprobada.
				</p>
				<p>
					Te deseamos suerte! .
				</p>';

				//$mensaje = wordwrap($mensaje, 200, "<br/>");
				$mensaje = sprintf($mensaje);

				// Enviarlo
				Core::mail($user->email,'Resultado de evaluación de miembros Bihaiv',$mensaje, strip_tags($mensaje));

				/*Procedimiento para envío de cuestionario a 6 actores*/
				$actores = parent::getActores($user->id);

				if(count($actores) == 0) {
					echo "No existen actores, nadie podrá validarte.";
					exit;
				}

				if(count($actores) >= 6) {
					$max = 6;
				} else {
					$max = count($actores);
				}

				$claves_aleatorias = array_rand($actores, $max);
				foreach ($claves_aleatorias as $key) {
					$actores_random[] = $actores[$key];
				}

				// Guardar seguimiento/estatus
				$Cuestionario = $CuestionarioData->getCuestionario(2);
				$CuestionariosUsuario = $CuestionariosUsuarioData->getByUserCuestionario($user->id,$Cuestionario->idCuestionario);
				$data = array("idCuestionariosUsuario"=>$CuestionariosUsuario->idCuestionariosUsuario,"estatus"=>2);
				$EstatusCuestionarioData->set($data);
				$EstatusCuestionarioCreado = $EstatusCuestionarioData->add();

				// Envia a actores
				foreach ($actores_random as $value) {
					// Registro de evaluación creada
			        $x = new Evaluacion();
			        $x->setFechaEvaluacion(date('Y-m-d H:i:s')); 
			        $x->setIdCuestionariosUsuario($CuestionariosUsuario->idCuestionariosUsuario);
			        $x->setUsuario_id($value->id);
			        $x->setConfidencialidad(0); 
			        $x->setRespuesta(0); 
					$evaluaciones = $DaoEvaluacion->add($x);
					
					if ($evaluaciones > 0) { 
						self::enviar_email("resultados_a_actor","",$value->id,$cuestionario_dtl,$user->id);
					}

				}

			break;

			
			case "reevaluacion":

				$mensaje = '
				<p>
				¡Hola de nuevo, %s! 
				</p>
				<p>
					La solicitud para reevaluar el caso de tu organización ha sido iniciada. 
				</p>
				<p>
					Este nuevo proceso consta de una segunda valoración que realizarán 3 miembros del <a href="%s">Consejo  de  Expertos  del  Bihaiv</a>  con  la  información  que  ya  nos  has  proporcionado  y teniendo en cuenta las obervaciones hecha por los actores pares del ecosistema. 
				</p>
				<p>
					Te haremos saber por este medio el resultado de tu solicitud. El  proceso  puede  demorarse  algunos  días,  por  lo  cual  te  pedimos  tengas paciencia.   
				</p>
				<p>
					Ten en cuenta que esta decisión es inapelable. 
				</p>
				<p>
					¡Éxito!
				</p>';

				$consejo_expertos_url = APP_PATH . "?view=consejo_expertos";
				$mensaje = sprintf($mensaje,$user->name,$consejo_expertos_url);

				//$mensaje = wordwrap($mensaje, 200, "<br/>");
				$mensaje = sprintf($mensaje);
				
				// Enviarlo
				Core::mail($user->email,'Tu solicitud de reevaluación esta en proceso',$mensaje, strip_tags($mensaje));

			break;


			case "resultados_a_actor":

				$mensaje = '
				<h1>Una organización a solicitado se le evalúe para pertenecer a la comunidad Bihaiv.</h1>
				<p>Se te ha encomendado evaluar los resultados de su examen de evaluación para su admisión en la comunidad.</p>
				<p>Ve al siguiente enlace para ver los resultados de los cuestionarios de perfilamiento y la información del actor solicitando para aprobar ó rechazar su solicitud para ingresar a la comunidad Bihaiv: </p>
				<p><a href="%s">Evaluar actor</a></p>';

				$enlace = $url_cuestionarios."evaluacion.php?user_id=".$user_id_ref."&evaluador=".$user->id;
				//$mensaje = wordwrap($mensaje, 200, "<br/>");
				$mensaje = sprintf($mensaje,$enlace);
				
				// Enviarlo
				//Core::mail("felipe.murillo.urbina@hotmail.com",'Se te ha encomendado evaluar a posible actor',$mensaje, strip_tags($mensaje));
				 Core::mail($user->email,'Se te ha encomendado evaluar a posible actor',$mensaje, strip_tags($mensaje));

			break;

			case "recordatorio":

				$mensaje = '
				<h1>Recuerda, que una organización a solicitado se le evalúe para pertenecer a la comunidad Bihaiv.</h1>
				<p>Se te ha encomendado evaluar los resultados de su examen de evaluación para su admisión en la comunidad. solo quedan '.$dias.' restantes para hacerlo.</p>
				<p>Ve al siguiente enlace para ver los resultados de los cuestionarios de perfilamiento y la información del actor solicitando para aprobar ó rechazar su solicitud para ingresar a la comunidad Bihaiv: </p>
				<p><a href="%s">Evaluar actor</a></p>';

				$enlace = $url_cuestionarios."evaluacion.php?user_id=".$user_id_ref."&evaluador=".$user_id;
				//$mensaje = wordwrap($mensaje, 200, "<br/>");
				$mensaje = sprintf($mensaje,$enlace);
				
				// Enviarlo
					// Core::mail('felipe.murillo.urbina@hotmail.com','Recuerda, se te ha encomendado evaluar a posible actor',$mensaje, strip_tags($mensaje));
				Core::mail($user->email,'Recuerda, se te ha encomendado evaluar a posible actor',$mensaje, strip_tags($mensaje));

			break;


			case "resultados_a_experto":

				$mensaje = '
				<h1>Una organización a solicitado se le evalúe para pertenecer a la comunidad Bihaiv.</h1>
				<p>Se te ha encomendado evaluar los resultados de su examen de evaluación para su admisión en la comunidad.</p>
				<p>Sigue el siguiente enlace para  ver los resultados de los cuestionarios de perfilamiento y la información del actor solicitando para aprovar ó recharzar su solicitud para ingresar a la comunidad Bihaiv:</p>
				<p><a href="%s">Evaluar actor</a></p>';

				$enlace = $url_cuestionarios."evaluacion.php?user_id=".$user_id_ref."&evaluador=".$user->id;
				//$mensaje = wordwrap($mensaje, 200, "<br/>");
				$mensaje = sprintf($mensaje,$enlace);
				
				// Enviarlo
				Core::mail($user->email,'Se te ha encomendado evaluar a posible actor',$mensaje, strip_tags($mensaje));

			break;


			case "finalizacion_aprovada_evaluacion":
		
				$Cuestionario = $CuestionarioData->getCuestionario(2);
				$CuestionariosUsuario = $CuestionariosUsuarioData->getByUserCuestionario($user->id,$Cuestionario->idCuestionario);

				// Obtiene retroalimentaciones
				$retroalimentaciones = $DaoEvaluacion->getByIdCuestionarioUsuario($CuestionariosUsuario->idCuestionariosUsuario);

				$retro_str = "";
				foreach ($retroalimentaciones as $retroalimentacion) {
					$retro_str .= "<p>";
					$evaluador = UserData::getById($retroalimentacion->Usuario_id);
					if ($retroalimentacion->Confidencialidad == 0) {
						$evaluador_name = $evaluador->name;
					} else {
						$evaluador_name = "";
					}

					if ($evaluador_name !== "") { 
						$retro_str .= "<strong>Evaluador:</strong> " . $evaluador_name . "<br>";
					}
					$retro_str .= "<strong>Retroalimentación:</strong> " . $retroalimentacion->Comentarios;
					$retro_str .= "</p>";
				}

				$mensaje = '
				<p>Estimado %s:</p>
				<p>Es para todo el equipo y los miembros de Bihaiv un gusto darte la más cordial bienvenida a la primera plataforma a nivel mundial de Inteligencia Social que fomenta la colaboración de los actores del Ecosistema de Emprendimiento de Alto Impacto.</p>
				<p>Nos alegra que seas parte de esta gran red, sabemos que pronto comenzarás a desarrollar importantes iniciativas en colaboración con otras organizaciones que comparten tu misma visión.</p>
				<p><a href="%s">Comienza a vivir la experiencia Bihaiv!  Date una vuelta y empieza a editar tu perfil.</a></p>';

				if ($retro_str !== "") {
					$mensaje .= '<h1>Retroalimentación por evaluadores</h1>';
					$mensaje .= $retro_str;
				}

				//$mensaje = wordwrap($mensaje, 200, "<br/>");
				$mensaje = sprintf($mensaje,$user->name,APP_PATH);
				
				// Enviarlo a actor
				Core::mail($user->email,'Tu solicitud ha sido aprobada por la comunidad',$mensaje, strip_tags($mensaje));


				//Habilitar usuario
				$return = $user->validateId($user->id);

				if ($return[0] == 1) {
					// Envio de correo a super usuario
					$admin = $user->getAdmin();

					$Organizacion = $DaoOrganizacion->getByUserId($user->id);
					$Roles = $DaoRoles->getById($Organizacion->Roles_idRol);

					$mensaje_superuser = '
					<p>Un nuevo actor con el nombre de <strong>%s</strong> ha sido integrado a la comunidad Bihaiv. con el rol de <strong>%s</strong>.</p>';

					//$mensaje_superuser = wordwrap($mensaje_superuser, 200, "<br/>");
					$mensaje_superuser = sprintf($mensaje_superuser,$user->name,$Roles->nombre);
					
					//Enviarlo a administrador
					Core::mail($admin->email,'Una solicitud ha sido aprobada por la comunidad',$mensaje_superuser, strip_tags($mensaje_superuser));

				}

			break;


			case "finalizacion_rechazada_evaluacion":
				
				$Cuestionario = $CuestionarioData->getCuestionario(2);
				$CuestionariosUsuario = $CuestionariosUsuarioData->getByUserCuestionario($user->id,$Cuestionario->idCuestionario);

				// Obtiene retroalimentaciones
				$retroalimentaciones = $DaoEvaluacion->getByIdCuestionarioUsuario($CuestionariosUsuario->idCuestionariosUsuario);

				$retro_str = "";
				foreach ($retroalimentaciones as $retroalimentacion) {
					$retro_str .= "<p>";
					$evaluador = UserData::getById($retroalimentacion->Usuario_id);
					if ($retroalimentacion->Confidencialidad == 0) {
						$evaluador_name = $evaluador->name;
					} else {
						$evaluador_name = "";
					}

					if ($evaluador_name !== "") { 
						$retro_str .= "<strong>Evaluador:</strong> " . $evaluador_name . "<br>";
					}
					$retro_str .= "<strong>Retroalimentación:</strong> " . $retroalimentacion->Comentarios;
					$retro_str .= "</p>";
				}

				$mensaje = '
				<p>Estimado %s: </p>
				<p>
				Te  informamos  que  tu  solicitud  ha  sido  rechazada  en  la  etapa  de  validación  de actores pares del ecosistema. 
				</p><p>
				Para  nosotros  es  muy  importante  que  sepas  cuales  son  los  motivos  de  esta decisión  y  cuáles  son  las  áreas  de  oportunidad  que  los  actores  reconodiso miembros de Bihaiv han establecido.
				</p><p>
				Por  lo  que  te  hacemos  llegar  una  retroalimentación  con  la  finalidad  de  que  tu organización  pueda establecer  un  plan  de  acción  para  una  segunda  aplicación  si así lo desea. 
				</p><p>
				Esperamos saber de ti pronto. Recuerda que en seis meses tu perfil puede crear una  nueva  solicitud  de ingreso,  así  que tienes  todo el  tiempo  que  necesites  para seguir trabajando. 
				</p><p>
				¡Queremos que seas parte de nuestra comunidad! 
				</p><p style="text-align:center;"><b>
				Retroalimentación de Pares  
				</b></p><p>
				Si  después  de  leer  nuestra  retroalimentación,  consideras  que  evaluación no  se ajusta  a  tu  percepción  sobre  la  organización,  puedes  solicitar una  revisión  que será efectuada por el 
				<a href="%s" >Consejo de Expertos de Bihaiv</a>,  quienes  valorarán  de  nuevo  tus  datos e información,  así  como  la  retroalimentación  de  los  actores  pares. 
				</p><p>
				<a href="%snuevaoportunidad.php?user_id=%s">Para  hacer  la solicitud a una segunda vista, haz click aquí</a>.</p>';

				if ($retro_str !== "") {
					$mensaje .= '<h1>Retroalimentación por evaluadores</h1>';
					$mensaje .= $retro_str;
				}
				
				//$mensaje = wordwrap($mensaje, 200, "<br/>");
				$consejo_expertos_url = APP_PATH . "?view=consejo_expertos";
				$mensaje = sprintf($mensaje,$user->name,$consejo_expertos_url,$url_cuestionarios,$user->id);

				// Enviarlo
				Core::mail($user->email,'Tu solicitud ha sido rechazada por la comunidad',$mensaje, strip_tags($mensaje));

			break;

		}
		return true;

	}

}

?>