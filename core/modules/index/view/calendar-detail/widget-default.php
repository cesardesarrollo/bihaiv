<?php
  @$id = $_SESSION["user_id"];

  require_once('./core/modules/index/model/DaoOrganizacion.php');
  require_once('./core/modules/index/model/DaoColaboracion.php');
  require_once('./core/modules/index/model/DaoEvento.php');

  $DaoEvento = new DaoEvento();
  $DaoOrganizacion = new DaoOrganizacion();
  $DaoColaboracion = new DaoColaboracion();
  $userdata = new UserData();
  /* Valida que el id sea correcto */
  if( isset($_GET['id']) AND is_numeric($_GET['id']) ){
    $evento_id = $_GET['id'];
    $evento_data = $DaoEvento -> getById($evento_id);

    // Si no existe el evento
    if(!is_object($evento_data)){
      Core::redir("./");
      exit;
    }

    /*  VALIDA SI EL EVENTO ES PRIVADO */
    if( $evento_data -> acceso == 'PRIVADO'){
      /* valida si existe la sesión */
      if(!empty($_SESSION) OR !isset($_SESSION['user_id']) ){
        /* Valida si le pertenece el evento */
        if( $evento_data -> actorId  == $_SESSION['user_id']){
          /* En este caso no pasa nada porque el evento le pertenece */
        }else{
          /*
          * Es un evento privado y no es propietario, debemos validar que este
          * en la lista de invitados
          */
          $colaboraciones = $DaoColaboracion->getByIdEvento( $evento_id );
          $array_colaboracion = array();
          foreach ($colaboraciones as $colaboracion) {
            $org = $DaoOrganizacion -> getById( $colaboracion['user_id'] );
            $user = $userdata -> getById( $org->Usuarios_idUsuarios );
            array_push($array_colaboracion, $user->id);
          }
          if (isset($_SESSION['user_id']) && in_array($_SESSION['user_id'], $array_colaboracion)) {
            /*
            * El usuario esta invitado al evento, en este caso puede ver la
            * información
            */
          }else{
            /* El usuario logueado no esta invitado a ese evento */
            Core::redir("./?view=calendar");
          }

        }
      }else{
        Core::redir("./");
      }
    }

  }else{
    Core::redir("./");
  }
?>
<style>
  #owl-demo .item img{
    display: block;
    width: 100%;
    height: 300px;
  }
  /* HERE COMES NEW CHALLERNGER!!!*/
  .calendar-thumbnail-list{
    background-color: whitesmoke;
    border-radius: 2px;
    box-shadow: 0 1px 3px 0 rgba(0,0,0,.2),0 1px 1px 0 rgba(0,0,0,.14),0 2px 1px -1px rgba(0,0,0,.12);
    background-repeat:no-repeat;
    background-position: center center;
    background-size: cover;
  }
  #owl-demo .item .cover-img{
    display: block;
    width: 100%;
    height: 300px;
    background-color: whitesmoke;
    background-repeat:no-repeat;
    background-position: center center;
    background-size: cover;
  }
  .img-event {
    height: 300px;
  }
  /* END HERE COMES NEW CHALLERNGER!!!*/
</style>

<div class="container-fluid" style="margin-left:0; margin-right:0;padding-left:0; padding-right:0; ">
    <?php include('./views/main_menu.php'); ?>
</div>

<div class="container">
    <section class="Profile__wall">
      <!-- slider -->
      <div class="row">
        <div class="col-md-12">
          <div id="owl-demo" class="owl-carousel owl-theme"></div>
        </div>
      </div>
      <div class="panel panel-card">
        <div class="panel-body">
          <!-- event detail -->
          <div id="detail-event"></div>
          <!-- Colaboradores -->
          <div class="row">
            <div class="col-md-12">
              <h2><?=translate('Colaboradores')?>:</h2>
              <div id="invitados_current_list"></div>
              <br>
              <h2><?=translate('Objetivos')?>:</h2>
              <div class="col-md-6"> <ul id="objetivos_list"></ul></div>
            </div>
          </div>
        </div>
      </div>
      <hr>
      <!-- navigation buttons -->
      <div class="row">
        <div class="col-md-12">
          <a href="#" id="btnRegresarAtras" class="btn btn-primary pull-left"><i class="fa fa-chevron-left" aria-hidden="true"></i> &nbsp; <?=translate('Regresar')?></a>
          <a href="?view=calendar" class="btn btn-primary pull-right"><i class="fa fa-list" aria-hidden="true"></i> &nbsp; <?=translate('Ver todos los eventos')?></a>
        </div>
      </div>
      <br><br>
    </section>
    <div id="modal-launcher"></div>
</div>
<!-- CURRENT EVENT TEMPLATE -->
<script id="detail-event-tmpl" type="x-text/handlebars">
<div class="row">
    <div class="col-md-12">
        <figure class="figure">
            <div class="img-event calendar-thumbnail-list" style="background-image: url(uploads/eventos/{{llaveImg}});"></div>
        </figure>
    </div>
</div>
<br>
<div class="row">
  <div class="col-xs-6 col-md-4 col-lg-5">
      <div class="form-group">
          <h2 class="title--initiative bolder">{{titulo}}</h2>
          <p><?=translate('Evento creado por')?>: <a href="<?php echo APP_PATH. '?view=home&id=';?>{{actorId}}"> {{actorName}}  </a></p>
          <p><?=translate('Fecha inicio')?>: {{fechaEvento}} <?=translate('Fecha finaliza')?>: {{fechaFin}}</p>
          <p><?=translate('Hora inicio')?>: {{horaInicio}} <?=translate('Hora finaliza')?>: {{horaFin}}</p>
          <p class="place--initiative bolder">{{lugar}}</p>
      </div>
      <div class="form-group">
          <p>{{textoLargo}}</p>
          <p><?=translate('Tipo de Evento')?>: {{tipoEvento}}</p>
          <p><?=translate('Url')?>: <a href="{{url}}" target="_blank">{{url}}</a></p>
          <p><?=translate('Acceso')?>: {{acceso}}</p>
      </div>
  </div>
  <div class="col-xs-6 col-md-8 col-lg-7">
      <div class="form-group">
          <div id="map_canvas" style="width: 100%; height: 280px; margin: 0 auto;"></div>
      </div>
  </div>
</div>
</script>
<script>
  $(function(){

      var event = new Evento();
      var id = null;
      <?php if (isset($_GET['id'])){ ?>
      id = <?php echo $_GET['id'] ?>;
      <?php } ?>
      event._getById(id);

      // Objetivos

      $.post('api/front/objetivo.php', {'method': 'allByEventoId', 'evento_id': id})
        .done(function( _res ) {
          var options = '';
          result = jQuery.parseJSON(_res);
          $objetivos_list=$('#objetivos_list');
          $objetivos_list.html('');
          for(var i=0; i< result.length; i++)
          {
            options += '<li class="list-group-item" style="width:auto;"> <i class="fa fa-dot-circle-o"/>' + result[i]['descripcion'] + '<div class="pull-right">'+result[i]['stars']+ '</div></li>';
          }
          if (options === ""){
            options = "<li><?=translate('No hay objetivos.')?></li>";
          }
          $objetivos_list.append(options);
          $rateYoObjetivosEventos = $(".rateYo-obejtivos-eventos").rateYo({
              rating    : 0,
              starWidth : "16px",
              ratedFill : '#fae617',
              precision : 0,
              spacing   : "2px",
              onSet: function (rating, rateYoInstance) {
                if( parseInt(rating) > 0 ){
                  var data = {
                    'objetivo_id'       : $(this).data('idobjetivoevento'),
                    'calificacion'    : rating,
                    'organizacion_id'   : null,
                    'method'          : 'update_rating',
                     'evento_id' : id
                  };
                  // Ajax request to save qualification
                  $.post('api/front/objetivo.php', data, function(response){
                    setFlash(response.msg, response.class);
                  });
                }else{
                  setFlash('Para calificar es necesario ingresar al menos una estrella', 'warning');
                }
              }
            });
        })
        .error(function( _err ){
          console.log( _err );
        });

      // Colaboradores
      $invitados_current_list = $('#invitados_current_list');
      $.post('api/front/colaboracion.php', {'method': 'byIdEvento', 'evento_id': id })
        .done(function( _res ) {
          console.log(_res);
          var _res = JSON.parse(_res);
          var options = '';

          $invitados_current_list.html('');
          var key_array = [];
          $.each(_res,function(index, value){
              options += '<a class="pull-left padding-col-lg" style="margin-right:5px;" href="?view=home&id=' + value.user_id + '" title="' + value.user_name + '">';
              options += '  <figure class="figure">';
              options += '    <img src="<?php APP_PATH ?>storage/users/' + value.user_id + '/profile/' + value.avatar + '" alt="' + value.user_id + '" height="40">';
              options += '  </figure>';
              options += '</a> ';
          });
          if (options === ""){
            options = "<ul><li><?=translate('No hay colaboradores.')?></li></ul>";
          }
          $invitados_current_list.append(options);
      });

      // Datepickers
      $("#txt__CurrentFilterDate").datepicker({ dateFormat: 'yy-mm-dd' });
      $("#txt__CoomingEventsDate").datepicker({ dateFormat: 'yy-mm-dd' });
      $("#txt__LatestEventDate").datepicker({ dateFormat: 'yy-mm-dd' });
      // Dibuja Carrusel
      event._getAllEvents();

      $('#btnRegresarAtras').on('click',function(e){
        e.preventDefault();
        history.go(-1)
      });
  });
</script>
