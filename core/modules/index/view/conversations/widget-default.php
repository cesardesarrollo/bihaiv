<?php if(!isset($_SESSION["user_id"])){ Core::redir("./");}?>
<?php
$msgs = ConversationData::getConversations($_SESSION["user_id"]);
?>
<div class="container padding-top-100">
<div class="row">
    <div class="col-md-3">
<?php Action::execute("_userbadge",array("user"=>Session::$user,"organizacion"=>Session::$organizacion,"from"=>"logged" ));?>
<?php Action::execute("_mainmenu",array());?>
    </div>
    <div class="col-md-7">
    <h2><?=translate('Conversaciones')?></h2>
    <?php if(count($msgs)>0):?>
      <table class="table table-bordered">
      <thead>
        <th><?=translate('Amigo')?></th>
      </thead>
      <?php foreach($msgs as $msg):?>
        <tr>
          <td>
          <?php
          $nmsg = MessageData::countUnReadsByUC($_SESSION["user_id"],$msg->id);
          $color = "label-default";
          if($nmsg->c>0){ $color="label-danger"; }
          echo "<span class='label $color pull-right'>".$nmsg->c."</span>";
          echo "<a href='./?view=conversation&id=$msg->id'>";

          if($msg->receptor_id==Session::$user->id){
            echo $msg->getSender()->getFullname();
          }else{
            echo $msg->getReceptor()->getFullname();          
          }
          echo "</a>";
          ?>
          </td>
        </tr>
      <?php 
      $msg->read();
      endforeach; ?>
      </table>
    <?php else:?>
      <div class="jumbotron">
      <h2><?=translate('No hay Mensajes')?></h2>
      </div>
    <?php endif;?>
    </div>
    <div class="col-md-2">
    </div>
  </div>
</div>


</div>
<br><br><br><br><br>