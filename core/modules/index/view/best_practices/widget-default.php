<?php

require_once('./core/modules/index/model/DaoSeccionAportacion.php');
$DaoSeccionAportacion = new DaoSeccionAportacion();

?>

<div class="container-fluid" style="margin-left:0; margin-right:0;padding-left:0; padding-right:0; ">
    <?php include('./views/main_menu.php'); ?>
    <!-- here comes new challenger -->
    <div class="gray-header col-md-12">
      <div class="gray-text">
        <div class="container">
          <div class="img-helper col-xs-6 col-xs-offset-3 col-sm-3 col-sm-push-9 col-sm-offset-0 col-md-3 col-md-push-9 col-md-offset-0">
            <!-- img -->
            <span class="helper"></span>
            <img src="assets/img/home/header-aportaciones.png" class="img-header img-resposive" />
          </div>
          <div class="col-xs-12 col-sm-9 col-sm-pull-3 col-md-9 col-md-pull-3">
            <h1 class="title-gray"><?=translate('Aportaciones del Ecosistema')?></h1>
            <p class="description-gray">
              <?=translate('Descripción corta sección Aportaciones')?>
            </p>
          </div>
        </div>
      </div>
    </div>
    <!-- End here comes new challenger -->
</div>

<div class="container">
  <div class="col-md-12 aportaciones-container">

    <?php

    if ( isset($_COOKIE['bihaiv_lang']) && $_COOKIE['bihaiv_lang'] == "en") {
      $lang_esp = false;
    } else {
      $lang_esp = true;
    }
    foreach ($DaoSeccionAportacion->getAll() as $seccion) {
      if($seccion->getURL() != '' && $seccion->getActivo() == 1) echo '<a href="'.$seccion->getURL().'">';

    ?>
      <div class="col-md-3">
        <div class="aportaciones-card card-<?php echo $seccion->getId(); ?>">
          <h3 class="aportaciones-title" ><?php echo ($lang_esp) ? $seccion->getTitulo() : $seccion->getTitle() ?></h3>
          <center>
            <img src="assets/img/home/aportacion-<?php echo $seccion->getId(); ?>.png" class="img-aportacion img-resposive" />
          </center>
          <p class="description">
            <?php echo ($lang_esp) ? $seccion->getDescripcion() : $seccion->getDescription() ?>
          </p>
        </div>
      </div>

    <?php
      if($seccion->getURL() != '' && $seccion->getActivo() == 1) echo '</a>';
    }
    ?>
  </div>
  <div id="modal-launcher"></div>
</div>
