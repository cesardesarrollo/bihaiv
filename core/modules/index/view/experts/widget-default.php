<?php
  require_once './core/modules/index/model/DaoExperto.php';
  require_once './core/modules/index/model/DaoEstados.php';
  require_once './core/modules/index/model/DaoRoles.php';
  $DaoExperto = new DaoExperto();
  $DaoRoles = new DaoRoles();
  $DaoEstados = new DaoEstados();
  $UserData = new UserData();
?>
<div class="row" style="margin-left:0; margin-right:0;padding-left:0; padding-right:0; ">
    <?php include('./views/main_menu.php'); ?>
    <!-- here comes new challenger -->
    <div class="gray-header col-md-12">
      <div class="gray-text">
        <div class="container">
          <div class="img-helper col-xs-6 col-xs-offset-3 col-sm-3 col-sm-push-9 col-sm-offset-0 col-md-3 col-md-push-9 col-md-offset-0">
            <!-- img -->
            <span class="helper"></span>
            <img src="assets/img/home/header-expertos.png" class="img-header img-resposive" />
          </div>
          <div class="col-xs-12 col-sm-9 col-sm-pull-3 col-md-9 col-md-pull-3">
            <h1 class="title-gray"><?=translate('¿Que es el consejo de expertos?')?></h1>
            <p class="description-gray">
              <?=translate('Descripción corta sección experts')?>
            </p>
          </div>
        </div>
      </div>
    </div>
    <!-- End here comes new challenger -->
</div>
<div class="container">
    <div class="col-sm-12 col-lg-12 margin-row-top">
      <div class="panel panel-card">
          <div class="panel-body">
            <div class="row">
              <div class="col-md-12">
                <h3 class="title" ></h3>
                <p>
                  <?=translate('Descripción sección experts')?>
                </p>
                <a class="btn btn-primary" href="<?=APP_PATH?>?view=consejo_expertos" style="margin-top:15px;">
                  <?=translate('Mostrar listado de expertos')?>
                </a>
              </div>
            </div>
          </div>
        </div>
    </div>
    <div class="cards-container col-md-12 col-lg-12">
       <h1 class="more-info-title"><?=translate('¿Deseas postularte como experto?')?></h1>
       <div class="panel panel-card color-gray">
         <div class="panel-body">
           <div class="row">
             <div class="col-md-2">
               <center>
                 <img src="assets/img/home/temp-sprite.png" style="max-width: 100%;" class="img-resposive" />
               </center>
             </div>
             <div class="col-md-10">
               <h3 class="title" ><?=translate('Envíanos tu solicitud')?></h3>
               <form id="expert_request_form" action="?action=sendexpertrequest" method="post">
                 <div class="form-group">
                   <label for="request_name"><span class="requerido">*</span><?=translate('Nombre completo')?>:</label>
                   <input type="text" class="form-control" id="request_name" name="request_name" maxlength="100" required>
                 </div>
                 <div class="form-group">
                   <label for="request_email"><span class="requerido">*</span><?=translate('Correo')?>:</label>
                   <input type="text" class="form-control" id="request_email" name="request_email" maxlength="100" required>
                 </div>
                 <div class="form-group">
                   <label for="request_message"><span class="requerido">*</span><?=translate('Mensaje')?>:</label>
                   <textarea class="form-control" id="request_message" name="request_message" maxlength="100" required></textarea>
                 </div>
                 <div class="form-group">
                   <button type="submit" class="btn btn-primary"><?=translate('Enviar')?></button>
                 </div>
               </form>
             </div>
           </div>
         </div>
       </div>
    </div>
    <div id="modal-launcher"></div>
</div>
