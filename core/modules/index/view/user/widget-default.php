<?php
$user = UserData::getById($_GET["id"]);
if($user!=null):
//if($user==Session::$user){ Core::redir("./?view=home");}

require_once 'core/modules/index/model/DaoOrganizacion.php';
$DaoOrganizacion = new DaoOrganizacion();

// $levels =LevelData::getAll();
$organizacion = $DaoOrganizacion->getByUserId($_SESSION["user_id"]);



?>
<div class="container">
<div class="row">
    <div class="col-md-3">
<?php 
$from = "logout";
if(isset($_SESSION["user_id"])){ $from="logged"; }
Action::execute("_userbadge",array("user"=>$user,"organizacion"=>$organizacion ,"from"=>$from));
Action::execute("_usermenu",array("user"=>$user,"from"=>$from));
?>
    </div>
    <div class="col-md-7">

    <?php echo Action::execute("_statuses",array("user"=>$user,"organizacion"=>$organizacion,"from"=>$from));?>

    </div>
    <div class="col-md-2">
    </div>
  </div>
</div>



<div class="container">
 
    <?php include('./views/main_menu.php'); ?>

    <section class="Profile__cover">
      <figure class="figure">
        <img src="assets/img/home/cover_profile.jpg" alt="" draggable="false">
      </figure>
    </section>
    
    <?php Action::execute("_userprofileinfo",array("user"=>$user,"profile"=>$profile,"from"=>$from ));?>

    <section class="Profile__wall">
      <div class="container">
        <div class="row padding-row">
          <div class="col-xs-12 col-sm-12 col-md-9 col-lg-8">

            <div class="Wall margin-row-bottom z-depth-1">
              <div class="Wall__header">
                <figure class="figure display-inline">
                  <img src="assets/img/home/enfoque.png" alt="">
                </figure>
                <h3 class="display-inline text-white bolder">Enfoque</h3>
                <div class="pull-right" style="margin-top:-3px">
                  <button class="btn btn-outline-primary">
                    <i class="fa fa-plus"></i> Agregar Nuevo
                  </button>
                </div>
              </div>
              <div class="Wall__content padding">
                <ul class="list-group">
                    <li class="list-group-item">
                      <i class="fa fa-caret-right"></i> Enfoque 1
                      <div class="pull-right">
                        <i class="fa fa-star star-rate"></i>
                        <i class="fa fa-star star-rate"></i>
                        <i class="fa fa-star-o star-rate"></i>
                        <i class="fa fa-star-o star-rate"></i>
                        <i class="fa fa-star-o star-rate"></i>
                      </div>
                    </li>
                    <li class="list-group-item">
                      <i class="fa fa-caret-right"></i> Enfoque 2
                      <div class="pull-right">
                        <i class="fa fa-star star-rate"></i>
                        <i class="fa fa-star-o star-rate"></i>
                        <i class="fa fa-star-o star-rate"></i>
                        <i class="fa fa-star-o star-rate"></i>
                        <i class="fa fa-star-o star-rate"></i>
                      </div>
                    </li>
                    <li class="list-group-item">
                      <i class="fa fa-caret-right"></i> Enfoque 3
                      <div class="pull-right">
                        <i class="fa fa-star star-rate"></i>
                        <i class="fa fa-star star-rate"></i>
                        <i class="fa fa-star-half-o star-rate"></i>
                        <i class="fa fa-star-o star-rate"></i>
                        <i class="fa fa-star-o star-rate"></i>
                      </div>
                    </li>
                    <li class="list-group-item">
                      <i class="fa fa-caret-right"></i> Enfoque 4
                      <div class="pull-right">
                        <i class="fa fa-star star-rate"></i>
                        <i class="fa fa-star star-rate"></i>
                        <i class="fa fa-star star-rate"></i>
                        <i class="fa fa-star star-rate"></i>
                        <i class="fa fa-star star-rate"></i>
                      </div>
                    </li>
                    <li class="list-group-item">
                      <i class="fa fa-caret-right"></i> Enfoque 5
                      <div class="pull-right">
                        <i class="fa fa-star star-rate"></i>
                        <i class="fa fa-star star-rate"></i>
                        <i class="fa fa-star star-rate"></i>
                        <i class="fa fa-star star-rate"></i>
                        <i class="fa fa-star star-rate"></i>
                      </div>
                    </li>
                </ul>
              </div>
              <div class="Wall__footer"></div>
            </div>

            <?php Action::execute("_usermodiniciativas",array("user"=>$user,"profile"=>$profile,"from"=>$from ));?>
            <?php Action::execute("_usermodcolaboraciones",array("user"=>$user,"profile"=>$profile,"from"=>$from ));?>
            <?php Action::execute("_usermodaportaciones",array("user"=>$user,"profile"=>$profile,"from"=>$from ));?>
       
            <!--
            <div class="Wall margin-row-bottom z-depth-1">
              <div class="Wall__header">
                <h3 class="display-inline text-white bolder">Noticias</h3>
              </div>
              <div class="Wall__content padding">
                <?php //Action::execute("_newstatus",array());?>
                <?php //echo Action::execute("_statuses",array("user"=>$user,"profile"=>$profile,"from"=>$from));?>
              </div>
            </div>
            -->

            <div class="Wall margin-row-bottom z-depth-1">
              <div class="Wall__header">
                <h3 class="display-inline text-white bolder">Mis redes</h3>
              </div>
              <div class="Wall__content padding">

              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-3 col-lg-4">
             <?php Action::execute("_usermodeventos",array("user"=>$user,"profile"=>$profile,"from"=>$from ));?>
          </div>
        </div>
      </div>
    </section>
    <div id="modal-launcher"></div>
    
    <!--TEMPLATES-->
    <script id="lead-template" type="text/x-handlebars-template">
      {{#each this}}
        <div class="row hoverable margin-row-bottom row-separator padding-row-sm">
          <div class="col-xs-6 col-md-4 col-lg-3 margin-row-bottom">
            <figure class="figure">
              <img class="img-initiative" src="{{avatar}}" alt="">
            </figure>
          </div>
          <div class="col-xs-6 col-md-8 col-lg-9 margin-row-bottom">
            <h2 class="title--initiative bolder">{{title}}</h2>
            <h4 class="date--initiative bolder">{{date}}</h4>
            <h4 class="place--initiative bolder">{{place}}</h4>
            <span class="margin-row-top">{{description}}<a href="#">Ver Más</a>
            </span>
          </div>
          <div class="col-xs-12 col-lg-12">
            <div class="col-xs-6 col-md-4 col-lg-3 margin-row-bottom">
              <label for="">Actores Asociados:</label>
            </div>
            <div class="col-xs-6 col-md-8 col-lg-9 actors margin-row-bottom">
              <a href="#">
                <figure class="figure">
                  <img src="assets/img/home/actor.jpg" alt="" height="40">
                </figure>
              </a>
              <a href="#">
                <figure class="figure">
                  <img src="assets/img/home/actor.jpg" alt="" height="40">
                </figure>
              </a>
              <a href="#">
                <figure class="figure">
                  <img src="assets/img/home/actor.jpg" alt="" height="40">
                </figure>
              </a>
              <a href="#">
                <figure class="figure">
                  <img src="assets/img/home/actor.jpg" alt="" height="40">
                </figure>
              </a>
              <a href="#">
                <figure class="figure">
                  <img src="assets/img/home/actor.jpg" alt="" height="40">
                </figure>
              </a>
              <a href="#">
                ... Ver todos
              </a>
            </div>
          </div>
          <div class="col-xs-12 col-lg-12 social">
            <div class="col-xs-4 col-md-4 col-lg-4">
              <figure class="figure">
                <img src="assets/img/home/comentar.png" alt="" height="20">
              </figure>
            </div>
            <div class="col-xs-4 col-md-4 col-lg-4">
              <figure class="figure">
                <img src="assets/img/home/like.png" alt="" height="20">
              </figure>
            </div>
            <div class="col-xs-4 col-md-4 col-lg-4">
              <figure class="figure">
                <img src="assets/img/home/compartir.png" alt="" height="20">
              </figure>
            </div>
          </div>
        </div>
      {{/each}}
    </script>
    <!--COLLABORATION TEMPLATE-->
    <script id="collaboration-template" type="text/x-handlebars-template">
      {{#each this}}
        <li class="list-group-item hoverable">
            <a href="#">{{company}}</a> Colaboro en el <a href="#">{{project}}</a> de <a href="#">{{project_company}}</a>
          </li>
      {{/each}}
    </script>
    <!--APORTES-->
    <script id="contributions-template" type="text/x-handlebars-template">
      {{#each this}}
        <div class="row margin-row-bottom row-separator padding-row-sm">
          <div class="col-xs-6 col-md-4 col-lg-3 margin-row-bottom">
            <figure class="figure">
              <img class="img-initiative" src="{{avatar}}" alt="{{title}}">
            </figure>
          </div>
          <div class="col-xs-6 col-md-8 col-lg-9 margin-row-bottom">
            <h2 class="title--initiative bolder">{{title}}</h2>
            <h4 class="date--initiative bolder">{{date}}</h4>
            <span class="margin-row-top">{{description}} <a href="#">Ver Más</a>
            </span>
          </div>
          <div class="col-xs-12 col-lg-12 social">
            <div class="col-xs-4 col-md-4 col-lg-4">
              <figure class="figure">
                <img src="assets/img/home/comentar.png" alt="" height="20">
              </figure>
            </div>
            <div class="col-xs-4 col-md-4 col-lg-4">
              <figure class="figure">
                <img src="assets/img/home/like.png" alt="" height="20">
              </figure>
            </div>
            <div class="col-xs-4 col-md-4 col-lg-4">
              <figure class="figure">
                <img src="assets/img/home/compartir.png" alt="" height="20">
              </figure>
            </div>
          </div>
        </div>
      {{/each}}
    </script>

</div>








<script>
function like(type,id){
  var base = "lk";
  if(type==2){ base = "ilk"; }
  $.post("index.php?action=addheart","t="+type+"&r="+id,function(data){
  $("a#"+base+"-"+id).html("<i class='fa fa-thumbs-up'></i> "+data);
  });


  if($("a#"+base+"-"+id).hasClass("btn-default")){
    $("a#"+base+"-"+id).removeClass("btn-default");
    $("a#"+base+"-"+id).addClass("btn-primary");
  }else if($("a#"+base+"-"+id).hasClass("btn-primary")){
    $("a#"+base+"-"+id).removeClass("btn-primary");
    $("a#"+base+"-"+id).addClass("btn-default");   
  }


}
/*  $("#publish").click(function(){
    var data = $("#status").serialize();
    var xhr = new XMLHttpRequest();
    xhr.open("GET","./?r=users/publish&"+data,false);
    xhr.send();
    $("#statuses").prepend(xhr.responseText);
    $("#statusarea").val("");
    $(".buttons").hide("fast");
    $("#statusarea").prop("rows",2);

  });
*/
</script>
<?php else:?>
<div class="container">
<div class="row">
<div class="col-md-9">
<div class="jumbotron">
<h2>No se encontro el usuario</h2>
</div>
</div>
</div>
</div>
<?php endif; ?>
<br><br>
<br><br>
<br><br>
