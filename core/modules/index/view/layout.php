<?php
   setlocale(LC_TIME,"en_US");
  require_once 'core/modules/index/model/DaoOrganizacion.php';
  $DaoOrganizacion = new DaoOrganizacion();

  // Establece Sesión si la hay
  if (isset($_COOKIE["id_usuario"]) && isset($_COOKIE["marca_aleatoria_usuario"])){
    //Tengo cookies memorizadas
    //además voy a comprobar que esas variables no estén vacías
    if ($_COOKIE["id_usuario"]!="" || $_COOKIE["marca_aleatoria_usuario"]!=""){
      //Voy a ver si corresponden con algún usuario
      $sql = "select * from user where id=" . $_COOKIE["id_usuario"] . " and session='" . $_COOKIE["marca_aleatoria_usuario"] . "' and session<>''";
      $query = Executor::doit($sql);
      $user = Model::one($query[0],new UserData());

      if (isset($user->email)) {
        $user = UserData::getLogin($user->email,$user->password);
        if($user!=null){      // Validaciones
          if(!$user->is_active){
            Core::alert("Error en inicio de sesión.");
            Core::redir("./");
          }
          if($user->is_active && $user->is_valid){
            Session::set("user_id",$user->id);
          }
        }
      }
    }
  }

  if(Session::exists("user_id")){
    Session::$user = UserData::getById(Session::get("user_id"));
    Session::$profile = $DaoOrganizacion->getByUserId(Session::get("user_id"));
    Session::$organizacion = $DaoOrganizacion->getByUserId(Session::get("user_id"));
    if(!empty(Session::$organizacion->id)) $_SESSION['org_id'] = Session::$organizacion->id;
  }

  header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
  header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
  header("Cache-Control: no-store, no-cache, must-revalidate");
  header("Cache-Control: post-check=0, pre-check=0", false);
  header("Pragma: no-cache");

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="icon" href="assets/img/home/url.png" type="image/x-icon" />
    <title>Bihaiv</title>
    <link rel="stylesheet" type="text/css" href="res/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="res/messages.css">
    <link rel="stylesheet" type="text/css" href="res/toast.css">
    <link rel="stylesheet" type="text/css" href="res/material.css">
    <link rel="stylesheet" type="text/css" href="res/font-awesome/css/font-awesome.min.css">
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>
    <!-- NEW STYLES -->
    <link rel="stylesheet" href="res/animate/animate.css">
    <link rel="stylesheet" href="res/owl.carousel.2.0.0-beta.2.4/assets/owl.carousel.css">
    <link rel="stylesheet" href="res/owl.carousel.2.0.0-beta.2.4/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="res/callendar/fullcalendar.min.css">
    <link rel="stylesheet" href="res/tagit_3/css/typeahead.tagging.css">
    <link rel="stylesheet" href="res/jquery-ui/jquery.ui.css">
    <link rel="stylesheet" href="res/dropzone/dist/min/dropzone.min.css">
    <link rel="stylesheet" href="res/timepicker/jquery.timepicker.css">
    <link rel="stylesheet" href="res/fullpage/dist/jquery.fullpage.css">
    <link rel="stylesheet" href="assets/css/base.css">
    <link rel="stylesheet" href="assets/css/new_styles.css">
    <script src="res/jquery/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.1.1/jquery.rateyo.min.css">
    <script src="res/bootstrap/js/bootstrap.min.js"></script>
    <!-- sweet alert -->
    <link rel="stylesheet" href="res/bootstrap-sweetalert/dist/sweetalert.css">
    <script src="res/bootstrap-sweetalert/dist/sweetalert.min.js"></script>
    <!-- GOOGLE CHARTS -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
  </head>
  <body>
      <!-- - - - - - - - - - - - - - - -->
      <?php include ('views/header.php'); ?>
      <!-- - - - - - - - - - - - - - - -->
      <?php View::load("index"); ?>
      <!-- - - - - - - - - - - - - - - -->
      <?php include ('views/footer.php'); ?>
      <!-- - - - - - - - - - - - - - - -->
      <!--SCRIPTS-->
      <script src="res/jquery-validate/dist/jquery.validate.min.js"></script>
      <!-- <script src="res/owl/lib/owl-carousel/owl.carousel.js"></script> -->
      <!-- NEW SCRIPT-->
      <script src="res/owl.carousel.2.0.0-beta.2.4/owl.carousel.min.js"></script>
      <!-- <script src="res/jquery.mb.YTPlayer.js"></script> -->
      <script src="res/handlebars/handlebars.js"></script>
      <script src="res/callendar/lib/moment.min.js"></script>
      <script src="res/callendar/lib/jquery-ui.custom.min.js"></script>
      <script src="res/callendar/fullcalendar.min.js"></script>
      <script src="res/jquery-ui/jquery.ui.js"></script>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.10.4/typeahead.bundle.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.1.1/jquery.rateyo.min.js"></script>
      <script src="res/dropzone/dist/min/dropzone.min.js"></script>
      <script src="res/timepicker/jquery.timepicker.min.js"></script>
      <script src="res/date-pair/dist/datepair.min.js"></script>
      <script src="res/date-pair/dist/jquery.datepair.min.js"></script>
      <script src="res/tagit_3/js/typeahead.tagging.js"></script>
      <script src="res/bloodhound.js"></script>
      <script src="res/mojs-master/build/mo.min.js"></script>
      <script src="res/fullpage/vendors/scrolloverflow.min.js"></script>
      <script src="res/fullpage/dist/jquery.fullpage.min.js"></script>
      <script src="http://maps.google.com/maps/api/js?key=AIzaSyBxG4FoW_Mx6zqrYJXQBsyzscH13xLFjqI&libraries=visualization"></script>
	    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>
      <script src="res/icmap.js"></script>
      <!-- Notificaction -->
      <script src="res/toast.js"></script>
      <script src="res/html2canvas.js"></script>
      <script>
        var opts = {
          "closeButton" : true,
          "debug" : false,
          "positionClass" : "toast-bottom-right",
          "onclick" : null,
          "showDuration" : "600",
          "hideDuration" : "1300",
          "timeOut" : "7500",
          "extendedTimeOut" : "1000",
          "showEasing" : "swing",
          "hideEasing" : "linear",
          "showMethod" : "fadeIn",
          "hideMethod" : "fadeOut"
        };
        function set_flash(msg, clase){
          switch (clase){
            case 'danger' :
              toastr.error(msg, '¡Error!', opts);
            break;
            case 'error' :
              toastr.error(msg, '¡Error!', opts);
            break;
            case 'success' :
              toastr.success(msg, '¡Perfecto!', opts);
            break;
            case 'warning' :
              toastr.warning(msg, 'Atención', opts);
            break;
            default :
              toastr.info(msg, 'Mensaje', opts);
            break;
          }
        }
        function setFlash(msg, clase){
          set_flash(msg, clase);
        }
        $("#fullpage").scroll(function(e){
          debugger;
          var scroll = $(window).scrollTop();
          if ( scroll > 500 ){
            $("#Header__fixed").fadeIn();
            $("#Header").css({ 'display' : 'none' });
          } else {
            $("#Header__fixed").fadeOut();
            $("#Header").css({ 'display' : 'block' });
          }
        });
      </script>
      <?php
        //Carga todos los archivos de la carpeta assets/js
        Core::includeJS();
      ?>
  </body>
</html>
