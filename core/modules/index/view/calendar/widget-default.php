<?php
  @$id = $_SESSION["user_id"];
?>
<style>
  #owl-demo .item img{
    display: block;
    width: 100%;
    height: 300px;
  }
  /* HERE COMES NEW CHALLERNGER!!!*/
  .calendar-thumbnail-list{
    background-color: whitesmoke;
    border-radius: 2px;
    box-shadow: 0 1px 3px 0 rgba(0,0,0,.2),0 1px 1px 0 rgba(0,0,0,.14),0 2px 1px -1px rgba(0,0,0,.12);
    background-repeat:no-repeat;
    background-position: center center;
    background-size: cover;
  }
  #owl-demo .item .cover-img{
    display: block;
    width: 100%;
    height: 300px;
    background-color: #bfbfbf;
    background-repeat:no-repeat;
    background-position: center center;
    background-size: cover;
  }
  .owl-carousel .owl-item {
    overflow: hidden;
  }
  /* END HERE COMES NEW CHALLERNGER!!!*/
</style>
<div class="container-fluid" style="margin-left:0; margin-right:0;padding-left:0; padding-right:0; ">
    <?php include('./views/main_menu.php'); ?>
    <!-- here comes new challenger -->
    <div class="gray-header col-md-12">
      <div class="gray-text">
        <div class="container">
          <div class="img-helper col-xs-6 col-xs-offset-3 col-sm-3 col-sm-push-9 col-sm-offset-0 col-md-3 col-md-push-9 col-md-offset-0">
            <!-- img -->
            <span class="helper"></span>
            <img src="assets/img/home/header-eventos.png" class="img-header img-resposive" />
          </div>
          <div class="col-xs-12 col-sm-9 col-sm-pull-3 col-md-9 col-md-pull-3">
            <h1 class="title-gray"><?=translate('Eventos')?></h1>
            <p class="description-gray">
              <?=translate('Descripción corta sección eventos')?>
            </p>
          </div>
        </div>
      </div>
    </div>
    <!-- End here comes new challenger -->
</div>
<div class="container">
    <section class="Profile__wall">
      <!-- Slider -->
      <div class="row">
        <div class="col-md-12">
          <div id="owl-demo" class="owl-carousel owl-theme"></div>
        </div>
      </div>
      <hr>
      <div class="row row-margin-bottom">
        <div class="col-md-12">
          <div class="col-md-12">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active">
                <a href="#home-tab" onclick="getCurrentEvents()" aria-controls="home" role="tab" data-toggle="tab">
                  <?=translate('Este Mes')?>
                </a>
              </li>
              <li role="presentation">
                <a href="#profile-tab" onclick="getCoomingEvents()" aria-controls="profile" role="tab" data-toggle="tab">
                <?=translate('Próximamente')?>
                </a>
              </li>
              <li role="presentation">
                <a href="#last-events-tab" onclick="getLastEvents()" aria-controls="last-events" role="tab" data-toggle="tab">
                  <?=translate('Más antiguos')?>
                </a>
              </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane fade in active" id="home-tab">
                <div class="col-xs-12 col-lg-12 margin-row-bottom">
                  <div class="row">
                    <h4><?=translate('Filtrar por')?>:</h4>
                    <div class="col-xs-12 col-md-6 col-lg-4">
                      <div class="form-group">
                        <select class="form-control" id="cmb_CurrentEvents" data-filter-type="currentEvents">
                          <option value=""><?=translate('Tipo evento')?></option>
                          <option value="Festival"><?=translate('Festival')?></option>
                          <option value="Taller"><?=translate('Taller')?></option>
                          <option value="Conferencia"><?=translate('Conferencia')?></option>
                          <option value="Exposición"><?=translate('Exposición')?></option>
                        </select>
                      </div><!-- /input-group -->
                    </div>
                    <div class="col-xs-12 col-md-6 col-lg-3">
                      <div class="form-group">
                        <input type="text" id="txt__CurrentFilterDate" class="form-control" placeholder="<?=translate('Fecha evento')?>">
                      </div><!-- /input-group -->
                    </div>

                    <div class="col-xs-12 col-md-6 col-lg-3">
                      <div class="form-group">
                        <input type="text" id="txt__CurrentEventName" class="form-control" placeholder="<?=translate('Nombre evento')?>">
                      </div><!-- /input-group -->
                    </div>

                    <div class="col-xs-12 col-md-12 col-lg-2">
                      <button class="btn btn-primary btn-block" onclick="eventFilter('currents')">
                        <i class="fa fa-search"></i> <?=translate('Filtrar')?>
                      </button>
                    </div>
                  </div>
                </div>
                <div class="col-xs-12 col-lg-12 margin-row-bottom">
                  <div class="row">
                    <div id="month-events"></div>
                  </div>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane fade " id="profile-tab">
                <div class="col-xs-12 col-lg-12 margin-row-bottom">
                  <div class="row">
                    <h4><?=translate('Filtrar por')?>:</h4>
                    <div class="col-xs-12 col-md-6 col-lg-4">
                      <div class="form-group">
                        <select class="form-control" id="cmb_CoomingEvents" data-filter-type="coomingEvents">
                          <option value="Festival"><?=translate('Festival')?></option>
                          <option value="Taller"><?=translate('Taller')?></option>
                          <option value="Conferencia"><?=translate('Conferencia')?></option>
                          <option value="Exposición"><?=translate('Exposición')?></option>
                        </select>
                      </div><!-- /input-group -->
                    </div>
                    <div class="col-xs-12 col-md-6 col-lg-3">
                      <div class="form-group">
                        <input type="text" id="txt__CoomingEventsDate" class="form-control" placeholder="<?=translate('Fecha evento')?>">
                      </div><!-- /input-group -->
                    </div>
                    <div class="col-xs-12 col-md-6 col-lg-3">
                      <div class="form-group">
                        <input type="text" id="txt__CoomingEventName" class="form-control" placeholder="<?=translate('Nombre evento')?>">
                      </div><!-- /input-group -->
                    </div>
                    <div class="col-xs-12 col-md-12 col-lg-2">
                      <button class="btn btn-primary btn-block" onclick="eventFilter('coomings')">
                        <i class="fa fa-search"></i> <?=translate('Filtrar')?>
                      </button>
                    </div>
                  </div>
                </div>
                <div class="col-xs-12 col-lg-12 margin-row-bottom">
                  <div class="row">
                    <div id="cooming-events"></div>
                  </div>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane fade " id="last-events-tab">
                <div class="col-xs-12 col-lg-12 margin-row-bottom">
                  <div class="row">
                    <h4><?=translate('Filtrar por')?>:</h4>
                    <div class="col-xs-12 col-md-6 col-lg-4">
                      <div class="form-group">
                        <select class="form-control" id="cmb_LatestEvents" data-filter-type="currentEvents">
                          <option value="Festival"><?=translate('Festival')?></option>
                          <option value="Taller"><?=translate('Taller')?></option>
                          <option value="Conferencia"><?=translate('Conferencia')?></option>
                          <option value="Exposición"><?=translate('Exposición')?></option>
                        </select>
                      </div><!-- /input-group -->
                    </div>
                    <div class="col-xs-12 col-md-6 col-lg-3">
                      <div class="form-group">
                        <input type="text" id="txt__LatestEventDate" class="form-control" placeholder="<?=translate('Fecha evento')?>">
                      </div><!-- /input-group -->
                    </div>
                    <div class="col-xs-12 col-md-6 col-lg-3">
                      <div class="form-group">
                        <input type="text" id="txt__LatestEventName" class="form-control" placeholder="<?=translate('Nombre evento')?>">
                      </div><!-- /input-group -->
                    </div>
                    <div class="col-xs-12 col-md-12 col-lg-2">
                      <button class="btn btn-primary btn-block" onclick="eventFilter('latest')">
                        <i class="fa fa-search"></i> <?=translate('Filtrar')?>
                      </button>
                    </div>
                  </div>
                </div>
                <div class="col-xs-12 col-lg-12 margin-row-bottom">
                  <div class="row">
                    <div id="last-events"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <div id="modal-launcher"></div>
</div>
<!-- CURRENT EVENTS TEMPLATE -->
<script id="month-events-tmpl" type="x-text/handlebars">
    {{#each this}}
      <div class="panel panel-card">
      <div class="panel-body">
        <div class="row">
          <div class="col-xs-6 col-md-4 col-lg-3 margin-row-bottom">
            <figure class="figure">
            <!-- <img class="img-event" src="uploads/eventos/{{_avatar}}" alt="{{titulo}}"> -->
              <div class="img-event calendar-thumbnail-list" style="background-image: url(uploads/eventos/{{_avatar}});"></div>
            </figure>
          </div>
          <div class="col-xs-6 col-md-8 col-lg-9 margin-row-bottom">
            <h2 class="title--initiative bolder">{{_titulo}}</h2>
            <h4 class="date--initiative bolder"><?=translate('Fecha')?>: {{_fecha}} <br> <?=translate('Empieza')?>: {{_horaInicio}} <?=translate('Finaliza')?>: {{_horaFin}}</h4>
            <h4 class="date--initiative bolder"><?=translate('Evento creado por')?>: <a href="<?php echo APP_PATH. '?view=home&id=';?>{{_id_actor}} ">{{_actor}}  </a></h4>
            <h4 class="place--initiative bolder">{{_lugar}}</h4>
            <h3 class="place--initiative bolder">{{_acceso}}</h3>
            <span class="margin-row-top">
    					<p>
    						{{_descripcion}}
    					</p>
    					<p>
    						<a href="?view=calendar-detail&id={{_idevento}}" class="btn btn-primary"><?=translate('Ver más')?></a>
    					</p>
            </span>
            {{#if _colaboradores}}
              <span class="margin-row-top">
                <h4><?=translate('Colaboradores')?></h4>
                <p>
                  {{#each _colaboradores}}
                    <a class="pull-left padding-col-lg" style="margin-right:5px;" href="?view=home&id={{this.user_id}}" title="{{this.user_name}}">
                      <figure class="figure">
                        <img src="<?php APP_PATH ?>storage/users/{{this.user_id}}/profile/{{this.avatar}}" alt="{{this.user_id}}" height="40">
                      </figure>
                    </a>
                  {{/each}}
                </p>
              </span>
            {{/if}}
          </div>
          <!--<div class="col-xs-12 col-lg-12 social">
            <div class="col-xs-4 col-md-4 col-lg-4">
              <figure class="figure">
                <div id="fav-{{@index}}" class="heart" onclick="addFav({{@index}}, {{_idevento}}, {{_tipo}})">
                    <i class="fa fa-heart"></i>
                    <span class="event-likes">{{_likes}}</span>
                </div>
              </figure>
            </div>-->
            <div class="col-xs-4 col-md-4 col-lg-4"></div>
          </div>
        </div>
     </div>
     </div>
    {{/each}}
</script>
<script>
  $(function(){
      getCurrentEvents();
      var e = new Evento();
      // Dibuja Carrusel
      e._getAllEvents();
      $("#txt__CurrentFilterDate").datepicker({ dateFormat: 'yy-mm-dd' });
      $("#txt__CoomingEventsDate").datepicker({ dateFormat: 'yy-mm-dd' });
      $("#txt__LatestEventDate").datepicker({ dateFormat: 'yy-mm-dd' });
  });
</script>
