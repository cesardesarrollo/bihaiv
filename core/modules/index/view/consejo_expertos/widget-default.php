<?php
  require_once './core/modules/index/model/DaoExperto.php';
  require_once './core/modules/index/model/DaoEstados.php';
  require_once './core/modules/index/model/DaoRoles.php';
  $DaoExperto = new DaoExperto();
  $DaoRoles = new DaoRoles();
  $DaoEstados = new DaoEstados();
  $UserData = new UserData();
?>
<div  class="container-fluid" style="margin-left:0; margin-right:0;padding-left:0; padding-right:0; ">
    <?php include('./views/main_menu.php'); ?>
    <!-- here comes new challenger -->
    <div class="gray-header col-md-12">
      <div class="gray-text">
        <div class="container">
          <div class="img-helper col-xs-6 col-xs-offset-3 col-sm-3 col-sm-push-9 col-sm-offset-0 col-md-3 col-md-push-9 col-md-offset-0">
            <!-- img -->
            <span class="helper"></span>
            <img src="assets/img/home/header-expertos.png" class="img-header img-resposive" />
          </div>
          <div class="col-xs-12 col-sm-9 col-sm-pull-3 col-md-9 col-md-pull-3">
            <h1 class="title-gray"><?=translate('Listado de expertos')?></h1>
            <p class="description-gray">
              <?=translate('Descripción corta sección expertos')?>
            </p>
          </div>
        </div>
      </div>
    </div>
    <!-- End here comes new challenger -->
    <div class="cards-container col-md-12">
      <?php
      $resultSet = $DaoExperto -> getAllExperts();
      ?>
       <?php foreach ($resultSet as $k => $Experto): ?>
         <div class="panel panel-card">
           <div class="panel-body">
             <div class="row">
               <div class="col-md-8">
                 <h3 class="title" ><?php echo ucfirst(strtolower($Experto['_name'])); ?></h3>
                 <p>
                   <?php echo $Experto['_organizacion']; ?>
                 </p>
                 <a class="more-info" href="#!" onclick="showInfoExpert(<?php echo $Experto['_id'] ?>);"><?=translate('Más información')?></a>
               </div>
               <div class="col-md-4">
                 <center>
                   <?php if ( empty($Experto['_avatar'])) : ?>
                     <img src="assets/img/home/temp-sprite.png" class="img-resposive" />
                   <?php elseif ( file_exists("admin/files/".$Experto['_avatar'])) : ?>
                     <img  src="admin/files/<?php echo $Experto['_avatar'] ?>" class="img-resposive" style="max-width: 100%; " />
                   <?php else: ?>
                     <img src="assets/img/home/temp-sprite.png" class="img-resposive" />
                   <?php endif; ?>
                 </center>
               </div>
             </div>
           </div>
         </div>
       <?php endforeach; ?>
       <!-- Modulo de mas informacio -->
       <h1 class="more-info-title"><?=translate('¿Necesitas más información?')?></h1>
       <div class="panel panel-card color-gray">
         <div class="panel-body">
           <div class="row">
             <div class="col-md-2">
               <center>
                 <img src="assets/img/home/temp-sprite.png" style="max-width: 100%;" class="img-resposive" />
               </center>
             </div>
             <div class="col-md-10">
               <h3 class="title" >Sample text</h3>
               <p>
                 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
                 do eiusmod tempor incididunt ut labore et dolore magna aliqua.
               </p>
             </div>
           </div>
         </div>
       </div>
       <div class="panel panel-card color-gray">
         <div class="panel-body">
           <div class="row">
             <div class="col-md-2">
               <center>
                 <img src="assets/img/home/temp-sprite.png" style="max-width: 100%;" class="img-resposive" />
               </center>
             </div>
             <div class="col-md-10">
               <h3 class="title" >Sample text</h3>
               <p>
                 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
                 do eiusmod tempor incididunt ut labore et dolore magna aliqua.
               </p>
             </div>
           </div>
         </div>
       </div>

    </div>

    <div id="modal-launcher"></div>
</div>
