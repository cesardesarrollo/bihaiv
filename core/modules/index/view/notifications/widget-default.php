<?php

//Comprueba sesión activa
if(!isset($_SESSION["user_id"])){ Core::redir("./"); }

if($_SESSION["user_id"]){
  require_once( './core/modules/index/model/DaoIniciativa.php' );
  require_once( './core/modules/index/model/DaoOrganizacion.php' );
  require_once( './core/modules/index/model/DaoColaboracion.php' );
  $DaoIniciativa = new DaoIniciativa();
  $DaoColaboracion = new DaoColaboracion();
  $DaoOrganizacion = new DaoOrganizacion();
  $Organizacion = $DaoOrganizacion->getByUserId($_SESSION["user_id"]);
  $notificaciones = NotificationData::getAllByUserIdInArray($_SESSION["user_id"]);
  ?>
  <div class="container padding-top-100">
    <div class="row">
      <div class="col-md-3">
        <?php Action::execute("_userbadge",array("user"=>Session::$user,"organizacion"=>Session::$organizacion,"from"=>"logged" ));?>
        <?php Action::execute("_mainmenu",array());?>
      </div>
      <div class="col-md-7">
        <h2><?=translate('Notificaciones')?></h2>
        <?php if(count($notificaciones)>0):?>
        <table class="table table-bordered">
          <thead>
            <th><?=translate('Notificación')?></th>
          </thead>
          <?php

            if (strlen($_SESSION["user_id"])>0) 
            {
              $notificaciones = NotificationData::getAllByUserIdInArray($_SESSION["user_id"]);
              $array_notificaciones = array();
             // echo count($notificaciones);
              if(count($notificaciones)>1){ 
              foreach ($notificaciones as $notificacion) {
                // Se marca como leida
                NotificationData::readById($notificacion['id']);
                // Se obtienen mensajes vigentes
                if (!NotificationData::getMessageById($notificacion['id'])) {
                  $mensaje = strip_tags(utf8_decode($notificacion['message'])) . " <small>(Esta notificación ya no se encuentra vigente)</small>";
                } else {
                  $mensaje = utf8_decode($notificacion['message']);
                }

                if ($mensaje){
                  $array_push = array('mensaje'     => $mensaje, 
                                      'is_readed'   => $notificacion['is_readed'], 
                                      'created_at'  => $notificacion['created_at'], 
                                      'not_type_id' => $notificacion['not_type_id'], 
                                      'type_id'     => $notificacion['type_id'], 
                                      'ref_id'      => $notificacion['ref_id']);
                    array_push($array_notificaciones, $array_push );
                }
              }
               }
            }

            foreach($array_notificaciones as $noti): ?>
            <tr class="<?php if(!$noti['is_readed']){ echo "warning";}?>">
              <td>
                <span class="message">
                  <?php echo $noti['mensaje']; ?></a>
                  <br><br>
                </span>
                <span class="time pull-right"><i class="fa fa-clock-o"></i>
                  <?php echo $noti['created_at']; ?>
                    <?php

                    // Colaboraciones iniciativa
                    $colaboracion_iniciativa = $DaoColaboracion->getByIdIniciativaUserId($noti['ref_id'],$Organizacion->id);
                    $idColaboracion_iniciativa = @$colaboracion_iniciativa[0]['idColaboracion'];
                    $estatus_iniciativa = @$colaboracion_iniciativa[0]['estatus'];

                    if($noti['type_id'] == 5 && $noti['not_type_id'] == 5){
                      if ($estatus_iniciativa != 1 && $estatus_iniciativa != 2){
                        echo "<button class='btn btn-success btn-xs btn_aceptar' value='".$idColaboracion_iniciativa."' >".translate('Aceptar')."</button> ";
                        echo "<button class='btn btn-danger btn-xs btn_rechazar' value='".$idColaboracion_iniciativa."' >".translate('Rechazar')."</button>";
                      } else if ($estatus_iniciativa == 1){
                        echo "  - ".translate('Estatus').": <strong class='text-success'>".translate('Colaborando')."<strong>";
                      } else if ($estatus_iniciativa == 2){
                        echo " - ".translate('Estatus').": <strong class='text-danger'>".translate('Rechazado')."<strong>";
                      }
                    }

                    // Colaboraciones evento
                    $colaboracion_evento = $DaoColaboracion->getByIdEventoUserId($noti['ref_id'],$Organizacion->id);
                    $idColaboracion_evento = @$colaboracion_evento[0]['idColaboracion'];
                    $estatus_evento = @$colaboracion_evento[0]['estatus'];

                    if($noti['type_id'] == 4 && $noti['not_type_id'] == 5){
                      if ($estatus_evento != 1 && $estatus_evento != 2){
                        echo "<button class='btn btn-success btn-xs btn_aceptar' value='".$idColaboracion_evento."' >".translate('Aceptar')."</button> ";
                        echo "<button class='btn btn-danger btn-xs btn_rechazar' value='".$idColaboracion_evento."' >".translate('Rechazar')."</button>";
                      } else if ($estatus_evento == 1){
                        echo "  - ".translate('Estatus').": <strong class='text-success'>".translate('Colaborando')."<strong>";
                      } else if ($estatus_evento == 2){
                        echo " - ".translate('Estatus').": <strong class='text-danger'>".translate('Rechazado')."<strong>";
                      }
                    }
                ?>
                </span>
              </td>
            </tr>
            <?php 
              endforeach; ?>
        </table>
        <?php else:?>
        <div class="jumbotron">
          <h2><?=translate('No hay notificaciones')?></h2>
        </div>
        <?php endif;?>
      </div>
      <div class="col-md-2">
      </div>
    </div>
  </div>
  </div>
  <br><br><br><br><br>
  <div id="modal-launcher"></div>
  <script type="text/javascript">
    $(function (){
      var colaboracion = new Colaboraciones();
      $('.btn_aceptar').on('click',function(e){
        e.preventDefault();
        var data = {
          '_idColaboracion'  : $(this).val(),
          '_estatus'         : 1, // Aceptar
          '_method'          : 'update'
        }
        colaboracion._set(data);
        window.location.reload();
      });
      $('.btn_rechazar').on('click',function(e){
        e.preventDefault();
        var data = {
          '_idColaboracion'  : $(this).val(),
          '_estatus'         : 2, // Rechazar
          '_method'          : 'update'
        }
        colaboracion._set(data);
        window.location.reload();
      });
    });
  </script>
<?php } ?>