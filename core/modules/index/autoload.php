<?php
// autoload.php
// esta funcion elimina el hecho de estar agregando los modelos manualmente


function __autoload($modelname){

	/* ==========================================================================
	*			Validacion par evitar que el admin ingrese al portal
	* ==========================================================================*/
	if(!empty($_SESSION)){
		/* La variable session no esta vacia */
		if(isset($_SESSION['is_admin']) AND $_SESSION['is_admin'] ==  true ){
			/* Es administrador se debe readireccionar al admin */
			header("Location: ".APP_PATH."admin"); // Redirecionamos a Admin
			exit();
		} else if(isset($_SESSION['is_expert']) AND $_SESSION['is_expert'] ==  true ){
			/* Es experto se debe readireccionar al admin */
			header("Location: ".APP_PATH."admin"); // Redirecionamos a Admin
			exit();
		}
	}
	if(Model::exists($modelname)){
		include Model::getFullPath($modelname);
	}

	if(Form::exists($modelname)){
		include Form::getFullPath($modelname);
	}
}



?>
