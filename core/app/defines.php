<?php 
    define('DS','/');
    $hostName = $_SERVER['HTTP_HOST']; 
    $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"], 0, 5)) == 'https://' ? 'https://' : 'http://';
    define ('APP_PATH', $protocol.$hostName.'/bihaiv_2/');
    if (!isset($_COOKIE['bihaiv_lang'])){
        setcookie('bihaiv_lang', 'es', time() + (86400 * 30), "/"); // 86400 = 1 día
    }
?>