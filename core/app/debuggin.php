<?php
function debug ( $var, $html = true, $backtrace = null ) {
  $id = uniqid();
  if (is_null ( $backtrace) ){
    $backtrace = debug_backtrace ( );
  }
  $debug = "<div id='$id'>" . "<code class=''>" . "<strong>FILE: " . $backtrace [ 0 ] [ 'file' ] . "</strong>" . "<BR />" . PHP_EOL . "<strong>LINE: " . $backtrace [ 0 ] [ 'line' ] . "</strong>" . "<BR />" . PHP_EOL . "<pre>";
  ob_start ( );
  print_r ( $var );
  $dump = ob_get_clean ( );
  $debug .= htmlentities ( $dump );
  $debug .= "</pre>" . "</code>" . "</div>";
  if ( !$html ){
    $debug = strip_tags ( $debug );
  }
  echo $debug;
}
function breakpoint ( $var, $show_source = false ) {
  $break = debug_backtrace ( );
  debug ( $var, true, $break );
  if ( isset ( $this ) ){
    unset ( $this );
  }
  if($show_source){
    show_source ( $break [ 0 ] [ 'file' ] );
  }
  die ( 'Fin del Brakepoint: ' . date('Y-m-d H:i:s'));
}
function json ( $data, $debug = false ) {
  if ( $debug ) {
    echo "<pre>";
    print_r($data);
    echo "</pre>";
  } else {
    if (is_null($data ) or !is_array ($data)){
      $this -> set_404 ( );
    }
    if (!headers_sent ( )){
      header('Content-type: application/json');
    }
    echo json_encode ($data);
  }
  die();
}
$response = array(
  'msg' 		=> null,		// Algun mensaje específico.
  'status' 	=> true,
  'class'		=> null			// success, fail.
);
 ?>
