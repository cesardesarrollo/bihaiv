<?php
require_once("../autoload.php");
require_once('../controller/Core.php');
require_once('../controller/Registro.php');
require_once('../controller/Cuestionario.php');
require_once('../modules/index/model/UserData.php');
require_once('../modules/index/model/EstatusCuestionarioData.php');
require_once('../modules/index/model/CuestionariosUsuarioData.php');

require_once('../modules/index/model/CuestionarioData.php');
require_once('../modules/index/model/DaoEvaluacion.php');

	$daoEvaluacion=new DaoEvaluacion();
		//METODO PARA TRAERNOS LAS EVALUACIONES PENDIENTES POR REVISAR Y CADUCADAS DE REVISAR
	  $arrayExpertCyP=evaluacionesCaducadasYPendientes($cuestionariosPendientesbyExpert=$daoEvaluacion->getdatabyOpcion("4"), $daoEvaluacion);
	  $arrayActorCyP=evaluacionesCaducadasYPendientes($cuestionariosPendientesbyactor=$daoEvaluacion->getdatabyOpcion("2"), $daoEvaluacion);
        //SE ENVIA LOS CORREO CON RECORDATORIO ALOS USUARIOS QUE TIENE PENDIENTE EVALUAR
        foreach ($arrayActorCyP['pendientes'] as  $evaluacionpendiente) {
		       	 Registro::enviar_email('recordatorio','', $evaluacionpendiente['usuarioCalificador'],'',$evaluacionpendiente['usuarioRequisitor'],  $evaluacionpendiente['diasPendientes'] );
       	}
       	 foreach ($arrayExpertCyP['pendientes'] as  $evaluacionpendiente) {
		       	 Registro::enviar_email('recordatorio','', $evaluacionpendiente['usuarioCalificador'],'',$evaluacionpendiente['usuarioRequisitor'],  $evaluacionpendiente['diasPendientes'] );
       	}
       	//AQUI REALIZAREMOS EL CAMBIO DE CALIFICADOR PARA LOS EVALUADORES  QUE YA CADUCARON
        //Core::debug($arrayActorCyP['caducaron']);
          CambiarEvaluador($arrayActorCyP['caducaron'], "resultados_a_actor", $daoEvaluacion);
          CambiarEvaluador($arrayExpertCyP['caducaron'], "resultados_a_experto", $daoEvaluacion);

  function CambiarEvaluador($arraypendientes, $opcionUsuario, $daoEvaluacion){
        
        foreach ($arraypendientes as   $evaluacionpendiente) {

                   $daoEvaluacion->ignore($evaluacionpendiente['idevaluacion']);
                   //Core::debug("update evaluacion set respuesta='3' where idEvaluacion='".$evaluacionpendiente['idevaluacion']."'")  ;
                     $actores_evaluadores = Cuestionario::getActoresExceptCustom($evaluacionpendiente['userid_a_validar'], $evaluacionpendiente['exceptuarUsuarios']);
                       // Core::debug('id:'.$evaluacionpendiente['idevaluacion']);
        //           Core::debug( $actores_evaluadores );
                     $actor_random = "";

                    $claves_aleatorias = array_rand($actores_evaluadores,count($actores_evaluadores) );
                    //Core::debug( $claves_aleatorias );
                    if(count($claves_aleatorias) > 0){
                        foreach ($claves_aleatorias as $key) {
                            $actor_random = $actores_evaluadores[$key];
                        }
                    }

                   if(is_object($actor_random) && !empty($actor_random)){
                     //echo '<pre>'.  print_r( $actor_random). '</pre> esto es para cada evaluacion</br/>';

                             // Registro de evaluación creada
                              $x = new Evaluacion();
                              $x->setFechaEvaluacion(date('Y-m-d H:i:s')); 
                              $x->setIdCuestionariosUsuario($evaluacionpendiente['idCuestionariosUsuario']);
                              $x->setUsuario_id($actor_random->id);
                              $x->setConfidencialidad(0); 
                              $x->setRespuesta(0); 
                              $evaluaciones = $daoEvaluacion->add($x);
                              
                              if ($evaluaciones > 0) { 
                                  Registro::enviar_email($opcionUsuario,"",$evaluacionpendiente['userid_a_validar'],"",$actor_random->id, 0);
                                  echo "se envio correo al nuevo evaluador: ".$actor_random->id. " con  el idevaluacion: ".$evaluacionpendiente['idevaluacion']."<br/>";
                                }
                                
                }

                echo "<br/>Proceso terminado!.";
  }
}
	//funcion para evaluar laas evaluaciones que estan caducadas y pendientes
	function evaluacionesCaducadasYPendientes($cuestionariosEvaluando, $daoEvaluacion){
		$array_evaluaciones_caducaron  = array();
        $array_evaluaciones_pendientes = array();
        foreach ($cuestionariosEvaluando as $cuestionario) {
        	if(!empty($cuestionario))
        	{
        		if(Cuestionario::Vigencia($cuestionario['fechaEvaluacion'], 'maximo_para_evaluar'))
        		{

                         $evaluacionCaducada = array("idevaluacion"=>$cuestionario['idEvaluacion'], 
                                                  "idCuestionariosUsuario"    => $cuestionario['idCuestionariosUsuario'],
                                                "exceptuarUsuarios" => $daoEvaluacion->getUsuariosByIdCuestionarioUsuarioC($cuestionario['idCuestionariosUsuario']),
                                                "fechaEvaluacion"           => $cuestionario['fechaEvaluacion'],
                                                "userid_a_validar"          => $cuestionario['Usuarios_idUsuarios']);
                            array_push( $array_evaluaciones_caducaron, $evaluacionCaducada );
                         // echo '<pre>'.  print_r( $evaluacionCaducada ). '</pre>';
        		}
        		else
        		{
              if($cuestionario['diasEvaluacion']>0){
        			    $evaluacionPendiente= array("idevaluacion"=>$cuestionario['idEvaluacion'],
        			    						 "idCuestionariosUsuario"    => $cuestionario['idCuestionariosUsuario'],
                                                "usuarioCalificador" =>$cuestionario['user_id'], 
                                                "fechaEvaluacion"           => $cuestionario['fechaEvaluacion'],
                                                "diasPendientes"           => $cuestionario['diasEvaluacion'],
                                                "usuarioRequisitor"          => $cuestionario['Usuarios_idUsuarios']);
                            array_push( $array_evaluaciones_pendientes, $evaluacionPendiente );
                            }
                          //  echo '<pre>'.  print_r( $evaluacionPendiente ). '</pre>';

        		}


        	}
        }
        $arrayretorno=array('caducaron'=>$array_evaluaciones_caducaron, 'pendientes'=>$array_evaluaciones_pendientes);

        return $arrayretorno;
	}
?>