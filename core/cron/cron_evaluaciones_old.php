<?php

    // Revisa evaluaciones que ya caducaron para reenviarlas a distintos evaluadores aleatorios
    require_once("../autoload.php");
    require_once('../controller/Core.php');
    require_once('../controller/Cuestionario.php');
    require_once('../controller/Registro.php');
    require_once('../modules/index/model/UserData.php');
    require_once('../modules/index/model/CuestionarioData.php');
    require_once('../modules/index/model/CuestionariosUsuarioData.php');
    require_once('../modules/index/model/EstatusCuestionarioData.php');
    require_once('../modules/index/model/DaoOrganizacion.php');
    require_once('../modules/index/model/DaoRoles.php');
    require_once('../modules/index/model/DaoAportacion.php');
    require_once('../modules/index/model/DaoPublicaciones.php');
    require_once('../modules/index/model/DaoEvaluacion.php');

    
    $DaoEvaluacion          = new DaoEvaluacion();


    function evaluacionesCaducadasYPendientes($cuestionariosEvaluando){

        $array_evaluaciones_caducaron  = array();
        $array_evaluaciones_pendientes = array();

        foreach ($cuestionariosEvaluando as $cuestionarioEvaluando) {
            
            // Barre tabla de evaluacion para obtener de cada cuestionario de usuario las que estan caducadas
            $DaoEvaluacion          = new DaoEvaluacion();
            $evaluacionesEnProceso  = $DaoEvaluacion->getWaitingByIdCuestionariosUsuario($cuestionarioEvaluando->idCuestionariosUsuario);

            

        Core::debug($evaluacionesEnProceso);


            if(!empty($evaluacionesEnProceso)){

                // Barrer evaluaciones en proceso de evaluación
                foreach ($evaluacionesEnProceso as $evaluacionEnProceso) {

                    // Fecha de la evaluación
                    $fechaEvaluacion = $evaluacionEnProceso->fechaEvaluacion;
                    
                    // Usuarios a validar
                    $CuestionariosUsuarioData = new  CuestionariosUsuarioData();
                    $CuestionarioUsuario = $CuestionariosUsuarioData->getById($evaluacionEnProceso->idCuestionariosUsuario);
                    $UserData = new  UserData();
                    $org_a_validar = $CuestionarioUsuario->user_id;
                    $userValidando = $UserData->getByOrgId( $org_a_validar );
                    $userid_a_validar = $userValidando->id;

                    $usersByEvaluacion = $DaoEvaluacion->getUsuariosByIdCuestionarioUsuario($evaluacionEnProceso->idCuestionariosUsuario);

                    if (count($usersByEvaluacion) > 0){

                        $usersIdByEvaluacion = array();
                        foreach ($usersByEvaluacion as $userByEvaluacion){
                            $usersIdByEvaluacion[] = $userByEvaluacion->Usuario_id;
                        }
                        // Caducó
                        if (Cuestionario::vigencia($fechaEvaluacion, 'maximo_para_evaluar')){

                            $evaluacionActorCaducada = array(   "idCuestionariosUsuario"    => $evaluacionEnProceso->idCuestionariosUsuario,
                                                                "exceptuarUsuarios"         => $usersIdByEvaluacion,
                                                                "fechaEvaluacion"           => $fechaEvaluacion,
                                                                "userid_a_validar"          => $userid_a_validar );
                            array_push( $array_evaluaciones_caducaron, $evaluacionActorCaducada );

                        } 
                        // Está pendiente
                        else {

                            $evaluacionActorPendiente = array(  "idCuestionariosUsuario"    => $evaluacionEnProceso->idCuestionariosUsuario,
                                                                "exceptuarUsuarios"         => $usersIdByEvaluacion,
                                                                "fechaEvaluacion"           => $fechaEvaluacion,
                                                                "userid_a_validar"          => $userid_a_validar );
                            array_push( $array_evaluaciones_pendientes, $evaluacionActorPendiente );
                        
                        }

                    }

                }

            }
    
        }


        $response = array(  "array_evaluaciones_caducaron"  => $array_evaluaciones_caducaron,
                            "array_evaluaciones_pendientes" => $array_evaluaciones_pendientes );

        return $response;
    }


    $noEvaluadosPorActores  = array();
    $noEvaluadosPorExpertos = array();


    $EstatusCuestionarioData = new EstatusCuestionarioData();

    // Obtiene cuestionarios en estado de proceso de evaluación por actores
    $cuestionariosEvaluandoPorActores = $EstatusCuestionarioData->getInEvaluationByActors();
    // Obtiene cuestionarios en estado de proceso de evaluación por expertos
    $cuestionariosEvaluandoPorExpertos = $EstatusCuestionarioData->getInEvaluationByExperts();



    $noEvaluadosPorActores  = evaluacionesCaducadasYPendientes($cuestionariosEvaluandoPorActores);
    $noEvaluadosPorExpertos = evaluacionesCaducadasYPendientes($cuestionariosEvaluandoPorExpertos);
    
    Core::debug( $noEvaluadosPorActores );
    Core::debug( $noEvaluadosPorExpertos );




    $noIds = array();
    // Barrer por evaluaciones de actores y enviar notificaciones aleatorias
    foreach ($noEvaluadosPorActores['array_evaluaciones_caducaron'] as $noEvaluadoPorActorCaducado) {


       $cuestionario_id = $noEvaluadoPorActorCaducado['idCuestionariosUsuario'];

        if(isset($cuestionario_id)){
            if( $cuestionario_id === $noEvaluadoPorActorCaducado['idCuestionariosUsuario'] ){

                if (is_object($actor_random) and isset($actor_random->id)) {
                    $noIds[] = $actor_random->id;
                    array_push($noEvaluadoPorActorCaducado['exceptuarUsuarios'], $noIds);
                    $noIds = array();
                    unset($noIds);
                }

            } else {
                
                $cuestionario_id = $noEvaluadoPorActorCaducado['idCuestionariosUsuario'];
                $noIds = array();
                unset($noIds);
            }

        } else {

            $cuestionario_id = $noEvaluadoPorActorCaducado['idCuestionariosUsuario'];

        }

        // Elige una a una las evaluaciones para reemplazarlas

        $actores_evaluadores = Cuestionario::getActoresExcept($noEvaluadoPorActorCaducado['userid_a_validar'], $noEvaluadoPorActorCaducado['exceptuarUsuarios']);

    Core::debug( $actores_evaluadores );
        $actor_random = "";

        $claves_aleatorias = array_rand($actores_evaluadores,count($actores_evaluadores) );
        Core::debug( $claves_aleatorias );
        if(count($claves_aleatorias) > 0){
            foreach ($claves_aleatorias as $key) {
                $actor_random = $actores_evaluadores[$key];
            }
        }

        
         Core::debug( $actor_random );

        if(is_object($actor_random) && !empty($actor_random)){

            // Registro de evaluación creada
            $x = new Evaluacion();
            $x->setFechaEvaluacion(date('Y-m-d H:i:s')); 
            $x->setIdCuestionariosUsuario($noEvaluadoPorActorCaducado['idCuestionariosUsuario']);
            $x->setUsuario_id($actor_random->id);
            $x->setConfidencialidad(0); 
            $x->setRespuesta(0); 
            $evaluaciones = $DaoEvaluacion->add($x);
            
            if ($evaluaciones > 0) { 
                //Registro::enviar_email("resultados_a_actor","",$noEvaluadoPorActorCaducado['userid_a_validar'],"",$actor_random->id);

                Core::debug( $noEvaluadoPorActorCaducado['userid_a_validar'] );
                    
                Core::debug( $actor_random->id );
                    
            }

        }
        
    }



?>