<?php
require_once 'includes/header.php';
?>
<link rel="stylesheet" href="../res/dropzone/dist/min/dropzone.min.css">
<div class="container-fluid">
    <div class="Wall margin-row-bottom">
        <div class="Wall__header z-depth-1">
            <h3 class="display-inline text-white bolder"><i class="fa fa-users"></i> Aportación</h3>
            <div class="pull-right" style="margin-top:-3px">
                <a href="aportaciones.php" class="btn btn-outline-primary">
                    <i class="fa fa-reply" aria-hidden="true"></i> Regresar
                </a>
            </div>
        </div>
        <div class="Wall__content large padding">
            <div class="row">
                <div class="col-xs-12 col-md-12 col-lg-12" id="box-aportacion">


                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="id-aportacion" value="<?php echo (isset($_REQUEST['id']))?$_REQUEST['id']:'' ?>"/>
<input type="file" id="files" name="files[]" multiple="">
<script src="../res/dropzone/dist/min/dropzone.min.js"></script>
<?php
require_once 'includes/footer.php';
?>


