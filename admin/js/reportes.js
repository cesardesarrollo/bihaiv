$(document).ready(function(){	
    updatePage()
    
})

function updatePage(offset){
    var params= new Object();
      params.action="updatePage";
      params.offset=offset;
    $.post("reportes_aj.php",params,function(resp){
      $('#box-table-reportes').html(resp);
    });
}

function del(id){
    if(confirm("Está seguro de querer eliminar el reporte?")){
    var params= new Object();
        params.method="delete";
        params.id=id;
        $.post("../api/admin/reportes.php",params,function(resp){
          updatePage();
        });
    }
}

function validate(id){
  if(confirm("Está seguro de querer validar este reporte?")){
    var comments_response = prompt("Añada un comentario acerca de esta acción");
    if(comments_response!=""){
      var params= new Object();
        params.method="validate";
        params.comments_response = comments_response;
        params.id=id;
        $.post("../api/admin/reportes.php",params,function(resp){
          updatePage();
        });
      }
    }
}

function read(id){
  if(confirm("Está seguro de querer marcar como revisado este reporte?")){
    var comments_response = prompt("Añada un comentario acerca de esta acción");
    if(comments_response!=""){
      var params= new Object();
        params.method="read";
        params.comments_response = comments_response;
        params.id=id;
        $.post("../api/admin/reportes.php",params,function(resp){
          updatePage();
        });
      }
    }
}

function buscador(){
 var params= new Object()
   params.action="updatePage";
   params.buscar="%";
   if($.trim($('#buscar').val())){
     params.buscar=$('#buscar').val();
   }
  var funcionExito=function funcionExito(resp){
     $('#box-table-reportes').html(resp);
  }
  $.post("reportes_aj.php",params,funcionExito);
}


