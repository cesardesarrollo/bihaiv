$(document).ready(function () {
    updatePage()

})

function updatePage() {
    var params = new Object()
    params.action = "updatePage";
    params.id = $('#id-video').val()
    $.post("video_aj.php", params, function (resp) {
        $('#box-principal').html(resp)
    })
}

function saveVideo() {
    var accion = "save";
    if ($('#id-video').val() > 0) {
        accion = "edit"
    }
    var params = new Object()
    params.method = accion
    params.id = $('#id-video').val()
    params.nombre = $('#nombre').val()
    params.descripcion = $('#descripcion').val()
    params.url = $('#url').val()
    params.portada = $('#portada').prop('checked')
    var error = 0
    var errores = new Array()
    if ($('#nombre').val().length == 0) {
        error = "Nombre requerido"
        errores.push(error);
        setFlash('El nombre es requerido', 'warning');
    }
    if ($('#url').val().length == 0) {
        error = "Url requerido"
        errores.push(error);
        setFlash('La url es requerida', 'warning');
    }

    if (errores.length == 0) {
        $.post("../api/admin/videos.php", params, function (resp) {
            setFlash("Datos guardados con éxito", 'success');
            window.location = "videos.php"
        }, "json")
    } else {
        // $string="Error\n\n";
        // $.each(errores, function(key, val) {
        //     $string+="-"+val+"\n"
        // })
        // alert($string)
    }
}
