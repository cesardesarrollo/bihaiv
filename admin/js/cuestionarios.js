$(document).ready(function(){
    updatePage()

})

function updatePage(offset){
    var params= new Object()
        params.action="updatePage";
        params.offset=offset
    $.post("cuestionarios_aj.php",params,function(resp){
           $('#box-table-cuestionarios').html(resp)
    })
}

function deleteCuestionario(id){
    if(confirm("Está seguro de querer eliminar el cuestionario?")){
    var params= new Object()
        params.method="delete";
        params.id=id;
        $.post("../api/admin/cuestionarios.php",params,function(resp){
              updatePage()
        })
    }
}


function buscador(){
 var params= new Object()
     params.action="updatePage"
     params.buscar="%"
     if($.trim($('#buscar').val())){
       params.buscar=$('#buscar').val()
     }
  var funcionExito=function funcionExito(resp){
         $('#box-table-cuestionarios').html(resp)
  }
  $.post("cuestionarios_aj.php",params,funcionExito);
}
