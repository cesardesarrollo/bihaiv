$(document).ready(function(){	
    updatePage()
    
})

function updatePage(offset){
    var params= new Object()
        params.action="updatePage";
        params.offset=offset
    $.post("aportaciones_aj.php",params,function(resp){
           $('#box-table-aportaciones').html(resp)
    })    
}

function deleteAportacion(id){
    if(confirm("Está seguro de querer eliminar la aportación?")){
    var params= new Object()
        params.method="delete";
        params.id=id
        
        $.post("../api/admin/aportaciones.php",params,function(resp){
          updatePage();
        });
    }
}

function saveConfiguration(){
    if(confirm("¿Esta seguro de querer guardar la configuración de las secciones?")){
      var items = new Array();
      var params = new Object();
      params.method = 'configuration';
      
      $('#configuration-container input').each(function(){
        var checked = '';
        if($(this).is(':checked')) checked = 'checked';
        items.push({ id: $(this).data('id'), checked: checked });
      });

      params.items = items;
        
      $.post("../api/admin/aportaciones.php",params,function(resp){
        console.log(resp);
      });
    }
}


function buscador(){
 var params= new Object()
     params.action="updatePage"
     params.buscar=""
     if($.trim($('#buscar').val())){
       params.buscar=$('#buscar').val()
     }
  var funcionExito=function funcionExito(resp){
         $('#box-table-aportaciones').html(resp)
  }
  $.post("aportaciones_aj.php",params,funcionExito);
}


