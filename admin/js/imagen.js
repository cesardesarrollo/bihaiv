$(document).ready(function () {
    updatePage()

})

function updatePage() {
    var params = new Object()
    params.action = "updatePage";
    params.id = $('#id-imagen').val()
    $.post("imagen_aj.php", params, function (resp) {
        $('#box-principal').html(resp)
        cargarImagen()
    })
}

function saveImagen() {
    var accion = "save";
    if ($('#id-imagen').val() > 0) {
        accion = "edit"
    }
    var params = new Object()
    params.method = accion
    params.id = $('#id-imagen').val()
    params.titulo = $('#nombre').val()
    params.descripcion = $('#descripcion').val()
    params.description = $('#description').val()
    params.llave = $('#llave-imagen').val()
    params.portada = $('#portada').prop('checked')
    var error = 0
    var errores = new Array()
    if ($('#nombre').val().length == 0) {
        error = "Título requerido"
        errores.push(error);
    }
    if ($('#llave-imagen').val().length == 0) {
        error = "Imagen requerido"
        errores.push(error);
    }

    if(!$('#descripcion').val()){
      error = "Descripcion requerida"
      errores.push(error);
    }
    if(!$('#description').val()){
      error = "Descripcion en ingles requerida"
      errores.push(error);
    }


    if (errores.length == 0) {
        $.post("../api/admin/imagenes.php", params, function (resp) {
            setFlash("Datos guardados con éxito", 'success');
            window.location = "imagenes.php"
        },"json")

    } else {
        $string="Error\n\n";
        $.each(errores, function(key, val) {
            $string+="-"+val+"\n"
        })
        setFlash($string, 'danger');
    }
}


function cargarImagen(){
  Dropzone.autoDiscover = false;
  var myDropzone = $("div#box-img").dropzone({
    url: 'upload_image.php?action=uploadAttachment&id='+$('#id-imagen').val()+'&llave='+$('#llave-imagen'),
    dictDefaultMessage: "",
    acceptedFiles: 'image/*',
    maxFiles: 1,
    accept: function(file, done) {
      $("div#box-img").css('background-image', 'none');
      done();
    },
    init: function() {
      this.on("addedfile", function() {
        if (this.files[1]!=null){
          this.removeFile(this.files[0]);
        }
      });
    },
    success: function(file, response){
      $('#llave-imagen').val(response)
    }
  });
}
