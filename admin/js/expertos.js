$(document).ready(function(){
    updatePage()

})

function updatePage(offset){
    var params= new Object()
        params.action="updatePage";
        params.offset=offset
    $.post("expertos_aj.php",params,function(resp){
           $('#box-table-expertos').html(resp)
    })
}

function deleteExperto(id){
    if(confirm("Está seguro de querer eliminar al experto?")){
    var params= new Object()
        params.method="delete";
        params.id=id
        $.post("../api/admin/expertos.php",params,function(resp){
              updatePage()
        })
    }
}

function getFormInvitacion(){
    var params= new Object()
        params.action="getFormInvitacion";
    $.post("expertos_aj.php",params,function(resp){
        mostrarModal(resp)
    })
}

function saveExperto() {
    var params = new Object()
    params.method = "save"
    params.nombre = $('#nombre').val()
    params.email = $('#email').val()
    params.nonce = "1"
    var error = 0
    var errores = new Array()
    if ($('#nombre').val().length == 0) {
        error = "Nombre requerido"
        errores.push(error);
    }
    if ($('#email').val().length == 0) {
        error = "Email requerido"
        errores.push(error);
    }
    if (errores.length == 0) {
        $.post("../api/admin/expertos.php", params, function (resp) {
            if(resp.id>0){
               sendIncitacion(resp.id)
            } else {
                setFlash(resp.error, 'warning');
            }
        },"json")
    } else {
        setFlash(errores.toString(), 'danger')
    }
}

function sendIncitacion(id){
    var params= new Object()
        params.action="sendIncitacion";
        params.id=id
    $.post("expertos_aj.php",params,function(resp){
           ocultarModal()
           setFlash(resp, 'info');
    })
}

function buscador(){
 var params= new Object()
     params.action="updatePage"
     params.buscar="%"
     if($.trim($('#buscar').val())){
       params.buscar=$('#buscar').val()
     }
  var funcionExito=function funcionExito(resp){
         $('#box-table-expertos').html(resp)
  }
  $.post("expertos_aj.php",params,funcionExito);
}
