$(document).ready(function(){
    updatePage()

});
function updatePage(offset){
    var params= new Object()
        params.action="updatePage";
        params.offset=offset
    $.post("aportaciones_aj_bihaiv.php",params,function(resp){
           $('#box-table-aportaciones').html(resp)
    })
}
function deleteAportacion(id){
  if(confirm("Está seguro de querer eliminar la aportación?")){
    var params= new Object()
    params.method="delete";
    params.id=id
    $.post("../api/admin/aportaciones.php",params,function(resp){
      updatePage();
    });
  }
}
