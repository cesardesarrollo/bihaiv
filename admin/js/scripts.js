function mostrarModal(html){
    $("#myModal").modal({
        show : true,
        backdrop : 'static',
        keyboard : false
    })
    $("#myModal").html(html)
}


function ocultarModal(){
    $("#myModal").modal('hide')
}

function logout() {
    var params = new Object()
    params.action = "logout";
    $.post("index_aj.php", params, function (resp) {
        window.location=resp
    })
}