<?php
require_once '../core/app/debuggin.php';
require_once '../core/modules/index/model/DaoEnfoque.php';

$DaoEnfoque = new DaoEnfoque();

$method = $_GET['method'];

switch ($method) {
  case 'get_all':
    json($DaoEnfoque -> getAllTipoEnfoque());
    break;
  case 'save':
    $DaoEnfoque -> saveTipoEnfoque($_POST['tipo_enfoque']);
    json($DaoEnfoque -> response);
    break;
  case 'delete':
    $DaoEnfoque -> deleteTipoEnfoque($_POST);
    json($DaoEnfoque -> response);
    break;

  default:
    # code...
    break;
}

 ?>
