<?php
session_start();
require_once '../core/modules/index/model/DaoAportacion.php';
require_once '../core/modules/index/model/DaoPublicaciones.php';
require_once '../core/app/defines.php';
require_once '../core/controller/Core.php';
require_once '../core/app/debuggin.php';
require_once '../core/controller/Database.php';
require_once '../core/controller/Executor.php';
require_once '../core/controller/Model.php';
require_once '../core/modules/index/model/UserData.php';

$count = 0;

if ($_POST['action'] == "updatePage") {
    ?>
    <script>
      $(function(){
        $('#lista-aportaciones').stacktable({myClass:'visible-xs'});
      })
    </script>
    <table class="table table-bordered hidden-xs" id="lista-aportaciones">
        <thead>
            <tr>
                <th>#</th>
                <th>Thumbnail</th>
                <th>Título</th>
                <th>Descripción</th>
                <th>Fecha</th>
                <th>Actividades</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $offset=null;
            $limit=10;
            if(isset($_POST['offset']) && $_POST['offset']>0){
                $offset=$_POST['offset'];
            }
            $buscar="";
            // Buscar por user_id
            if(isset($_POST['buscar']) && strlen($_POST['buscar'])>0){
              $buscar= $_POST['buscar'];
            }
            $DaoPublicaciones = new DaoPublicaciones();
            $count = 1;

            $ResultSet = $DaoPublicaciones->getAll($offset,$limit,$buscar, 'fecha desc', 5);
            foreach ($ResultSet as $publicacion) {
   
                $DaoAportacion = new DaoAportacion();
                $aportacion  =  $DaoAportacion->getByPostId( $publicacion->id );
            ?>
            <tr>
                <td><?php echo $count; ?></td>
                <td><img class="img-responsive" width="120" src="../uploads/aportaciones/<?php echo $aportacion->getThumbnail() ?>"></td>
                <td><?php echo $aportacion->getTitulo() ?></td>
                <td><?php echo $aportacion->getDescripcion() ?></td>
                <td><?php echo $aportacion->getFecha() ?></td>
                <td class="center">
                    <?php
                    $controls = false;
                    if($_SESSION['is_expert']){
                        if( $publicacion->by_expert && $_SESSION['experto_id'] == $publicacion->user_id ){
                            $controls = true;
                        } else {
                            $controls = false;
                        }
                    } else if ($_SESSION['is_admin']){
                        $controls = true;
                    } else {
                        $controls = false;
                    }
                    
                    if ($controls):
                    ?>
                    <a href="aportacion_bihaiv.php?id=<?php echo $aportacion->getId() ?>"><button type="button" class="btn btn-default">Editar</button></a>
                    <button type="button" class="btn btn-default" onclick="deleteAportacion(<?php echo $aportacion->getId() ?>)">Eliminar</button>
                    <?php endif; ?>
                </td>
            </tr>
            <?php
            $count++;
            }
            ?>
        </tbody>
    </table>
    <nav>
        <?php
        //$count=50;
        if($count>10){
        ?>
            <ul class="pagination">
                <?php
                if($offset>=10){
                    ?>
                     <li><a onclick="updatePage(<?php echo $offset-10;?>)" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
                   <?php
                }
                $numPaginas=1;
                for($i=1;$i<=$count;$i++){
                    if(($i%10)==1){
                    ?>
                     <li <?php if(($i-1)==$offset){ ?> class="active" <?php } ?>><a onclick="updatePage(<?php echo $i-1;?>)"><?php echo $numPaginas?></a></li>
                    <?php
                       $numPaginas++;
                    }
                }
                if($offset>=10){
                    ?>
                     <li><a onclick="updatePage(<?php echo $offset+10;?>)"aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
                     <?php
                }
                ?>
            </ul>
        <?php
        }
        ?>
    </nav>
    <?php
}
