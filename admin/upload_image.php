<?php
require_once '../core/modules/index/model/DaoImagenHome.php';
require_once '../core/modules/index/model/DTO/ImagenHome.php';


if (isset($_GET['action']) && $_GET['action'] == "uploadAttachment") {
    if (count($_FILES) > 0) {
        if ($_FILES["file"]["error"] > 0) {
            echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
        } else {

            $DaoImagenHome = new DaoImagenHome();
            $namefile = $DaoImagenHome->generarKey() . ".jpg";
            if ($_GET['id'] > 0) {
                $imagen = $DaoImagenHome->getById($_GET['id']);
                if ($imagen->getLlaveImg()!=$_GET['llave'] && strlen($imagen->getLlaveImg())>0) {
                    //Eliminar la foto anterior
                    if(file_exists('files/' . $imagen->getLlaveImg())){
                        unlink('files/' . $imagen->getLlaveImg());
                    }
                }
            }
            $ubicacion = "files/" . $namefile;
            move_uploaded_file($_FILES["file"]["tmp_name"], $ubicacion);
            echo $namefile;
        }
    } elseif (isset($_GET['action'])) {

        if (isset($_FILES["file"]["error"]) && $_FILES["file"]["error"] > 0) {

            echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
        } else {


            if (isset($_GET['base64'])) {
                // If the browser does not support sendAsBinary ()
                $dataFile = file_get_contents('php://input');
                if (isset($_POST['fileExplorer'])) {
                    //If the browser support readAsArrayBuffer ()
                    //PHP handles spaces in base64 encoded string differently
                    //so to prevent corruption of data, convert spaces to +
                    $dataFile = $_POST['fileExplorer'];
                    $dataFile = str_replace(' ', '+', $dataFile);
                }
                $content = base64_decode($dataFile);
            }else{
                $content = file_get_contents('php://input');
            }
                                
            $DaoImagenHome = new DaoImagenHome();
            $namefile = $DaoImagenHome->generarKey() . ".jpg";
            if ($_GET['id'] > 0) {
                $imagen = $DaoImagenHome->getById($_GET['id']);
                if ($imagen->getLlaveImg()!=$_GET['llave'] && strlen($imagen->getLlaveImg())>0) {
                    if(file_exists('files/' . $imagen->getLlaveImg())){
                        unlink('files/' . $imagen->getLlaveImg());
                    }
                }
            }

            $ubicacion = "files/" . $namefile;
            file_put_contents($ubicacion, $content);
            echo $namefile;
        }
    }
}