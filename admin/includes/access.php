<?php

     function getPassExpert(){
      $getPass = false;
      $getPass = ( isset($_SESSION['is_expert']) AND $_SESSION['is_expert'] ===  true ) ? true : false;
      return $getPass;
    }

    function getPassAdmin(){
      $getPass = false;
      $getPass = ( isset($_SESSION['is_admin']) AND $_SESSION['is_admin'] ===  true ) ? true : false;
      return $getPass;
    }

    function haveAccess($archivo){

      switch($archivo){
        case "":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? true : false;

        break;
        case "actores.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? false : false;

        break;
        case "actores_aj.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? false : false;

        break;
        case "ajax_tipo_enfoque.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? false : false;

        break;
        case "ajax_videos_home.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? false : false;

        break;
        case "aportacion.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? false : false;

        break;
        case "aportacion_aj.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? true : false;

        break;
        case "aportacion_bihaiv.php":

          $getPassAdmin  = (getPassAdmin()) ? false : false;
          $getPassExpert = (getPassExpert()) ? true : false;

        break;
        case "aportacion_bihaiv_aj.php":

          $getPassAdmin  = (getPassAdmin()) ? false : false;
          $getPassExpert = (getPassExpert()) ? false : false;

        break;
        case "aportaciones.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? false : false;

        break;
        case "aportaciones_aj.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? false : false;

        break;
        case "aportaciones_aj_bihaiv.php":

          $getPassAdmin  = (getPassAdmin()) ? false : false;
          $getPassExpert = (getPassExpert()) ? true : false;

        break;
        case "aportaciones_bihaiv.php":

          $getPassAdmin  = (getPassAdmin()) ? false : false;
          $getPassExpert = (getPassExpert()) ? true : false;

        break;
        case "categoria_aportacion.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? false : false;

        break;
        case "categoria_aportacion_aj.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? false : false;

        break;
        case "categorias_aportacion.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? false : false;

        break;
        case "categorias_aportacion_aj.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? false : false;

        break;
        case "cuestionario.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? false : false;

        break;
        case "cuestionario_aj.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? false : false;

        break;
        case "cuestionarios.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? false : false;

        break;
        case "cuestionarios_aj.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? false : false;

        break;
        case "experto.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? true : false;

        break;
        case "experto_aj.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? true : false;

        break;
        case "expertos.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? false : false;

        break;
        case "expertos_aj.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? false : false;

        break;
        case "form_tipo_enfoque.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? false : false;

        break;
        case "form_videos_home.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? false : false;

        break;
        case "imagen.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? false : false;

        break;
        case "imagen_aj.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? false : false;

        break;
        case "imagenes.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? false : false;

        break;
        case "imagenes_aj.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? false : false;

        break;
        case "index.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? true : false;

        break;
        case "index_aj.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? true : false;

        break;
        case "logout.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? false : false;

        break;
        case "reportes.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? false : false;

        break;
        case "reportes_aj.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? false : false;

        break;
        case "rol.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? false : false;

        break;
        case "rol_aj.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? false : false;

        break;
        case "roles.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? false : false;

        break;
        case "roles_aj.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? false : false;

        break;
        case "tipos_enfoque.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? false : false;

        break;
        case "upload_image.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? false : false;

        break;
        case "video.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? false : false;

        break;
        case "video_aj.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? false : false;

        break;
        case "videos.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? false : false;

        break;
        case "videos_aj.php":

          $getPassAdmin  = (getPassAdmin()) ? true : false;
          $getPassExpert = (getPassExpert()) ? false : false;

        break;

      } 

      if ($getPassExpert) { 
        return ""; 
      } else if ($getPassAdmin) { 
        return ""; 
      } else { return "hide"; } 
      
    }


    // Obtenemos el archivo en la ruta
    $archivo = parse_url($_SERVER['REQUEST_URI'],PHP_URL_PATH);

    $folders = "/bihaiv_2/admin/";

    $archivo = str_replace($folders, "", $archivo);

    if (haveAccess($archivo) === "hide"){
      echo "No tienes permiso de acceso.";
      exit;
    }