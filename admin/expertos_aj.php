<?php
require_once '../core/modules/index/model/DaoEstados.php';
require_once '../core/modules/index/model/DaoExperto.php';
require_once '../core/modules/index/model/DTO/Experto.php';

require_once '../core/app/defines.php';
require_once '../core/controller/Database.php';
require_once '../core/controller/Executor.php';
require_once '../core/controller/Model.php';
require_once '../core/vendor/phpmailer/class.phpmailer.php';
require_once '../core/controller/Cuestionario.php';
require_once '../core/modules/index/model/UserData.php';


if ($_POST['action'] == "updatePage") {
    ?>
    <script>
      $(function(){
        $('#lista-expertos').stacktable({myClass:'visible-xs'});
      })
    </script>
    <table class="table table-bordered hidden-xs" id="lista-expertos">
        <thead>
            <tr>
                <th>#</th>
                <th style="width: 95px;">Foto</th>
                <th>Nombre</th>
                <th>Ubicación</th>
                <th>Organización a la que pertenece</th>
                <th class="center">Actividades</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $offset=null;
            $limit=10;
            if(isset($_POST['offset']) && $_POST['offset']>0){
                $offset=$_POST['offset'];
            }
            $buscar="";
            if(isset($_POST['buscar']) && strlen($_POST['buscar'])>0){
              $buscar= $_POST['buscar'];
            }
            $DaoExperto = new DaoExperto();
            $DaoEstados = new DaoEstados();
            $UserData = new UserData();
            $count = 1;
            foreach ($DaoExperto->getAll($offset,$limit,$buscar) as $experto) {
                $user = $UserData->getById($experto->getUsuarios_idUsuarios());
                $nombreEstado = "";
                if ($experto->getUbicacion() > 0) {
                    $estado = $DaoEstados->getById($experto->getUbicacion());
                    $nombreEstado = $estado->getNombre();
                }
                $imagen = "";

                if (strlen($user->imagen) > 0) {
                    $imagen = 'style="background-image: URL(files/' . $user->imagen . ')"';
                }
                ?>
                <tr>
                    <td><?php echo $count; ?></td>
                    <td><div class="box-avatar" <?php echo $imagen; ?>></div></td>
                    <td><?php echo $user->name ?></td>
                    <td><?php echo $nombreEstado ?></td>
                    <td><?php echo $experto->getOrganizacion() ?></td>
                    <td class="center">
                        <a href="experto.php?id=<?php echo $experto->getId() ?>"><button type="button" class="btn btn-default">Editar</button></a>
                        <button type="button" class="btn btn-default" onclick="deleteExperto(<?php echo $experto->getId() ?>)">Eliminar</button>
                        </th>
                </tr>
                <?php
                $count++;
            }
            ?>
        </tbody>
    </table>
    <nav>
        <?php
        if($count>1){
        ?>
            <ul class="pagination">
                <?php
                if($offset>=10){
                    ?>
                     <li><a onclick="updatePage(<?php echo $offset-10;?>)" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
                   <?php
                }
                $numPaginas=1;
                for($i=1;$i<=$count;$i++){
                    if(($i%10)==1){
                    ?>
                     <li <?php if(($i-1)==$offset){ ?> class="active" <?php } ?>><a onclick="updatePage(<?php echo $i-1;?>)"><?php echo $numPaginas?></a></li>
                    <?php
                       $numPaginas++;
                    }
                }
                if($offset>=10){
                    ?>
                     <li><a onclick="updatePage(<?php echo $offset+10;?>)"aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
                     <?php
                }
                ?>
            </ul>
        <?php
        }
        ?>
    </nav>
    <?php
}

if ($_POST['action'] == "getFormInvitacion") {
    ?>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> Invitar experto</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="nombre" class="control-label">Nombre:</label>
                        <input type="text" class="form-control" id="nombre">
                    </div>
                    <div class="form-group">
                        <label for="email" class="control-label">Email:</label>
                        <input type="text" class="form-control" id="email">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" onclick="saveExperto()">Invitar</button>
            </div>
        </div>
    </div>
    <?php
}

if ($_POST['action'] == "sendIncitacion") {
    $Cuestionario = new Cuestionario();
    $DaoExperto = new DaoExperto();
    $UserData = new UserData();

    $experto = $DaoExperto->getById($_POST['id']);
    $user = $UserData->getById($experto->getUsuarios_idUsuarios());

    $nombre = $user->name;
    $emailto = $user->email;
    $nonce = $experto->getNonce();
    $subject = "Bihaiv - Invitación para colaborar como experto";
    $body = '<h2>Hola ' . $nombre . '</h2>
            <p>Has sido invitado para colaborar como experto en Bihaiv</p>
            <p>Si estas de acuerdo sigue la siguiente liga:</p>
            <p><a href="' . APP_PATH . 'invitacion/experto.php?nonce=' . $nonce . '">Aceptar invitación</a></p>';

    $altbody = 'Hola ' . $nombre . '

                Has sido invitado para colaborar como experto en Bihaiv
                Si estas de acuerdo ingresa a la siguiente liga:

                ' . APP_PATH . 'invitacion/experto.php?nonce=' . $nonce;

    $resp = $Cuestionario->sendMail($emailto, $subject, $body, $altbody);
    echo $resp;
}
