<?php
require_once '../core/modules/index/model/DaoCategoriaAportacion.php';
require_once '../core/modules/index/model/DaoSeccionAportacion.php';
require_once '../core/controller/Database.php';
require_once '../core/controller/Executor.php';
require_once '../core/controller/Model.php';

if (isset($_POST['action']) && $_POST['action'] == "updatePage") {
    $DaoCategoriaAportacion = new DaoCategoriaAportacion();
    $idCategoria = 0;
    $titulo = "";
    $activo = 1;
    $idPadre = 0;
    $idSeccion = 0;

    if (isset($_POST['id']) && $_POST['id'] > 0) {
        $categoria = $DaoCategoriaAportacion->getById($_POST['id']);
        $idCategoria = $categoria->getId();
        $titulo = $categoria->getTitulo();
        $activo = $categoria->getActivo();
        $idPadre = $categoria->getIdPadre();
        $idSeccion = $categoria->getIdSeccion();
    }
    ?>

    <div class="form-group">
        <label for="titulo">Título:</label>
        <input type="text" class="form-control" id="titulo" required value="<?php echo $titulo;?>">
    </div>
    <div class="form-group">
        <label for="descripcion">Estatus:</label>
        <select class="form-control" id="activo">
            <option value="1" <?php if($activo == 1) echo 'selected'; ?>>Activo</option>
            <option value="0" <?php if($activo == 0) echo 'selected'; ?>>Inactivo</option>
        </select>
    </div>
    <div class="form-group">
       <label for="idPadre">Categoría padre:</label>
       <select class="form-control" id="idPadre">
           <option value="0" <?php if($idPadre == 0) echo 'selected'; ?>>No tiene categoría padre</option>
           <?php
   
           $DaoCategoriaAportacion = new DaoCategoriaAportacion();
   
           foreach ($DaoCategoriaAportacion->getAllopcion('Padres') as $category) {
               if($idCategoria != $category->id){
           ?>
               <option value="<?php echo $category->id; ?>" data-section="<?php echo $category->idSeccion; ?>" <?php if($idPadre == $category->id) echo 'selected'; ?>><?php echo $category->titulo; ?></option>
           <?php
               }
           }
           ?>
       </select>
   </div>
    <div class="form-group">
        <label for="idSeccion">Sección:</label>
        <select class="form-control" id="idSeccion">
            <option value="0" <?php if($idSeccion == 0) echo 'selected'; ?>>No tiene sección asignada</option>
            <?php

            $DaoSeccionAportacion = new DaoSeccionAportacion();

            foreach ($DaoSeccionAportacion->getAllWithSumary(1) as $section) {
            ?>
                <option value="<?php echo $section->id; ?>" <?php if($idSeccion == $section->id) echo 'selected'; ?>><?php echo $section->titulo; ?></option>
            <?php
            }
            ?>
        </select>
    </div>
    <button type="submit" class="btn btn-primary pull-right" onclick="saveCategory()" >Guardar categoría</button>
<?php
}
?>