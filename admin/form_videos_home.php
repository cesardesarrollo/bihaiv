<?php
require_once '../core/app/debuggin.php';
require_once '../core/modules/index/model/DaoImagenHome.php';

$DaoImagenHome = new DaoImagenHome();
$id = $descripcion = $description = null;
$video_required = 'required';
if( isset($_POST['id']) AND !empty($_POST['id']) AND is_numeric($_POST['id']) AND $_POST['id'] > 0 ){
  $id = $_POST['id'];
  $resultSet = $DaoImagenHome -> getOneRow("SELECT * FROM videoshome WHERE idVideosHome = $id");
  if( !empty($resultSet) ){
    $descripcion = $resultSet['descripcion'];
    $description = $resultSet['description'];
    $video_required = '';
  }
}
 ?>
<form id="form-tipo-enfoque" enctype="multipart/form-data">
  <input type="hidden" name="videoshome[id]" value="<?= $id; ?>">
  <div class="form-group">
    <label for="exampleInputEmail1">Descripcion *</label>
    <input name="videoshome[descripcion]" type="text" class="form-control" placeholder="Descripción del video..." value="<?= $descripcion; ?>">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Description *</label>
    <input name="videoshome[description]" type="text" class="form-control" placeholder="Video description..." value="<?= $description; ?>">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Video* (.mp4)</label>
    <input type="file" name="video" value="" id="video_input" accept=".mp4" <?php echo $video_required ?>>
  </div>
  <div class="checkbox">
    <label>
      <input name="videoshome[portada]" type="checkbox" checked value="1"> Mostrar en portada
    </label>
  </div>
  <hr>
  <div class="form-group">
    <div class="pull-right">
      <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
      <button type="tipo_enfoque[submit]" class="btn btn-primary">Guardar</button>
    </div>
    <br>
  </div>
</form>
<script>
  $(function(){
    var video_input = $("#video_input")
    video_input.fileinput();
  });
</script>
