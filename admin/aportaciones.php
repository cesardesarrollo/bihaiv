<?php
require_once 'includes/header.php';
require_once('../core/modules/index/model/DaoSeccionAportacion.php');
$DaoSeccionAportacion = new DaoSeccionAportacion();

?>
<div class="container-fluid">
    <div class="row margin-row-bottom">
        <div class="Wall margin-row-bottom z-depth-1">
            <div class="Wall__header z-depth-1">
                <h3 class="display-inline text-white bolder"><i class="fa fa-users"></i> Habilitar secciones de aportaciones</h3>
            </div>
            <div class="Wall__content large padding">
                <div id="configuration-container">
                    <?php foreach ($DaoSeccionAportacion->getAll() as $seccion) { ?>
                    <div class="form-group">
                        <label for="habilitar_seccion_<?php echo $seccion->getId(); ?>"><input type="checkbox" id="habilitar_seccion_<?php echo $seccion->getId(); ?>" name="habilitar_seccion_<?php echo $seccion->getId(); ?>" data-id="<?php echo $seccion->getId(); ?>" <?php if($seccion->getActivo() == 1) echo 'checked'; ?>> "<?php echo $seccion->getTitulo(); ?>"</label>
                    </div>
                    <?php } ?>
                    <button type="button" class="btn btn-primary" onclick="saveConfiguration()">Guardar ajustes</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row margin-row-bottom">
        <div class="pull-right">
            <form class="form-inline" style="margin-right: 14px;">
                <div class="form-group">
                    <input type="text" class="form-control" id="buscar" placeholder="Buscar" onkeyup="buscador()">
                </div>
            </form>
        </div>
    </div>

    <div class="row margin-row-bottom">
      <div class="Wall margin-row-bottom z-depth-1">
          <div class="Wall__header z-depth-1">
              <h3 class="display-inline text-white bolder"><i class="fa fa-users"></i> Catálogo de aportaciones</h3>
  			<div class="pull-right" style="margin-top:-3px">
  				<a href="aportacion.php" class="btn btn-outline-primary">
  					<i class="fa fa-plus"></i> Agregar Nueva
  				</a>
  			</div>
          </div>
          <div class="Wall__content large padding">
              <div class="table-responsive" id="box-table-aportaciones">

              </div>
          </div>
      </div>
    </div>
</div>

<?php
require_once 'includes/footer.php';
?>
