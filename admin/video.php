<?php
require_once 'includes/header.php';
?>
<div class="container-fluid">
    <div class="Wall margin-row-bottom">
        <div class="Wall__header z-depth-1">
            <h3 class="display-inline text-white bolder"><i class="fa fa-file-video-o"></i> Video</h3>
            <div class="pull-right" style="margin-top:-3px">
                <a href="videos.php" class="btn btn-outline-primary">
                    <i class="fa fa-reply" aria-hidden="true"></i> Regresar
                </a>
            </div>
        </div>
        <div class="Wall__content large padding">
            <div class="row">
                <div class="col-xs-12 col-md-12 col-lg-12" id="box-principal"></div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="id-video" value="<?php echo $_REQUEST['id'] ?>"/>
<input type="file" id="files" name="files[]" multiple="">
<?php
require_once 'includes/footer.php';
?>
