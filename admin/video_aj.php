<?php
session_start();
require_once '../core/modules/index/model/DaoVideosHome.php';
require_once '../core/modules/index/model/DTO/VideosHome.php';

require_once '../core/controller/Database.php';
require_once '../core/controller/Executor.php';
require_once '../core/controller/Model.php';
require_once '../core/modules/index/model/UserData.php';

if (isset($_POST['action']) && $_POST['action'] == "updatePage") {
    $DaoVideosHome= new DaoVideosHome();
    $UserData = new UserData();

    $nombre = "";
    $descripcion = "";
    $portada = "";
    $url = "";
    if (isset($_POST['id']) && $_POST['id'] > 0) {

        $video = $DaoVideosHome->getById($_REQUEST['id']);
        $nombre = $video->getNombre();
        $descripcion = $video->getDescripcion();
        $portada = $video->getPortada();
        $url=$video->getRuta();
    }
    ?>
    <div class="col-xs-12 col-md-12 col-lg-12 seccion">
      <form id="video_form">
        <div class="form-group">
            <label for="nombre"><span class="requerido">*</span>Nombre:</label>
            <input type="text" class="form-control" id="nombre"  value="<?php echo $nombre; ?>">
        </div>
        <div class="form-group">
            <label for="descripcion"><span class="requerido"> </span>Descripción:</label>
            <input type="text" class="form-control" id="descripcion" value="<?php echo $descripcion; ?>">
        </div>
        <div class="form-group">
            <label for="url"><span class="requerido">*</span>Youtube Video ID:</label>
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon3">https://www.youtube.com/embed/</span>
              <input type="text" class="form-control" id="url" value="<?php echo $url; ?>" aria-describedby="basic-addon3">
            </div>
        </div>
        <div class="form-group">
            <label for="portada"><span class="requerido"> </span>Incluir en carrusel de portada:</label>
            <input type="checkbox" class="form-control" id="portada" <?php echo ($portada==1)?'checked="checked"':''; ?>>
        </div>
        <button type="submit" class="btn btn-primary pull-right">Guardar video</button>
      </form>
      <script>
        $(function(){
          $('#video_form').on('submit', function(e){
            e.preventDefault();
            saveVideo();
          })
        })
      </script>
      </script>
    </div>
    <?php
}
