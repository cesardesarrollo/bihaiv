<?php
require_once 'includes/header.php';
?>
<div class="container-fluid">
    <div class="row margin-row-bottom">
        <div class="pull-right">
            <form class="form-inline" style="margin-right: 14px;">
                <div class="form-group">
                    <input type="text" class="form-control" id="buscar" placeholder="Buscar" onkeyup="buscador()">
                </div>
            </form>
        </div>
    </div>
    <div class="Wall margin-row-bottom z-depth-1">
        <div class="Wall__header z-depth-1">
            <h3 class="display-inline text-white bolder"><i class="fa fa-list-ol"></i> Catálogo de cuestionarios</h3>
            <div class="pull-right" style="margin-top:-3px">
                <a href="cuestionario.php" class="btn btn-outline-primary" data-toggle="tooltip" title="Agregar Nuevo">
                    <i class="fa fa-plus"></i> <span class="hidden-xs hidden-sm" >Agregar Nuevo</span>
                </a>
            </div>
        </div>
        <div class="Wall__content large padding">
            <div class="table-responsive" id="box-table-cuestionarios">
            </div>
        </div>
    </div>
</div>
<?php
require_once 'includes/footer.php';
?>
