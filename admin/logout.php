<?php
    session_start();

    setcookie("id_usuario", "");
    setcookie("marca_aleatoria_usuario", "");

    if (isset($_SESSION["user_id"])) {
        unset($_SESSION["user_id"]);
        session_destroy();
    }
?>
<script>
    function delete_cookie( name ) {
        document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    }
    delete_cookie('id_usuario');
    delete_cookie('marca_aleatoria_usuario');
    window.location="../home/login.php";
</script>