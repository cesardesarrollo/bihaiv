<?php
require_once '../core/modules/index/model/DaoRoles.php';
require_once '../core/modules/index/model/DTO/Roles.php';

require_once '../core/app/defines.php';
require_once '../core/controller/Database.php';
require_once '../core/controller/Executor.php';
require_once '../core/controller/Model.php';
require_once '../core/modules/index/model/UserData.php';


if ($_POST['action'] == "updatePage") {
    ?>
    <script>
      $(function(){
        $('#lista-roles').stacktable({myClass:'visible-xs'});
      })
    </script>
    <table class="table table-bordered hidden-xs" id="lista-roles">
        <thead>
            <tr>
                <th>#</th>
                <th style="width: 95px;">Foto</th>
                <th>Nombre</th>
                <th>Descripción</th>
                <th>Activo</th>
                <th class="center">Actividades</th>
            </tr>
        </thead>
        <tbody>
            <?php

            $offset=null;
            $limit=10;
            if(isset($_POST['offset']) && $_POST['offset']>0){
                $offset=$_POST['offset'];
            }
            $buscar="";
            if(isset($_POST['buscar']) && strlen($_POST['buscar'])>0){
              $buscar= $_POST['buscar'];
            }
            $DaoRoles = new DaoRoles();

            $count = 1;
            foreach ($DaoRoles->getAll($offset,$limit,$buscar) as $rol) {

                $imagen = "";

                if (isset($rol->imagen)) {
                    $imagen = 'style="background-image: URL(files/' . $rol->imagen . ')"';
                }

                ?>
                <tr>
                    <td><?php echo $count; ?></td>
                    <td><div class="box-avatar" <?php echo $imagen; ?>></div></td>
                    <td><?php echo $rol->getNombre(); ?></td>
                    <td><?php echo $rol->getDescripcion() ?></td>
                    <td><?php echo ($rol->getActivo()==1)?'<i class="fa fa-check-circle"></i>':'<i class="fa fa-times-circle"></i>' ?></td>
                    <td class="center">
                        <a href="rol.php?id=<?php echo $rol->getId() ?>"><button type="button" class="btn btn-default">Editar</button></a>
                        <!--<button type="button" class="btn btn-default" onclick="deleteRol(<?php echo $rol->getId() ?>)">Eliminar</button>-->
                    </td>
                </tr>
                <?php
                $count++;
            }
            ?>
        </tbody>
    </table>
    <nav>
        <?php
        //$count=50;
        if($count>1){
        ?>
            <ul class="pagination">
                <?php
                if($offset>=10){
                    ?>
                     <li><a onclick="updatePage(<?php echo $offset-10;?>)" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
                   <?php
                }
                $numPaginas=1;
                for($i=1;$i<=$count;$i++){
                    if(($i%10)==1){
                    ?>
                     <li <?php if(($i-1)==$offset){ ?> class="active" <?php } ?>><a onclick="updatePage(<?php echo $i-1;?>)"><?php echo $numPaginas?></a></li>
                    <?php
                       $numPaginas++;
                    }
                }
                if($offset>=10){
                    ?>
                     <li><a onclick="updatePage(<?php echo $offset+10;?>)"aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
                     <?php
                }
                ?>
            </ul>
        <?php
        }
        ?>
    </nav>
    <?php
}
