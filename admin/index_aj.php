<?php
session_start();
require_once '../core/app/defines.php';

if ($_POST['action'] == "logout") {

    if (isset($_SESSION["user_id"])) {
        unset($_SESSION["user_id"]);
        session_destroy();
        echo APP_PATH . "home/login.php";
    }
}