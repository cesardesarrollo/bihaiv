<?php
require_once '../core/modules/index/model/DaoImagenHome.php';
require_once '../core/modules/index/model/DTO/ImagenHome.php';

require_once '../core/app/defines.php';
require_once '../core/controller/Database.php';
require_once '../core/controller/Executor.php';
require_once '../core/controller/Model.php';
require_once '../core/modules/index/model/UserData.php';


if ($_POST['action'] == "updatePage") {
    ?>
    <script>
      $(function(){
        $('#lista-expertos').stacktable({myClass:'visible-xs'});
      })
    </script>
    <table class="table table-bordered hidden-xs" id="lista-expertos">
        <thead>
            <tr>
                <th>#</th>
                <th style="width: 95px;">Imagen</th>
                <th>Nombre</th>
                <th>Descripción</th>
                <th>En Portada</th>
                <th class="center">Actividades</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $offset = null;
            $limit = 10;
            if (isset($_POST['offset']) && $_POST['offset'] > 0) {
                $offset = $_POST['offset'];
            }
            $buscar = "";
            if (isset($_POST['buscar']) && strlen($_POST['buscar']) > 0) {
                $buscar = $_POST['buscar'];
            }
            $DaoImagenHome = new DaoImagenHome();
            $count = 1;
            foreach ($DaoImagenHome->getAll($offset, $limit, $buscar) as $imagen) {
                $img = "";
                if (strlen($imagen->getLlaveImg()) > 0) {
                    $img = 'style="background-image: URL(files/' . $imagen->getLlaveImg() . ')"';
                }
                ?>
                <tr>
                    <td><?php echo $count; ?></td>
                    <td><div class="box-avatar" <?php echo $img; ?>></div></td>
                    <td><?php echo $imagen->getTitulo() ?></td>
                    <td><?php echo $imagen->getDescripcion() ?></td>
                    <td><?php echo ($imagen->getPortada()==1)?'<i class="fa fa-check-circle"></i>':'<i class="fa fa-times-circle"></i>'; ?></td>
                    <td class="center">
                        <a href="imagen.php?id=<?php echo $imagen->getId() ?>"><button type="button" class="btn btn-default">Editar</button></a>
                        <button type="button" class="btn btn-default" onclick="deleteImagen(<?php echo $imagen->getId() ?>)">Eliminar</button>
                        </th>
                </tr>
                <?php
                $count++;
            }
            ?>
        </tbody>
    </table>
    <nav>
        <?php
        //$count=50;
        if ($count > 1) {
            ?>
            <ul class="pagination">
                <?php
                if ($offset >= 10) {
                    ?>
                    <li><a onclick="updatePage(<?php echo $offset - 10; ?>)" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
                    <?php
                }
                $numPaginas = 1;
                for ($i = 1; $i <= $count; $i++) {
                    if (($i % 10) == 1) {
                        ?>
                        <li <?php if (($i - 1) == $offset) { ?> class="active" <?php } ?>><a onclick="updatePage(<?php echo $i - 1; ?>)"><?php echo $numPaginas ?></a></li>
                            <?php
                            $numPaginas++;
                        }
                    }
                    if ($offset >= 10) {
                        ?>
                    <li><a onclick="updatePage(<?php echo $offset + 10; ?>)"aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
                    <?php
                }
                ?>
            </ul>
            <?php
        }
        ?>
    </nav>
    <?php
}
