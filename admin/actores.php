<?php
require_once 'includes/header.php';
?>
<div class="container-fluid">
    <div class="row margin-row-bottom">
        <div class="pull-right">
            <form class="form-inline" style="margin-right: 14px;">
                <div class="form-group">
                    <input type="text" class="form-control" id="buscar" placeholder="Buscar" onkeyup="buscador()">
                </div>
            </form>
        </div>
    </div>
    <div class="Wall margin-row-bottom z-depth-1">
        <div class="Wall__header z-depth-1">
            <h3 class="display-inline text-white bolder"><i class="fa fa-users"></i> Catálogo de actores</h3>            
            <div class="pull-right">
                <button class="btn btn-outline-primary"  onclick="getActorsReport()"><i class="fa fa-drive-o" aria-hidden="true"></i> Descargar Reporte</button>
            </div>
        </div>
        <div class="Wall__content large padding">
            <div class="table-responsive" id="box-table-actores">

            </div>
        </div>
    </div>
</div>
<?php
require_once 'includes/footer.php';
?>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"></div>
