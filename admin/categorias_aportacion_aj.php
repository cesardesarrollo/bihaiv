<?php
require_once '../core/modules/index/model/DaoCategoriaAportacion.php';
require_once '../core/app/defines.php';
require_once '../core/controller/Database.php';
require_once '../core/controller/Executor.php';
require_once '../core/controller/Model.php';
require_once '../core/modules/index/model/UserData.php';
$count = 0;

if ($_POST['action'] == "updatePage") {
    ?>
    <script>
      $(function(){
        $('#lista-categorias-aportacion').stacktable({myClass:'visible-xs'});
      })
    </script>
    <table class="table table-bordered hidden-xs" id="lista-categorias-aportacion">
        <thead>
            <tr>
                <th>#</th>
                <th>Id</th>
                <th>Título</th>
                <th>Id Padre</th>
                <th>Activo</th>
                <th>Actividades</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $offset=null;
            $limit = 10;
            $where = [];

            if(isset($_POST['offset']) && $_POST['offset']>0){
                $offset = $_POST['offset'];
            }

            if(isset($_POST['buscar']) && strlen($_POST['buscar'])>0){
              $where = ['titulo LIKE ' => $_POST['buscar']];
            }

            $DaoCategoriaAportacion = new DaoCategoriaAportacion();
            $count = 1;
            foreach ($DaoCategoriaAportacion->getAll([1,0],$where,$offset,$limit) as $category) {
            ?>
            <tr>
                <td><?php echo $count; ?></td>
                <td><?php echo $category->getId(); ?></td>
                <td><?php echo $category->getTitulo() ?></td>
                <td><?php echo $category->getIdPadre() ?></td>
                <td><?php echo ($category->getActivo()==1)?'<i class="fa fa-check-circle"></i>':'<i class="fa fa-times-circle"></i>' ?></td>

                <td class="center">
                    <a href="categoria_aportacion.php?id=<?php echo $category->getId() ?>"><button type="button" class="btn btn-default">Editar</button></a>
                    <button type="button" class="btn btn-default" onclick="deleteCategory(<?php echo $category->getId() ?>)">Eliminar</button>
                </td>
            </tr>
            <?php
            $count++;
            }
            ?>
        </tbody>
    </table>
    <nav>
      <?php if($count>0): ?>
        <ul class="pagination">
          <?php if($offset>=10): ?>
            <!-- <li><a onclick="updatePage(<?php //echo $offset-10;?>)" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li> -->
          <?php else: ?>
            <!-- <li><a aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li> -->
          <?php endif; ?>
          <?php
          $numPaginas=1;
          for($i=1;$i<=$count;$i++){
            if(($i%10)==1){
              $nextButton = null;
              ?>
              <li <?php if(($i-1)==$offset){ $nextButton = $offset+10;?> class="active" <?php } ?>><a onclick="updatePage(<?php echo $i-1;?>)"><?php echo $numPaginas?></a></li>
              <?php
              $numPaginas++;
            }
          }
          ?>
          <?php if($offset>=10):?>
            <!-- <li><a onclick="updatePage(<?php //echo $offset+10;?>)" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li> -->
          <?php endif; ?>
        </ul>
      <?php endif;?>
    </nav>
    <?php
}
