<?php
require_once '../core/modules/index/model/DaoRoles.php';
require_once '../core/controller/Database.php';
require_once '../core/controller/Executor.php';
require_once '../core/controller/Model.php';
require_once '../core/modules/index/model/UserData.php';

if (isset($_POST['action']) && $_POST['action'] == "updatePage") {
    $DaoRoles = new DaoRoles();
    $UserData = new UserData();

    $nombre = "";
    $descripcion = "";
    $description = "";
    $descripcion_corta = "";
    $short_description = "";
    $activo = "-1";
    $imagen = "";
    $llaveImagen = "";
    $readonly = "";

    if (isset($_POST['id']) && $_POST['id'] > 0) {

        $rol = $DaoRoles->getById($_REQUEST['id']);
        $nombre = $rol->getNombre();

        $descripcion = $rol->getDescripcion();
        $description = $rol->getDescription();
        $descripcion_corta = $rol->getDescripcionCorta();
        $short_description = $rol->getShortDescription();
        $activo = $rol->getActivo();
        $llaveImagen= $rol->imagen;

        if (strlen($rol->imagen) > "") {
            $imagen = 'style="background-image: URL(files/' . $rol->imagen . ')"';
        }
        $readonly = "readonly";
    }
    ?>

    <div class="col-xs-12 col-sm-6 col-md-5 col-lg-4">
      <div class="col-xs-12 col-md-12 col-lg-12 box-img" <?php echo $imagen; ?>></div>
      <button  class="col-xs-12 col-md-12 col-lg-12 btn btn-primary" onclick="mostrarFinder()">Cargar imagen</button>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-7 col-lg-8 seccion">
        <div class="form-group">
            <label for="txt__Name">Nombre:</label>
            <input type="text" class="form-control" id="txt__Name" required value="<?php echo $nombre;?>" <?php echo $readonly;?>>
        </div>
        <div class="form-group">
            <label for="txt__Descripcion">Descripción (esp):</label>
            <input type="text" class="form-control" id="txt__Descripcion" required value="<?php echo $descripcion;?>">
        </div>
        <div class="form-group">
            <label for="txt__Description">Descripción (eng):</label>
            <input type="text" class="form-control" id="txt__Description" required value="<?php echo $description;?>">
        </div>
        <!--
        <div class="form-group">
            <label for="txt__DescripcionCorta">Descripción corta (esp):</label>
            <input type="text" class="form-control" id="txt__DescripcionCorta" required value="<?php echo $descripcion_corta;?>">
        </div>
        <div class="form-group">
            <label for="txt__ShortDescription">Descripción corta (eng):</label>
            <input type="text" class="form-control" id="txt__ShortDescription" required value="<?php echo $short_description;?>">
        </div>-->
        <div class="form-group">
            <label for="exampleInputPassword1">Estatus:</label>
            <select class="form-control" id="estatus">
                <option value="-1">Seleccione una opción</option>
                <option value="1" <?php if($activo==1){ ?> selected="selected" <?php } ?>>Activo</option>
                <option value="0" <?php if($activo==0){ ?> selected="selected" <?php } ?>>Inactivo</option>
            </select>
        </div>
    </div>
    <button type="submit" class="btn btn-primary pull-right" onclick="saveRol()" >Guardar rol</button>
    <input type="hidden" id="llave-imagen" value="<?php echo $llaveImagen;?>"/>
    <?php
}


if (isset($_GET['action']) && $_GET['action'] == "uploadAttachment") {
    if (count($_FILES) > 0) {
        if ($_FILES["file"]["error"] > 0) {
            echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
        } else {

            $DaoRoles = new DaoRoles();
            $namefile = $DaoRoles->generarKey() . ".jpg";
            if ($_GET['id'] > 0) {
                $rol = $DaoRoles->getById($_GET['id']);
                if ($rol->imagen!=$_GET['llave'] && strlen($rol->imagen)>0) {
                    //Eliminar la foto anterior
                    if(file_exists('files/' . $rol->imagen)){
                       unlink('files/' . $rol->imagen);
                    }
                }
            }
            $ubicacion = "files/" . $namefile;
            move_uploaded_file($_FILES["file"]["tmp_name"], $ubicacion);
            echo $namefile;
        }
    } elseif (isset($_GET['action'])) {

        if (isset($_FILES["file"]["error"]) && $_FILES["file"]["error"] > 0) {

            echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
        } else {


            if (isset($_GET['base64'])) {
                // If the browser does not support sendAsBinary ()
                $dataFile = file_get_contents('php://input');
                if (isset($_POST['fileExplorer'])) {
                    //If the browser support readAsArrayBuffer ()
                    //PHP handles spaces in base64 encoded string differently
                    //so to prevent corruption of data, convert spaces to +
                    $dataFile = $_POST['fileExplorer'];
                    $dataFile = str_replace(' ', '+', $dataFile);
                }
                $content = base64_decode($dataFile);
            }

            $DaoRoles = new DaoRoles();
            $namefile = $DaoRoles->generarKey() . ".jpg";
            if ($_GET['id'] > 0) {
                $rol=$DaoRoles->getById($_GET['id']);
                if ($rol->imagen!=$_GET['llave'] && strlen($rol->imagen)>0) {
                    //Eliminar la foto anterior
                    if(file_exists('files/' . $rol->imagen)){
                       unlink('files/' . $rol->imagen);
                    }
                }
            }

            $ubicacion = "files/" . $namefile;
            file_put_contents($ubicacion, $content);
            echo $namefile;
        }
    }
}
